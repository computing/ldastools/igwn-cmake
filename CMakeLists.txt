#========================================================================
# -*- mode: cmake -*-
#------------------------------------------------------------------------
#
# IGWN CMake Macros - A collection of cmake macros largely intended to replace autoconf/automake
#
# Copyright (C) 2018 California Institute of Technology
#
# IGWN CMake Macros is free software; you may redistribute it and/or
# modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# IGWN CMake Macros is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE
#
#------------------------------------------------------------------------
# Establish search path for macros
#------------------------------------------------------------------------
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/Modules")

set( PACKAGE_NAME igwn-cmake-macros )
set( PACKAGE_VERSION 1.6.0 )
set( planguages NONE )

set(
  PACKAGE_DESCRIPTION
  "This is a collection of CMake functions used as a replacement for autoconf macros"
  )
set(
  PACKAGE_DESCRIPTION_LONG
  "This collection of cmake macros and scripts was developed"
  "to ease the transition from autotools/automake to CMake."
  )

set( PACKAGE_BUGREPORT "https://git.ligo.org/ldastools/igwn-cmake/-/issues"
  CACHE INTERNAL "URL of where to submit problems" )
set( PACKAGE_URL "https://git.ligo.org/ldastools/igwn-cmake"
  CACHE INTERNAL "Homepage URL for Project" )
set( PACKAGE_SOURCE_URL "https://software.igwn.org/lscsoft/source/"
  CACHE INTERNAL "Source URL for Project" )

set( IGWN_CMAKE_SCRIPTDIR "${CMAKE_SOURCE_DIR}/scripts" )

cmake_minimum_required(VERSION 3.2)
project( ${PACKAGE_NAME} LANGUAGES ${planguages})
message(STATUS "After cmake_minimum_required")

#------------------------------------------------------------------------
# Preload common packages
#------------------------------------------------------------------------

message( STATUS "About to load includes" )
include( CTest )
include( GNUInstallDirs )
include( IGWNCMakeInstallDirs )
include( Autotools/Autotools )
include( Autotools/Archive/Archive )
include( Autotools/ArchiveX/ArchiveX )

#========================================================================
# Configure PROJECT
#------------------------------------------------------------------------

message(STATUS "About to call cm_prereq")
cm_prereq(3.2)
message(STATUS "About to call cm_init: '${PACKAGE_NAME}' '${PACKAGE_VERSION}' '${PACKAGE_BUGREPORT}' '${PACKAGE_NAME}' '${PACKAGE_URL}'")
cm_init(
  ${PACKAGE_NAME}
  "${PACKAGE_VERSION}"
  ${PACKAGE_BUGREPORT}
  ${PACKAGE_NAME}
  ${PACKAGE_URL}
  LANGUAGES ${planguages}
  DESCRIPTION "${PACKAGE_DESCRIPTION}"
  )

cx_msg_debug_variable( PROJECT )
cx_msg_debug_variable( PROJECT_NAME )
cx_msg_debug_variable( PROJECT_VERSION )
cx_msg_debug_variable( PROJECT_DESCRIPTION )
cx_msg_debug_variable( PROJECT_DESCRIPTION_LONG )

#========================================================================
# Traverse the tree
#------------------------------------------------------------------------

add_subdirectory(config)
add_subdirectory(Modules)
add_subdirectory(scripts)
add_subdirectory(doc)

#========================================================================
# Prepare for testing
#------------------------------------------------------------------------

include(CTest)
enable_testing( )

#========================================================================
# Prepare the distribution
#------------------------------------------------------------------------

cx_cpack( DEBIAN_BUILD_TYPE --build=all
          MACPORTS_CATEGORY devel )

add_custom_target( install-doc
  COMMENT "Installing documentation"
  COMMAND ${CMAKE_COMMAND} --build .  --target install
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/doc
  )

