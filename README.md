# Abstract
This is a collection of macros and scripts that were developed to aid in the
process of converting Autotools based projects into CMake.

# Quick Start
To effectively use the macros, the following is a minimal example

```cmake
cmake_minimum_required( VERSION 3.2 )

include( GNUInstallDirs )

find_package( IGWNCMake CONFIG REQUIRED
  HINTS ${CMAKE_INSTALL_FULL_DATADIR}/igwn-cmake/cmake/Modules
  PATHS ${CMAKE_INSTALL_FULL_DATADIR}/igwn-cmake/cmake/Modules
  )

cm_prereq(3.2)
cm_init(
   MyProject
   1.0.0
   https://myproject.org/bugs
   MyProject
   https://myproject.org/
   )
```
# Detailed documentation
