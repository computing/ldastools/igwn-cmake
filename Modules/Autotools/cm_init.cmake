#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_init( package version [bug-report] [tarname] [homepage] )
#
# NOTE:
#   This is implemented as a macro so the variables become part of the
#      current context.
#   This should only be called from the top level CMakeLists.txt file.
#------------------------------------------------------------------------
include( CMakeParseArguments )

include(Autotools/cm_define)
include(Autotools/cm_msg_notice)
include(Autotools/Internal/ci_cache)
include(Autotools/Internal/ci_project_config)
include(Autotools/Internal/ci_set_policy)
include(Autotools/ArchiveX/cx_msg_debug_variable)
include(Autotools/ArchiveX/cx_scheme_sanitizer)

function(cm_init_set_var_ VARIABLE DEFAULT_VALUE )
  if ( NOT DEFINED ${VARIABLE} )
    if ( DEFINED ENV{${VARIABLE}} )
      set( ${VARIABLE} "$ENV{${VARIABLE}}" CACHE INTERNAL "")
    else( )
      set( ${VARIABLE} "${DEFAULT_VALUE}" CACHE INTERNAL "" )
    endif( )
  else( )
    set( ${VARIABLE} "${${VARIABLE}}" CACHE INTERNAL "" )
  endif( )
endfunction( )

## /**
## @igwn_group_InitializeCMake
## @igwn_group_begin
## */
## /**
## @fn cm_init( package version bugreport tarname homepage )
## @brief Initialize package configuration
## @details
## Initialize package configuration for the CMake project
##
## @param string package
## Name of the package
##
## @param version
## String representation of the version number
##
## @param bugreport
## URL of where to submit bug reports
##
## @param tarname
## The name used by the package when producing tarballs
##
## @param homepage
## URL of the project's home page
##
## @igwn_named_param{DESCRIPTION}
## Single line description of the project
##
## @igwn_named_param{LANGUAGES}
## List of languages used by the project as used by the CMake project() function
##
## @code{.cmake}
## cm_prereq(3.2)
## cm_init(
##   MyProject
##   1.0.0
##   https://myproject.org/bugs
##   MyProject
##   https://myproject.org/
##   )
## @endcode
##
## @author Edward Maros
## @date   2019-2020
## @igwn_copyright
## */
## /** @igwn_group_end */
## cm_init( package version bugreport tarname homepage );
macro(CM_INIT package version bug_report tarname homepage )
  set( largn "${ARGN}")
  set(options
    )
  set(oneValueArgs
    DESCRIPTION
    )
  set(multiValueArgs
    LANGUAGES
    LONG_DESCRIPTION
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${largn} )

  # package version bug_report tarname homepage
  cx_msg_debug_variable( largn )
  cx_msg_debug_variable( ARG_LANGUAGES )
  cx_msg_debug_variable( ARG_UNPARSED_ARGUMENTS )

  list(FIND largn "LANGUAGES" have_languages)
  cm_msg_notice("ARG_LANGUAGES: '${ARG_LANGUAGES}' have_languages: ${have_languages} ARGN: ${ARGN}")
  if ( NOT have_languages EQUAL -1 )
    list(INSERT ARG_LANGUAGES 0 LANGUAGES )
  endif( )
  cm_msg_notice("ARG_LANGUAGES: '${ARG_LANGUAGES}' have_languages: ${have_languages}")

  ci_set_policy( CMP0048 NEW )

  project(${package}
    ${ARG_LANGUAGES}
    )
  #----------------------------------------------------------------------
  # 
  #----------------------------------------------------------------------
  cm_init_set_var_( IGWN_TOP_DIR "${CMAKE_INSTALL_PREFIX}" )
  cm_init_set_var_( IGWN_CMAKE_SCRIPTDIR "${IGWN_TOP_DIR}/scripts" )
  #----------------------------------------------------------------------
  #----------------------------------------------------------------------
  if ( ARG_DESCRIPTION )
    set( PROJECT_DESCRIPTION ${ARG_DESCRIPTION} CACHE STRING "Project Description" FORCE )
    set( PACKAGE_DESCRIPTION ${ARG_DESCRIPTION} CACHE STRING "Project Description" FORCE )
  endif ( )
  if ( ARG_LONG_DESCRIPTION )
    set( PROJECT_LONG_DESCRIPTION ${ARG_DESCRIPTION} CACHE STRING "Project Long Description" FORCE )
    set( PACKAGE_LONG_DESCRIPTION ${ARG_DESCRIPTION} CACHE STRING "Project Long Description" FORCE )
  endif ( )
  if ( NOT tarname )
    string( REGEX REPLACE "^GNU " "" tarname ${package} )
    string( TOLOWER ${tarname} tarname )
    string( REGEX REPLACE "[^A-Za-z0-9_]" "-" tarname ${tarname} )
  endif( NOT tarname )

  #----------------------------------------------------------------------
  # Initialize the autoheader components
  #----------------------------------------------------------------------

  unset( CH_TOP_TEXT CACHE )
  unset( CH_BOTTOM_TEXT CACHE )
  unset( CH_BODY CACHE )

  #----------------------------------------------------------------------
  # Set everything else to keep backwards compatability
  #----------------------------------------------------------------------
  cm_cache( PROJECT_NAME VALUE ${package} SCOPE_LOCAL )
  cm_cache( PROJECT_VERSION VALUE ${version} SCOPE_LOCAL )
  string(REPLACE "." ";" version_list "${version}")
  list(LENGTH version_list version_len)
  if ( ${version_len} GREATER 0 )
    list( GET version_list 0 PROJECT_VERSION_MAJOR )
    if ( ${version_len} GREATER 1 )
      list( GET version_list 1 PROJECT_VERSION_MINOR )
      if ( ${version_len} GREATER 2 )
	      list( GET version_list 2 PROJECT_VERSION_PATCH )
	      if ( ${version_len} GREATER 3 )
	        list( GET version_list 3 PROJECT_VERSION_TWEAK )
	      endif ( ${version_len} GREATER 3 )
      endif ( ${version_len} GREATER 2 )
    endif ( ${version_len} GREATER 1 )
  endif ( ${version_len} GREATER 0 )

  cm_cache( PROJECT_VERSION_MAJOR )
  cm_cache( PROJECT_VERSION_MINOR )
  cm_cache( PROJECT_VERSION_PATCH )
  cm_cache( PROJECT_VERSION_TWEAK )
  cm_cache( PROJECT_${PROJECT_NAME}_VERSION_MAJOR VALUE ${PROJECT_VERSION_MAJOR} )
  cm_cache( PROJECT_${PROJECT_NAME}_VERSION_MINOR VALUE ${PROJECT_VERSION_MINOR} )
  cm_cache( PROJECT_${PROJECT_NAME}_VERSION_PATCH VALUE ${PROJECT_VERSION_PATCH} )
  cm_cache( PROJECT_${PROJECT_NAME}_VERSION_TWEAK VALUE ${PROJECT_VERSION_TWEAK} )

  cx_msg_debug_variable( PROJECT_NAME )
  cx_msg_debug_variable( PROJECT_VERSION )
  cx_msg_debug_variable( PROJECT_VERSION_MAJOR )
  cx_msg_debug_variable( PROJECT_VERSION_MINOR )
  cx_msg_debug_variable( PROJECT_VERSION_PATCH )
  cx_msg_debug_variable( PROJECT_VERSION_TWEAK )

  #----------------------------------------------------------------------
  # Things for an autotools look and feel
  #----------------------------------------------------------------------
  ci_project_config( INIT )
  cm_define(
    VARIABLE PACKAGE
    VALUE "\"${tarname}\""
    DESCRIPTION "Name of package" )
  cm_define(
    VARIABLE PACKAGE_BUGREPORT
    VALUE "\"${bugreport}\""
    DESCRIPTION "Define to the address where bug reports for this package should be sent." )
  cm_define(
    VARIABLE PACKAGE_NAME
    VALUE "\"${tarname}\""
    DESCRIPTION "Define to the full name of this package." )
  cm_define(
    VARIABLE PACKAGE_STRING
    VALUE "\"${tarname} ${version}\""
    DESCRIPTION "Define to the full name and version of this package." )
  cm_define(
    VARIABLE PACKAGE_TARNAME
    VALUE "\"${tarname}\""
    DESCRIPTION "Define to the one symbol short name of this package." )
  cm_define(
    VARIABLE PACKAGE_URL
    VALUE "\"${homepage}\""
    DESCRIPTION "Define to the home page for this package." )
  cm_define(
    VARIABLE PACKAGE_VERSION
    VALUE "\"${version}\""
    DESCRIPTION "Define to the version of this package." )
  cm_define(
    VARIABLE VERSION
    VALUE "\"${version}\""
    DESCRIPTION "Version number of package" )
  #----------------------------------------------------------------------
  # if the docdir has been set before the project_name was known,
  #   correct it here
  #----------------------------------------------------------------------
  if ( DEFINED CMAKE_INSTALL_DOCDIR )
    string( REGEX REPLACE "Project$" "${PROJECT_NAME}" new_value "${CMAKE_INSTALL_DOCDIR}" )
    set( CMAKE_INSTALL_DOCDIR "${new_value}" CACHE INTERNAL "" FORCE )
  endif( )
  if ( DEFINED CMAKE_INSTALL_FULL_DOCDIR )
    string( REGEX REPLACE "Project$" "${PROJECT_NAME}" new_value "${CMAKE_INSTALL_FULL_DOCDIR}" )
    set( CMAKE_INSTALL_FULL_DOCDIR "${new_value}" )
  endif( )

endmacro(CM_INIT)
