#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_msg_warn( )
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/cm_msg_error )

function( cm_check_progs var )
  set(options REQUIRED
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )

  cmake_parse_arguments( ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  find_program(
    ${var}
    NAMES ${ARG_UNPARSED_ARGUMENTS} )
  if ( ARG_REQUIRED AND NOT ${var} )
    list( LENGTH ARG_UNPARSED_ARGUMENTS L )
    if ( ${L} EQUAL 1 )
      cm_msg_error( "Unable to find the program: ${ARG_UNPARSED_ARGUMENTS}" )
    else( )
      cm_msg_error( "Unable to find any of the programs: ${ARG_UNPARSED_ARGUMENTS}" )
    endif( )
  endif( )

endfunction( )
