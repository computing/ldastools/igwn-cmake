include( CheckPrototypeDefinition)
include( Autotools/Internal/ci_result )
include( Autotools/cm_check_funcs )
include( Autotools/cm_define )
include( Autotools/ArchiveX/cx_msg_debug_variable )

function(CM_FUNC_STRERROR_R)
  check_prototype_definition(
    strerror_r
    "char *strerror_r( int errnum, char *strerrbuf, size_t buflen)"
    "(char*)(NULL)"
    "string.h"
    HAVE_DECL_STRERROR_R )
  if ( NOT HAVE_DECL_STRERROR_R )
    unset( HAVE_DECL_STRERROR_R CACHE )
    check_prototype_definition(
      strerror_r
      "int strerror_r( int errnum, char *strerrbuf, size_t buflen)"
      "(int)(NULL)"
      "string.h"
      HAVE_DECL_STRERROR_R )
    if ( NOT HAVE_DECL_STRERROR_R )
      set( HAVE_DECL_STRERROR_R 0 )
    endif ( NOT HAVE_DECL_STRERROR_R )
  endif ( NOT HAVE_DECL_STRERROR_R )
  ci_result( HAVE_DECL_STRERROR_R result )
  message( STATUS "Checking for strerror_r prototype ... ${result}" )
  cm_define(
    VARIABLE HAVE_DECL_STRERROR_R
    DESCRIPTION
      "Define to 1 if you have the declaration of `strerror_r', and to 0 if you"
      "don't." )
  ci_result( HAVE_DECL_STRERROR_R result )
  cm_check_funcs(strerror_r)
  set( inc "
#include <string.h>
")

  set( body "
int err = 5;
char buffer[256];
char* retval;

retval = strerror_r(err, buffer, sizeof( buffer ) );
")
  cm_try_compile( "${inc}" "${body}" STRERROR_R_CHAR_P)
  cm_define(
    VARIABLE STRERROR_R_CHAR_P
    DESCRIPTION "Define to 1 if strerror_r returns char *." )
  cx_msg_debug_variable( STRERROR_R_CHAR_P )
endfunction(CM_FUNC_STRERROR_R)
