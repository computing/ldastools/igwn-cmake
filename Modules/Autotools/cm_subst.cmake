#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_subst( )
#------------------------------------------------------------------------
function( CM_SUBST _VARIABLE _VALUE)
  if ( NOT _VALUE )
    set(_VALUE ${${_VARIABLE}} )
  endif ( NOT _VALUE )
  set(${_VARIABLE} ${_VALUE}
    CACHE INTERNAL "" )
endfunction( CM_SUBST )
