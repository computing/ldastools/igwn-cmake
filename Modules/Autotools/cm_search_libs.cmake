#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )
include( CheckFunctionExists )
include( CheckLibraryExists )
include( Autotools/ArchiveX/cx_msg_debug )

function(cm_search_libs)
  set(options)
  set(oneValueArgs
    FUNCTION
    VARIABLE )
  set(multiValueArgs
    SEARCH_LIBS
    EXTRA_LIBS )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  set( saved_libs ${LIBS} )
  if ( EXTRA_LIBS )
    set( LIBS ${EXTRA_LIBS} ${LIBS} )
  endif ( EXTRA_LIBS )
  check_function_exists( ${ARG_FUNCTION} have_func_in_lib )
  if ( NOT have_func_in_lib )
    foreach( lib ${ARG_SEARCH_LIBS} )
      cx_msg_debug( "find_lib - ${lib}" )
      find_library( have_lib ${lib} )
      cx_msg_debug( STATUS "find_lib have_lib - ${have_lib}" )
      if ( have_lib )
        cx_msg_debug( "check_library_exists - ${lib} ${ARG_FUNCTION}" )
        check_library_exists( ${lib} ${ARG_FUNCTION} "" have_func_in_lib )
        if ( have_func_in_lib )
	        set( saved_libs ${lib} ${LIBS} )
	        if ( ARG_VARIABLE )
	          set( ${ARG_VARIABLE} ${lib} PARENT_SCOPE )
	        endif( ARG_VARIABLE )
	        break()
        endif ( have_func_in_lib )
      endif ( have_lib )
    endforeach( lib )
  endif( )
  set( LIBS ${saved_libs} )
  cx_msg_debug_variable( LIBS )

endfunction(cm_search_libs)
