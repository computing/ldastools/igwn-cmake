#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_prereq( version )
#------------------------------------------------------------------------
include( CMakeParseArguments )

include(Autotools/cm_msg_failure)

set(IGWN_CMAKE_VERSION_MINIMUM 3.0.2 CACHE INTERNAL "Minimum CMake version required by IGWN CMake package")

## /**
## @igwn_group_add{CMakeVersioning,}
## @igwn_group_begin
## */
## /**
## @fn cm_prereq( version )
## @brief Verify minimum CMake version
## @details
## Verify that the system is running at least the specified version of CMake
##
## @param version
## Minimum version requirement
##
## @code{.cmake}
## cm_prereq(3.2)
## @endcode
##
## @author Edward Maros
## @date   2019-2020
## @igwn_copyright
## */
## /** @igwn_group_end */
## cm_prereq( version );
macro(cm_prereq version )
  if(${CMAKE_VERSION} VERSION_LESS ${version})
    cm_msg_failure("cmake is at version ${CMAKE_VERSION} but ${version} was requested as a minimum")
  elseif(${CMAKE_VERSION} VERSION_LESS ${IGWN_CMAKE_VERSION_MINIMUM})
    cm_msg_failure("cmake is at version ${CMAKE_VERSION} but this collection of macros require ${IGWN_CMAKE_VERSION_MINIMUM} as a minimum")
  endif()
  cmake_minimum_required(VERSION ${version} FATAL_ERROR)
endmacro()
