include( TestBigEndian )
include( Autotools/cm_subst )

## /**
## @igwn_group{CCompiler,}
## @igwn_group_begin
## */
## /**
## @fn cm_c_bigendian( )
## @brief Check if system is bigendian
## @details
## Check if words are stored with the most significant bit first.
##
## The variable WORDS_BIGENDIAN will be set to one (1) if the most significant bit is first,
## zero (0) otherwise.
## The variable WORDS_BIGENDIAN can be used in @ substitution.
##
## @code{.cmake}
## cm_c_bigendian( )
## @endcode
##
## @author Edward Maros
## @date   2019-2020
## @igwn_copyright
## */
## /** @igwn_group_end */
## cm_c_bigendian( );
function( CM_C_BIGENDIAN )
  unset( _RESULT CACHE )
  test_big_endian( _RESULT )
  if ( _RESULT )
    set( WORDS_BIGENDIAN 1 )
  else( )
    set( WORDS_BIGENDIAN 0 )
  endif ( _RESULT )
  unset( _RESULT CACHE )
  cm_subst( WORDS_BIGENDIAN ${WORDS_BIGENDIAN} )
endfunction( CM_C_BIGENDIAN )
