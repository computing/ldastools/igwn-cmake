#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# ac_project_config( [ FILENAME <name_of_file> ] [ WRITE <line> [<line> ...]
#                    [VARIABLE <variable> [<value>]] )
#------------------------------------------------------------------------
include( CMakeParseArguments )

function( CI_PROJECT_CONFIG )
  # cx_msg_debug( "CI_PROJECT_CONFIG: ${ARGN}" )
  set(options INIT)
  set(oneValueArgs FILENAME)
  set(multiValueArgs WRITE VARIABLE PREFIX)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( ARG_FILENAME )
    if ( CI_PROJECT_CONFIG_FILENAME )
      #------------------------------------------------------------------
      # Set up to preserve any work already done
      #------------------------------------------------------------------
      set( _old_name ${CI_PROJECT_CONFIG_FILENAME} )
    endif ( CI_PROJECT_CONFIG_FILENAME )
    set(
      CI_PROJECT_CONFIG_FILENAME ${ARG_FILENAME}
      CACHE INTERNAL "Internal" FORCE )
    if ( _old_name AND EXISTS ${_old_name} )
      #------------------------------------------------------------------
      # Save the work
      #------------------------------------------------------------------
      get_filename_component( new_dir ${CI_PROJECT_CONFIG_FILENAME} DIRECTORY )
      if ( NOT EXISTS ${new_dir} )
	file( MAKE_DIRECTORY ${new_dir} )
      endif ( NOT EXISTS ${new_dir} )
      file( RENAME ${_old_name} ${CI_PROJECT_CONFIG_FILENAME} )
    else ( _old_name AND EXISTS ${_old_name} )
      #------------------------------------------------------------------
      # Scrub the file
      #------------------------------------------------------------------
      file(WRITE ${CI_PROJECT_CONFIG_FILENAME} "")
    endif ( _old_name AND EXISTS ${_old_name} )
  endif( ARG_FILENAME )
  if ( NOT CI_PROJECT_CONFIG_FILENAME )
    #--------------------------------------------------------------------
    # Establish default
    #--------------------------------------------------------------------
    set(
      CI_PROJECT_CONFIG_FILENAME "${CMAKE_CURRENT_BINARY_DIR}/config.h.in"
      CACHE INTERNAL "Internal" FORCE )
    file(WRITE ${CI_PROJECT_CONFIG_FILENAME} "")
  endif ( NOT CI_PROJECT_CONFIG_FILENAME )
  if ( ARG_INIT )
    file(WRITE ${CI_PROJECT_CONFIG_FILENAME} "")
  endif ( ARG_INIT )
  if ( ARG_PREFIX )
    set( _tmp_filename "${CMAKE_CURRENT_BINARY_DIR}/apc.cmake_tmp" )
    file( WRITE ${_tmp_filename} "" )
    foreach( line ${ARG_PREFIX} )
      file(APPEND ${_tmp_filename} "${line}\n")
    endforeach( line )
    file( READ "${CI_PROJECT_CONFIG_FILENAME}" contents )
    file( APPEND ${_tmp_filename} "${contents}" )
    file( RENAME "${_tmp_filename}" "${CI_PROJECT_CONFIG_FILENAME}" )
  endif ( ARG_PREFIX )
  if ( ARG_WRITE )
    foreach( line ${ARG_WRITE} )
      file(APPEND ${CI_PROJECT_CONFIG_FILENAME} "${line}\n")
    endforeach( line )
  endif ( ARG_WRITE )
  if ( ARG_VARIABLE )
    list( LENGTH ARG_VARIABLE len )
    if ( ${len} GREATER 0 )
      list( GET ARG_VARIABLE 0 var )
    endif ( ${len} GREATER 0 )
    if ( ${len} GREATER 1 )
      list( GET ARG_VARIABLE 1 value )
    endif ( ${len} GREATER 1 )
    if ( ${CI_PROJECT_CONFIG_FILENAME} MATCHES "[.]in$" )
      file(APPEND
	${CI_PROJECT_CONFIG_FILENAME}
	"#cmakedefine ${var} @${var}@\n")
    else ( ${CI_PROJECT_CONFIG_FILENAME} MATCHES "[.]in$" )
      if ( ${var} )
	file(APPEND
	  ${CI_PROJECT_CONFIG_FILENAME}
	  "#define ${var} ${${var}}\n")
      else ( ${var} )
	file(APPEND
	  ${CI_PROJECT_CONFIG_FILENAME}
	  "/* #undef ${var} */o\n")
      endif ( ${var} )
    endif ( ${CI_PROJECT_CONFIG_FILENAME} MATCHES "[.]in$" )
    file(APPEND ${CI_PROJECT_CONFIG_FILENAME} "\n")
  endif ( ARG_VARIABLE )

endfunction( CI_PROJECT_CONFIG )
