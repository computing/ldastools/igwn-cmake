#========================================================================
# -*- mode: cmake -*-
#
#------------------------------------------------------------------------
CMAKE_MINIMUM_REQUIRED(VERSION 3.10)

set( MODULE_SOURCES
  ci_cache.cmake
  ci_cache_list.cmake
  ci_join.cmake
  ci_project_config.cmake
  ci_result.cmake
  ci_set_policy.cmake
  )

ci_create_macro_file(
  TAG AutotoolsInternal
  MODULES ${MODULE_SOURCES}
  MODULE_DIR "Autotools/Internal"
  INSTALL_DIR "${IGWNCMAKE_INSTALL_DATADIR}/cmake/Modules"
  )
