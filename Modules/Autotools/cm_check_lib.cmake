#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )
include( CheckLibraryExists )
include( Autotools/cm_define )
include( Autotools/cm_msg_checking )
include( Autotools/cm_msg_result )

## /**
## @igwn_group{Library Files,}
## @igwn_group_begin
## */
##
## /**
## @fn cm_check_lib( library, function )
## @brief Check for the presense of library archive files
##
## @code{.cmake}
## cm_check_lib( zstd ZSTD_compress )
## #endcode
##
## @author Edward Maros
## @date   2022
## @igwn_copyright
## */
function(CM_CHECK_LIB _LIBRARY _FUNCTION)
  set(options)
  set(oneValueArgs)
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  string(TOUPPER ${_LIBRARY} _LIBRARY_UPPER)
  unset( _RESULT CACHE )
  unset( _LIB CACHE )
  find_library( _LIB ${_LIBRARY} )
  if ( _LIB )
    check_library_exists( ${_LIB} ${_FUNCTION} "" _RESULT)
    if ( _RESULT )
      set( LIBS ${_LIB} ${LIBS} )
      set( HAVE_LIB${_LIBRARY_UPPER} 1 CACHE INTERNAL "Internal" )
    endif ( _RESULT )
  endif( )
  unset( _LIB CACHE )
  cm_define(
    VARIABLE HAVE_LIB${_LIBRARY_UPPER}
    DESCRIPTION "Define to 1 if you have `${_LIBRARY}' library (-l${_LIBRARY})"  )

  if ( _RESULT )
    set( _RESULT "yes" )
  else ( _RESULT )
    set( _RESULT "no" )
  endif ( _RESULT )
  cm_msg_checking( "if library ${_LIBRARY} contains ${_FUNCTION}" )
  cm_msg_result( ${_RESULT} )

endfunction(CM_CHECK_LIB)
## /** @igwn_group_end */
