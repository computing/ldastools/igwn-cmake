include( Autotools/cm_check_headers )

function( CM_CHECK_HEADERS_DEFAULT_AUTOTOOLS )
  cm_check_headers(
      sys/types.h
      sys/stat.h
      stdlib.h
      string.h
      memory.h
      strings.h
      inttypes.h
      stdint.h
      unistd.h )
endfunction( CM_CHECK_HEADERS_DEFAULT_AUTOTOOLS )
