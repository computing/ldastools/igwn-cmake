#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_mktemp( )
#------------------------------------------------------------------------
include( CMakeParseArguments )

## /**
## @fn cx_mktemp( temp_file_variable )
## @brief Creates a temporary filename
## @details
## This only creates the name of a temporary file.
## It does not create the temporary file.
##
## @param temp_file_variable
## Variable in which to store the temporary filename
##
## @code{.cmake}
## cx_mktemp(  )
## @endcode
## @author Edward Maros
## @date   2022
## @igwn_copyright
## */
## cx_mktemp( temp_file_variable )
function( cx_mktemp temp_file_variable )
  if (DEFINED ENV{TMPDIR})
    set(TEMP_DIR "$ENV{TMPDIR}")
  elseif (DEFINED ENV{TEMP})
    set(TEMP_DIR "$ENV{TEMP}")
  elseif (DEFINED ENV{TMP})
    set(TEMP_DIR "$ENV{TMP}")
  else()
    # Default fallback if none of the above is defined
    set(TEMP_DIR "/tmp")
  endif()
  # Generate a unique identifier based on date and time
  string(TIMESTAMP CURRENT_DATETIME "%Y%m%d%H%M%S")

  # Create a temporary directory path
  set(TEMP_FILE "${TEMP_DIR}/cmake_temp_${CURRENT_DATETIME}")

  # Make sure the path uses native file separators
  file(TO_NATIVE_PATH ${TEMP_FILE} TEMP_FILE_NATIVE)

  set( ${temp_file_variable} ${TEMP_FILE_NATIVE} PARENT_SCOPE )
endfunction( )
