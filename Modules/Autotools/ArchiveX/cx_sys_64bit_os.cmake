#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )

set( test_filename "${CMAKE_CURRENT_BINARY_DIR}/cmake_cx_sys_64bit_os.c" )
file(
  WRITE "${test_filename}"
  "int main () {
    if (sizeof(void*) == 8) {
      return 0;
    }
    return 1; }"
  )
try_run(
  RESULT COMPILE_RESULT
  ${CMAKE_CURRENT_BINARY_DIR}
  "${test_filename}" )
if( RESULT EQUAL 0 )
  set( RESULT "yes" )
  set( HAVE_64BIT_OS 1 )
  if( EXISTS "/usr/lib/amd64" )
    set( LIB_64_DIR "/amd64" )
  elseif( EXISTS "/usr/lib/sparcv9" )
    set( LIB_64_DIR "/sparcv9" )
  elseif( EXISTS "/usr/lib64" )
    set( LIB_64_DIR "64" )
  else( EXISTS "/usr/lib/amd64" )
    set( LIB_64_DIR "64" )
  endif( EXISTS "/usr/lib/amd64" )
else( RESULT EQUAL 0 )
  set( RESULT "no" )
endif( RESULT EQUAL 0 )
set(
  HAVE_64BIT_OS "${HAVE_64BIT_OS}"
  CACHE INTERNAL "Internal"
  )
set(
  LIB_64_DIR "${LIB_64_DIR}"
  CACHE INTERNAL "Internal"
  )
message( STATUS "Checking for 64-bit OS ... ${RESULT}" )
if ( LIB_64_DIR )
  message( STATUS "Checking for lib 64 dir ... ${LIB_64_DIR}" )
endif ( LIB_64_DIR )
