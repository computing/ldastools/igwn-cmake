#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/cm_msg_notice )

set(
  CX_MSG_DEBUG_VERBOSE False
  CACHE BOOL "Enable to have debug messages be verbose"
)

## /**
## @igwn_group_PrintingMessages
## @igwn_group_begin
## */
## /**
## @fn cx_msg_debug( txt )
## @brief Display debugging message
## @details
## This command will display the txt on the screen
## if the global variable CM_MSG_DEBUG_VERBOSE is True
##
## @param  txt
##    If a list, then a collection of items to be displayed
##    If a string, then a text string to be displayed
##
## @note
## These messages will only be displayed if the package was configured
## with CM_MSG_DEBUG_VEROSE being defined as True
##
## @sa cx_msg_debug_variable
##
## @code{.cmake}
## cx_msg_debug( "hello world" )
## @endcode
## @author Edward Maros
## @date   2019-2020
## @igwn_copyright
## */
## /** @igwn_group_end */
## cx_msg_debug( txt );
function(cx_msg_debug txt)
  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( CX_MSG_DEBUG_VERBOSE )
    cm_msg_notice( "+++ DEBUG +++ ${txt}" )
  endif( )
endfunction(cx_msg_debug)
