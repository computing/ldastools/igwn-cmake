#========================================================================
# -*- mode: cmake; -*-
#------------------------------------------------------------------------
#
# cx_check_sizes( )
#
#------------------------------------------------------------------------
include( Autotools/cm_define )
include( Autotools/cm_check_sizeof )

function(CX_CHECK_SIZES)

  cm_check_sizeof( "short"     0 )
  cm_check_sizeof( "int"       0 )
  cm_check_sizeof( "long"      0 )
  cm_check_sizeof( "long long" 0 )
  cm_check_sizeof( "float"     0 )
  cm_check_sizeof( "double"    0 )
  cm_check_sizeof( "void*"     0 )
  cm_check_sizeof( "pid_t"     0 )
  cm_check_sizeof( "size_t"    0 )

  # ---------------------------------------------------------------------
  #   2 byte integer
  # ---------------------------------------------------------------------
  if( SIZEOF_SHORT EQUAL 2 )
    set(LDAS_2_BYTE_INT short)
  endif( SIZEOF_SHORT EQUAL 2 )
  cm_define(
    VARIABLE LDAS_2_BYTE_INT
    DESCRIPTION "Data type to use to represent a 2 byte integer" )

  # ---------------------------------------------------------------------
  #   4 byte integer
  # ---------------------------------------------------------------------
  if( SIZEOF_INT EQUAL 4 )
    set(LDAS_4_BYTE_INT int)
  elseif( SIZEOF_LONG EQUAL 4 )
    set(LDAS_4_BYTE_INT long)
  endif( SIZEOF_INT EQUAL 4 )
  cm_define(
    VARIABLE LDAS_4_BYTE_INT
    DESCRIPTION "Data type to use to represent a 4 byte integer" )

  # ---------------------------------------------------------------------
  #   8 byte integer
  # ---------------------------------------------------------------------
  if( SIZEOF_INT EQUAL 8 )
    set(LDAS_8_BYTE_INT int)
  elseif( SIZEOF_LONG EQUAL 8 )
    set(LDAS_8_BYTE_INT long)
  elseif( SIZEOF_LONG_LONG EQUAL 8 )
    set(LDAS_8_BYTE_INT "long long")
  endif( SIZEOF_INT EQUAL 8 )
  cm_define(
    VARIABLE LDAS_8_BYTE_INT
    DESCRIPTION "Data type to use to represent a 8 byte integer" )

  # ---------------------------------------------------------------------
  #   4 byte real
  # ---------------------------------------------------------------------
  if( SIZEOF_FLOAT EQUAL 4 )
    set(LDAS_4_BYTE_REAL float)
    set(LDAS_4_BYTE_REAL_DIGITS FLT_DIG)
  endif( SIZEOF_FLOAT EQUAL 4 )
  cm_define(
    VARIABLE LDAS_4_BYTE_REAL
    DESCRIPTION "Data type to use to represent a 4 byte real" )
  cm_define(
    VARIABLE LDAS_4_BYTE_REAL_DIGITS
    DESCRIPTION "Number of digits for a 4 byte real" )

  # ---------------------------------------------------------------------
  #   8 byte real
  # ---------------------------------------------------------------------
  if( SIZEOF_DOUBLE EQUAL 8 )
    set(LDAS_8_BYTE_REAL double)
    set(LDAS_8_BYTE_REAL_DIGITS DBL_DIG)
  endif( SIZEOF_DOUBLE EQUAL 8 )
  cm_define(
    VARIABLE LDAS_8_BYTE_REAL
    DESCRIPTION "Data type to use to represent a 8 byte real" )
  cm_define(
    VARIABLE LDAS_8_BYTE_REAL_DIGITS
    DESCRIPTION "Number of digits for a 8 byte real" )

  # ---------------------------------------------------------------------
  #   memory pointer
  # ---------------------------------------------------------------------
  if( SIZEOF_VOIDP EQUAL 4 )
    set(LDAS_VOIDP_INT_TYPE INT_4U)
  elseif( SIZEOF_VOIDP EQUAL 8 )
    set(LDAS_VOIDP_INT_TYPE INT_8U)
  endif( SIZEOF_VOIDP EQUAL 4 )
  cm_define(
    VARIABLE LDAS_VOIDP_INT_TYPE
    DESCRIPTION "Integer type needed to hold a void pointer" )

  # ---------------------------------------------------------------------
  #   Check the side effects for header files
  # ---------------------------------------------------------------------
  cm_check_headers( "sys/types.h" )
  cm_check_headers( "stddef.h" )
  cm_check_headers( "stdint.h" )
endfunction(CX_CHECK_SIZES)
