#========================================================================
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )

function( cx_target_syncronize )
  set(options)
  set(oneValueArgs
    SOURCE_DIR
    DESTINATION_DIR
    )
  set(multiValueArgs
    FILES
    )

  cmake_parse_arguments(
    ARG
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  #======================================================================
  # Setup
  #----------------------------------------------------------------------
  string( REPLACE "${CMAKE_SOURCE_DIR}" "" target_suffix "${ARG_DESTINATION_DIR}" )
  string( REPLACE "/" "_" target_suffix "${target_suffix}" )
  #----------------------------------------------------------------------
  # Establish default values
  #----------------------------------------------------------------------

  #----------------------------------------------------------------------
  # Iterate over list of files, copying as needed
  #----------------------------------------------------------------------
  add_custom_target( sync_files_${target_suffix}
    WORKING_DIRECTORY ${ARG_SOURCE_DIR}
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${ARG_FILES} ${ARG_DESTINATION_DIR}
    )
  if ( NOT TARGET sync-files )
    add_custom_target( sync-files)
  endif( )
  if ( TARGET sync-files )
    add_dependencies( sync-files sync_files_${target_suffix} )
  endif( )
endfunction( )
