# -----------------------------------------------------------------------
# -----------------------------------------------------------------------
include( Autotools/ArchiveX/cx_prog_changed_files )

function( cx_target_changed_files_push )
  cx_prog_changed_files( )

  set( options
    )
  set( oneValueArgs
    GIT_PREFIX
    INSTALLED_PREFIX
    TARGET_PREFIX
    LINK_PREFIX
    USER
    GROUP
    )
  set( multiValueArgs
    FILES
    PERMISSIONS
    )

  cmake_parse_arguments( ARG
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  set( target ${ARG_TARGET_PREFIX}-push-changed-files )

  if ( ARG_USER )
    set( USER_OPTION "-DUSER_OPTION=${ARG_USER}" )
  endif( )
  if ( ARG_GROUP )
    set( GROUP_OPTION "-DGROUP_OPTION=${ARG_GROUP}" )
  endif( )
  if ( ARG_PERMISSIONS )
    set( PERMISSIONS_OPTION "-DPERMISSIONS_OPTION='${ARG_PERMISSIONS}'" )
  endif( )
  if ( ARG_LINK_PREFIX )
    set( LINK_PREFIX "-DLINK_PREFIX='${ARG_LINK_PREFIX}'" )
  endif( )

  add_custom_target(
    ${target}
    ${CMAKE_COMMAND}
    -DMODE=push
    "-DGIT_PREFIX=${ARG_GIT_PREFIX}"
    "-DINSTALLED_PREFIX=${ARG_INSTALLED_PREFIX}"
    "-DFILES='${ARG_FILES}'"
    ${USER_OPTION} ${GROUP_OPTION} ${PERMISSIONS_OPTION}
    ${LINK_PREFIX}
    -P ${PROGRAM_CHANGED_FILES}
    )

  if ( NOT TARGET push-changed-files )
    add_custom_target( push-changed-files )
  endif( )

  add_dependencies( push-changed-files ${target} )
endfunction( )
