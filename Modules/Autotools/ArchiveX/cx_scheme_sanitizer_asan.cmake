# ASan-specific options
set(ASAN_OPTIONS_LIST
  ENABLE
  ENABLE_USE_AFTER_SCOPE
  DISABLE_DETECT_HEAP_USE_AFTER_FREE
  DISABLE_DETECT_ODR_VIOLATION
  DISABLE_LEAK_DETECTION
  DISABLE_RECOVERY
  DISABLE_STACK_USE_AFTER_RETURN
  DISABLE_USE_AFTER_FREE
)
set(ASAN_ONE_VALUE_ARGS
  OUTPUT
  SUPPRESSION_FILE
  IGNORELIST_FILE
)
set(ASAN_MULTI_VALUE_ARGS
  FLAGS
)

macro(cl_extend_asan_options environment )
  if ( DEFINED ENV{ASAN_OPTIONS})
    set( ENV{ASAN_OPTIONS} "$ENV{ASAN_OPTIONS}:${environment}" )
  else()
    set( ENV{ASAN_OPTIONS} "${environment}" )
  endif()
endmacro()

function(cx_scheme_sanitizer_asan)
  # Parse arguments for AddressSanitizer
  cmake_parse_arguments(CX_ASAN "${ASAN_OPTIONS_LIST}" "${ASAN_ONE_VALUE_ARGS}" 
    "${ASAN_MULTI_VALUE_ARGS}" ${ARGN}
  )

  # Append AddressSanitizer to the global list if enabled
  if(CX_ASAN_ENABLE)
    list(APPEND CX_SCHEME_SANITIZERS_ENABLED "address")
  endif()

  # Handle ASan-specific options
  if(CX_ASAN_ENABLE_USE_AFTER_SCOPE)
    list(APPEND ASAN_FLAGS "-fsanitize-address-use-after-scope")
  endif()

  if(CX_ASAN_DISABLE_RECOVERY)
    list(APPEND ASAN_FLAGS "-fno-sanitize-recover=address")
  endif()

  if(CX_ASAN_FLAGS)
    list(APPEND ASAN_FLAGS "${CX_ASAN_FLAGS}")
  endif()

  # Set ignorelist file if provided
  if(CX_ASAN_IGNORELIST_FILE)
    list(APPEND ASAN_FLAGS "-fsanitize-blacklist=${CX_ASAN_IGNORELIST_FILE}")
  endif()

  # Set suppression file if provided
  if(CX_ASAN_SUPPRESSION_FILE)
    cl_extend_asan_options("suppressions=${CX_ASAN_SUPPRESSION_FILE}")
  endif()

  if ( CX_ASAN_DISABLE_DETECT_HEAP_USE_AFTER_FREE )
    cl_extend_asan_options("detect_heap_use_after_free=0")
  endif( )

  if ( CX_ASAN_DISABLE_DETECT_ODR_VIOLATION )
    cl_extend_asan_options("detect_odr_violation=0")
  endif( )

  # Set debugging verbosity level
  if (SANITIZERS_DEBUG_LEVEL)
    cl_extend_asan_options("verbosity=${SANTIZER_DEBUG_LEVEL}")
  endif( )

  if(CX_ASAN_DISABLE_LEAK_DETECTION)
    cl_extend_asan_options("detect_leaks=0")
  endif()

  if(CX_ASAN_DISABLE_STACK_USE_AFTER_RETURN)
    cl_extend_asan_options("detect_stack_use_after_return=0")
  endif()

  if(CX_ASAN_DISABLE_USE_AFTER_FREE)
    cl_extend_asan_options("detect_use_after_free=0")
  endif()

  # Set the generated ASan flags to the output or globally
  if(CX_ASAN_OUTPUT)
    set(${CX_ASAN_OUTPUT} "${ASAN_FLAGS}" PARENT_SCOPE)
  else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ASAN_FLAGS}")
  endif()
endfunction()
