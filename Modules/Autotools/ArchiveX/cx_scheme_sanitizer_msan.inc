<H1>MemorySanitizer (MSan)</H1>

<H2>Purpose</H2>
<p>MemorySanitizer (MSan) detects <b>uninitialized memory reads</b>, one of
the most common sources of unpredictable behavior in C++ programs.
MSan helps identify parts of your program where variables or memory regions
are used before being properly initialized.</p>

<H2>When to Use</H2>
<p>MSan is essential when working on a project where the initialization of
complex structures or arrays is critical, and you need to ensure that no
uninitialized memory is being accessed.</p>

<H2>Key Options</H2>
<ul>
  <li><b>ENABLE</b>: Turn on MemorySanitizer.</li>
  <li><b>DISABLE_RECOVERY</b>: Ensures that the program terminates
  immediately when an uninitialized memory read is detected.</li>
  <li><b>SUPPRESSION_FILE</b>: Use a suppression file to ignore known issues
  (e.g., external library problems).</li>
</ul>

<H2>Typical Use Case</H2>
\code
cx_scheme_sanitizer_msan(ENABLE)
\endcode

<H2>Benefits</H2>
<ul>
  <li>Detect uninitialized memory issues that can cause non-deterministic
  behavior.</li>
  <li>Gain confidence that all memory allocations in your code are properly
  initialized before use.</li>
</ul>

