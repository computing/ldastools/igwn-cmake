#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------

include( Autotools/ArchiveX/cx_msg_debug_variable )

##
# @brief Appends additional flags to the C++ compiler flags.
#
# This function takes one or more compiler options and appends them to the
# existing `CMAKE_CXX_FLAGS` variable. It uses `cmake_parse_arguments` to
# parse the options provided.
#
# @param[in] OPTIONS A list of compiler options to append to `CMAKE_CXX_FLAGS`.
#
# @note This function modifies the global `CMAKE_CXX_FLAGS` variable in the
# CMake cache. The change is forced and will overwrite any previous value.
#
# @example
#   cx_cxx_append_flags(OPTIONS -Wall -Wextra -Werror)
#
# @warning Use with caution as this function modifies the global state of
# `CMAKE_CXX_FLAGS` and may affect other parts of the project.
#
function(cx_cxx_append_flags)
  set(options)
  set(one_value_args)
  set(multi_value_args OPTIONS)

  # Parse arguments
  cmake_parse_arguments(
    ARG
    "${options}"
    "${one_value_args}"
    "${multi_value_args}"
    ${ARGN}
  )

  cx_msg_debug_variable( ARG_OPTIONS )
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ARG_OPTIONS}"
    CACHE STRING "" FORCE
  )
endfunction( )
