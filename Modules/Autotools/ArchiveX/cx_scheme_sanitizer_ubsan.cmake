# UBSan-specific options
set(UBSAN_OPTIONS_LIST
  ENABLE
  DISABLE_RECOVERY
)
set(UBSAN_ONE_VALUE_ARGS
  OUTPUT
  SUPPRESSION_FILE
)
set(UBSAN_MULTI_VALUE_ARGS
  FLAGS
)

##
# @brief Configures UndefinedBehaviorSanitizer (UBSan) options for the project.
#
# This function enables and configures UndefinedBehaviorSanitizer (UBSan),
# which detects undefined behavior such as integer overflows, invalid memory
# accesses, illegal type casts, and more. It allows users to customize UBSan
# behavior, disable recovery, and specify a suppression file.
#
# @param ENABLE
#   Enables the use of UndefinedBehaviorSanitizer. If this option is not
#   specified, UBSan will not be enabled.
#   @example
#     cx_scheme_sanitizer_ubsan(ENABLE)
#     # Enables UBSan with default settings.
#
# @param DISABLE_RECOVERY
#   Disables recovery in UBSan. When this option is set, the program will
#   terminate immediately upon detecting undefined behavior.
#   @example
#     cx_scheme_sanitizer_ubsan(ENABLE DISABLE_RECOVERY)
#     # Enables UBSan and disables recovery on error.
#
# @param SUPPRESSION_FILE
#   Specifies the path to a suppression file for UBSan, where known issues or
#   false positives will be ignored.
#   @example
#     cx_scheme_sanitizer_ubsan(ENABLE SUPPRESSION_FILE "/path/to/ubsan_suppressions.txt")
#     # Enables UBSan and applies a suppression file for known issues.
#
# @param FLAGS
#   Additional custom UBSan flags that should be appended. This allows you to
#   customize UBSan configurations as needed.
#   @example
#     cx_scheme_sanitizer_ubsan(ENABLE FLAGS "-fsanitize=float-divide-by-zero")
#     # Enables UBSan and appends additional custom flags.
#
# @param OUTPUT
#   The name of the variable where the generated UBSan flags should be stored.
#   If this option is not provided, the flags will be applied globally to
#   `CMAKE_CXX_FLAGS`.
#   @example
#     cx_scheme_sanitizer_ubsan(ENABLE OUTPUT UBSAN_FLAGS)
#     # Stores the generated UBSan flags in the UBSAN_FLAGS variable.
#
# @note If OUTPUT is not specified, the flags are applied to `CMAKE_CXX_FLAGS`.
#
function(cx_scheme_sanitizer_ubsan)
  # Parse arguments for UndefinedBehaviorSanitizer
  cmake_parse_arguments(CX_UBSAN "${UBSAN_OPTIONS_LIST}" "${UBSAN_ONE_VALUE_ARGS}"
    "${UBSAN_MULTI_VALUE_ARGS}" ${ARGN}
  )

  # Append UndefinedBehaviorSanitizer to the global list if enabled
  if(CX_UBSAN_ENABLE)
    list(APPEND CX_SCHEME_SANITIZERS_ENABLED "undefined")
  endif()

  # Handle UBSan-specific flags
  if(CX_UBSAN_DISABLE_RECOVERY)
    list(APPEND UBSAN_FLAGS "-fno-sanitize-recover=undefined")
  endif()

  if(CX_UBSAN_FLAGS)
    list(APPEND UBSAN_FLAGS "${CX_UBSAN_FLAGS}")
  endif()

  # Set suppression file if provided
  if(CX_UBSAN_SUPPRESSION_FILE)
    set(ENV{UBSAN_OPTIONS} "$ENV{UBSAN_OPTIONS}:suppressions=${CX_UBSAN_SUPPRESSION_FILE}")
  endif()

  # Set debugging verbosity level
  if (SANITIZERS_DEBUG_LEVEL)
    set(ENV{ASAN_OPTIONS} "$ENV{ASAN_OPTIONS}:verbosity=${SANTIZER_DEBUG_LEVEL}")
  endif( )

  if ( DISABLE_DETECT_ODR_VIOLATION )
    set( ENV{ASAN_OPTIONS} "$ENV{ASAN_OPTIONS}:detect_odr_violation=0" )
  endif( )

  # Set the generated UBSan flags to the output or globally
  if(CX_UBSAN_OUTPUT)
    set(${CX_UBSAN_OUTPUT} "${UBSAN_FLAGS}" PARENT_SCOPE)
  else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${UBSAN_FLAGS}")
  endif()
  if ( ENV{UBSAN_OPTIONS} )
    set( ENV{UBSAN_OPTIONS} "$ENV{UBSAN_OPTIONS}" PARENT_SCOPE )
  endif( )
endfunction()
