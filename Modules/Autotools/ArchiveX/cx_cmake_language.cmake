include( Autotools/ArchiveX/cx_mktemp )

## /**
## @fn cx_cmake_language( EVAL CODE CODE_BLOCK )
## @brief Display notification message
## @details
## Display a message informing the user of the group of features being checked.
##
## @param EVAL
## Literal
##
## @param CODE
## literal
##
## @param CODE_BLOCK
## String of cmake code to be executed
##
## @note
##   This is implemented as a macro so the side effects of set() take place
##   within the scope of the caller
##
## @code{.cmake}
## set(A TRUE)
## set(B TRUE)
## set(C TRUE)
## set(condition "(A AND B) OR C")
##
## cmake_language(EVAL CODE "
##   if (${condition})
##     message(STATUS TRUE)
##   else()
##     message(STATUS FALSE)
##   endif()"
##   )
## @endcode
## @author Edward Maros
## @date   2022
## @igwn_copyright
## */
## cx_cmake_language( EVAL CODE code_block );
macro( cx_cmake_language EVAL CODE code_block )
  if ( ( CMAKE_VERSION VERSION_GREATER 3.18 )
      OR ( CMAKE_VERSION VERSION_EQUAL 3.18 ) )
    message( "DEBUG: INFO: Using builtin support" )
    cmake_language( EVAL CODE "${code_block}" )
  else( )
    message( "DEBUG: INFO: work around" )
    cx_mktemp( temp_file )
    file( WRITE "${temp_file}" "${code_block}" )
    include( "${temp_file}" )
    file( REMOVE "${temp_file}" )
  endif( )
endmacro( )
