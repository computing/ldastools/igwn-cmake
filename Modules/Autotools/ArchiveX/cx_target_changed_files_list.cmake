# -----------------------------------------------------------------------
# -----------------------------------------------------------------------
include( Autotools/ArchiveX/cx_prog_changed_files )

function( cx_target_changed_files_list )
  cx_prog_changed_files( )

  set( options
    )
  set( oneValueArgs
    GIT_PREFIX
    INSTALLED_PREFIX
    TARGET_PREFIX
    USER
    GROUP
    )
  set( multiValueArgs
    FILES
    PERMISSIONS
    )

  cmake_parse_arguments( ARG
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  set( target ${ARG_TARGET_PREFIX}-list-changed-files )
  add_custom_target(
    ${target}
    ${CMAKE_COMMAND}
    -DMODE=list
    "-DGIT_PREFIX=${ARG_GIT_PREFIX}"
    "-DINSTALLED_PREFIX=${ARG_INSTALLED_PREFIX}"
    "-DFILES='${ARG_FILES}'"
    -P ${PROGRAM_CHANGED_FILES}
    )

  if ( NOT TARGET list-changed-files )
    add_custom_target( list-changed-files )
  endif( )

  add_dependencies( list-changed-files ${target} )
endfunction( )
