# ---------------------------------------------------------------------
#  Check for Support of AX_CHECK_LIB_ZSTD
# ---------------------------------------------------------------------
include( Autotools/cm_define )
include( Autotools/cm_check_lib )

## /**
## @igwn_group{Library Files,}
## @igwn_group_begin
## */
##
## /**
## @fn cx_check_lib_zstd( )
## @brief Check for the presense of the Zstandard library
##
## @code{.cmake}
## cx_check_lib_zstd( )
## #endcode
##
## @author Edward Maros
## @date   2022
## @igwn_copyright
## */
function( CX_CHECK_LIB_ZSTD )
  cm_check_lib( zstd ZSTD_compress )
  if ( HAVE_LIBZSTD )
    find_library( LIBZSTD zstd )
  endif ( HAVE_LIBZSTD )
endfunction( CX_CHECK_LIB_ZSTD )
## /** @igwn_group_end */
