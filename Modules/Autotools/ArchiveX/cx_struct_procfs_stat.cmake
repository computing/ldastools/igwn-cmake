include( Autotools/cm_try_run )
include( Autotools/cm_msg_checking )
include( Autotools/cm_define )

function( CX_STRUCT_PROCFS_STAT )
  cm_try_run(
    VARIABLE HAVE_PROCFS_STAT
    SOURCE
      "#include <unistd.h>"
      " "
      "#include <fstream>"
      "#include <sstream>"
      " "
      "int"
      "main()"
      "{"
      "  std::ostringstream filename[<sc>]"
      " "
      "  filename << \"/proc/\""
      "  #if SIZEOF_INT == SIZEOF_PID_T"
      "	     << (int)::getpid( )"
      "  #else /* SIZEOF_INT == SIZEOF_PID_T */"
      "	     << ::getpid( )"
      "  #endif /* SIZEOF_INT == SIZEOF_PID_T */"
      "      << \"/stat\"[<sc>]"
      "  std::ifstream f( filename.str( ).c_str( ) )[<sc>]"
      "  const int retval ="
      "    ( ( f.is_open( ) ) ? 0 : 1 )[<sc>]"
      "  f.close( )[<sc>]"
      "  return retval[<sc>]"
      "}"
    DEFINES
      -DSIZEOF_INT=${SIZEOF_INT}
      -DSIZEOF_PID_T=${SIZEOF_PID_T}
      )
    cm_define(
      VARIABLE HAVE_PROCFS_STAT
      DESCRIPTION "Defined if /proc/<pid>/stat exists" )
    cm_msg_checking(
      "if process info is in /proc/<pid>/stat"
      CONDITION HAVE_PROCFS_STAT )
endfunction( CX_STRUCT_PROCFS_STAT )
