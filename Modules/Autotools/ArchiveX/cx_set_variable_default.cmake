#------------------------------------------------------------------------
# -*- mode: cmake -*-
#
# This macro sets a variable to the given value if it currently has
#   no definition
#------------------------------------------------------------------------
function(cx_set_variable_default var)
  # cx_msg_debug_variable(${var})
  if  ( NOT ${var} )
    set(${var} ${ARGN} PARENT_SCOPE)
  endif  ( NOT ${var} )
endfunction(cx_set_variable_default)