##
# @brief Sets environment variables for specified test targets based on sanitizer options.
#
# This function sets up environment variables for the provided test targets using
# the values of various sanitizer environment variables (e.g., `ASAN_OPTIONS`,
# `LSAN_OPTIONS`, `MSAN_OPTIONS`, `TSAN_OPTIONS`, and `UBSAN_OPTIONS`).
# It can also automatically apply these settings to all test targets if none
# are explicitly specified.
#
# @param[in] TEST_TARGETS [Optional] A list of test targets to which the environment
#        variables should be applied. If not specified, the function retrieves all
#        tests defined in the current directory.
# @param[in] VARIABLE_NAMES [Optional] A list of environment variable names to set for
#        the test targets. If not specified, the function uses a default list of
#        sanitizer variables: `ASAN_OPTIONS`, `LSAN_OPTIONS`, `MSAN_OPTIONS`,
#        `TSAN_OPTIONS`, and `UBSAN_OPTIONS`.
# @param[out] OUTPUT [Optional] Name of variable in parent scope to store the
#        environment variables.
#
# @note If no `TEST_TARGETS` are provided, the function automatically retrieves all
#       tests in the current directory using the `TESTS` property.
#
# @note If no `VARIABLE_NAMES` are provided, the function defaults to using common
#       sanitizer-related environment variables.
#
# @warning The environment variables are applied globally to the specified test targets.
#          Ensure that these settings are appropriate for all tests to avoid unintended
#          behavior.
#
# @example
#   cx_scheme_sanitizer_set_environment(
#       TEST_TARGETS my_test_1 my_test_2
#       VARIABLE_NAMES ASAN_OPTIONS UBSAN_OPTIONS
#   )
#   # This sets the ASAN and UBSAN options for `my_test_1` and `my_test_2`.
#
function(cx_scheme_sanitizer_set_environment )
  set(options
  )
  set(oneValueArgs
    OUTPUT
  )
  set(multiValueArgs
    TEST_TARGETS
    VARIABLE_NAMES
  )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT ARG_TEST_TARGETS AND NOT ARG_OUTPUT )
    get_property( ARG_TEST_TARGETS
      DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
      PROPERTY TESTS
    )
  endif( )
  cx_msg_debug_variable( ARG_TEST_TARGETS )
  if ( NOT ARG_VARIABLE_NAMES )
    set( ARG_VARIABLE_NAMES
      ASAN_OPTIONS
      LSAN_OPTIONS
      MSAN_OPTIONS
      TSAN_OPTIONS
      UBSAN_OPTIONS
    )
  endif ( )
  cx_msg_debug_variable( ARG_VARIABLE_NAMES )
  foreach( variable_name ${ARG_VARIABLE_NAMES} )
    set( value $ENV{${variable_name}} )
    if ( DEFINED value )
      list(APPEND
        environment_variables
        "${variable_name}=set:$ENV{${variable_name}}"
      )
    endif( )
  endforeach( )
  cx_msg_debug_variable(environment_variables)
  if ( ARG_OUTPUT )
    set(output_list)
    foreach(environment_variable IN LISTS environment_variables )
      string(REPLACE "=set:" "="
        moddified_environment_variable "${environment_variable}"
      )
      list(APPEND output_list "${moddified_environment_variable}")
    endforeach( )
   set(
      ${ARG_OUTPUT}
      ${output_list}
      PARENT_SCOPE
    )
  endif()
  if ( ARG_TEST_TARGETS )
    set_tests_properties(
      ${ARG_TEST_TARGETS}
      PROPERTIES ENVIRONMENT_MODIFICATION "${environment_variables}"
    )
  endif( )
endfunction( )
