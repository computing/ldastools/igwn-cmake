#------------------------------------------------------------------------
# -*- mode: cmake -*-
#    MACPORTS_CATEGORY (default: science)
#    MACPORTS_NAME     (default: ${PROJECT_NAME})
#    MACPORTS_PORT_FILE_TEMPLATE
#                      ( default: ${CMAKE_SOURCE_DIR}/config/Portfile.in )
#    MACPORTS_FORCE_INSTALL
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/ArchiveX/cx_msg_debug_variable )
include( Autotools/ArchiveX/cx_set_variable_default )

function(_cx_packaging_macports_cache FILENAME )
  file( REMOVE ${FILENAME} )
  list( LENGTH ARGN size )
  while( ${size} GREATER 0 )
    list( GET ARGN 0 var_ )
    list( REMOVE_AT ARGN 0 )
    list( GET ARGN 0 value_ )
    list( REMOVE_AT ARGN 0 )
    if ( DEFINED ${value_} )
      file( APPEND ${FILENAME} "set(${var_} \"${${value_}}\" CACHE INTERNAL \"\" )\n" )
    else( )
      file( APPEND ${FILENAME} "set(${var_} \"${value_}\" CACHE INTERNAL \"\")\n" )
    endif( )
    list( LENGTH ARGN size )
  endwhile( )
endfunction( )

function(cx_packaging_macports)
  #----------------------------------------------------------------------
  # Establish defaults
  #----------------------------------------------------------------------
  set(options)
  set(oneValueArgs
    MACPORTS_CATEGORY
    MACPORTS_NAME
    MACPORTS_PORT_FILE_TEMPLATE
    )
  set(multiValueArgs SUBPORTS)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( MACPORTS_FORCE_INSTALL )
    set( _macports_force_install -f -N )
  endif( )
  set( _macports_debug_options -d -v -t )

  cx_set_variable_default( ARG_MACPORTS_CATEGORY science )
  cx_set_variable_default( ARG_MACPORTS_NAME ${PROJECT_NAME} )
  cx_set_variable_default( ARG_MACPORTS_PORT_FILE_TEMPLATE
    "${CMAKE_SOURCE_DIR}/config/Portfile.in" )

  find_program(MACPORTSBUILD port)
  find_program(MACPORTSINDEX portindex)
  if (MACPORTSBUILD)
    set( PORT_PKG_DIR "/opt/local/var/macports/distfiles/${ARG_MACPORTS_NAME}" )
    set( PORT_CONFIG "/opt/local/etc/macports/sources.conf" )
    set( PORT_TOP_DIR ${CMAKE_CURRENT_BINARY_DIR}/test_port )
    set( PORT_TEST_DIR ${PORT_TOP_DIR}/${ARG_MACPORTS_CATEGORY}/${ARG_MACPORTS_NAME} )
    string( REGEX REPLACE "^[\"](.*)[\"]$" "\\1" MACPORTS_PACKAGE_URL "${PACKAGE_URL}" )
    set( portfile_cache_filename "${CMAKE_CURRENT_BINARY_DIR}/portfile_cache.txt" )
    _cx_packaging_macports_cache(
      ${portfile_cache_filename}
      TEMPLATE "${ARG_MACPORTS_PORT_FILE_TEMPLATE}"
      OUTPUT_DIR ${PORT_TEST_DIR}
      SOURCE_DISTRIBUTION "${CMAKE_BINARY_DIR}/${CPACK_SOURCE_PACKAGE_FILE_NAME_FULL}"
			PACKAGE_URL                         MACPORTS_PACKAGE_URL
			PACKAGE_SOURCE_URL                  PACKAGE_SOURCE_URL
			PROJECT_NAME                        PROJECT_NAME
			PROJECT_VERSION                     PROJECT_VERSION
			PROJECT_DESCRIPTION                 PROJECT_DESCRIPTION
			PROJECT_DESCRIPTION_LONG            PROJECT_DESCRIPTION_LONG
			CPACK_SOURCE_PACKAGE_FILE_EXTENSION CPACK_SOURCE_PACKAGE_FILE_EXTENSION
      )
    add_custom_target(macports)
    add_custom_target( macports_portfile
    	DEPENDS
		  dist
      COMMAND ${CMAKE_COMMAND}
      -C "${CMAKE_CURRENT_BINARY_DIR}/portfile_cache.txt"
      -P ${IGWN_CMAKE_SCRIPTDIR}/portfile.cmake
      )
    add_custom_target(macports_build
		      DEPENDS macports_portfile
		      WORKING_DIRECTORY ${PORT_TEST_DIR}
		      COMMAND ${CMAKE_COMMAND} -E make_directory ${PORT_PKG_DIR}
		      COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_BINARY_DIR}/${CPACK_SOURCE_PACKAGE_FILE_NAME_FULL}" ${PORT_PKG_DIR}
		      COMMAND ${MACPORTSBUILD} lint --nitpick
		      COMMAND ${MACPORTSBUILD} ${_macports_force_install} uninstall --follow-dependents
		      COMMAND ${MACPORTSBUILD} clean
		      COMMAND ${MACPORTSBUILD} ${_macports_force_install} ${_macports_debug_options} install
    )
    if ( ARG_SUBPORTS )
      foreach( SUBPORT ${ARG_SUBPORTS} )
        add_custom_target( macports-subport-${SUBPORT}
          COMMAND ${MACPORTSBUILD} ${_macports_force_install} install subport=${SUBPORT}
          DEPENDS macports_build
          WORKING_DIRECTORY ${PORT_TEST_DIR}
          )
        add_dependencies(macports macports-subport-${SUBPORT})
      endforeach( )
    endif ( )
    add_custom_target(macports_index
		      DEPENDS macports_build
		      WORKING_DIRECTORY ${PORT_TOP_DIR}
		      COMMAND ${MACPORTSINDEX}
    )
    add_dependencies(macports macports_index)
  else(MACPORTSBUILD)
    add_custom_target(port
		      COMMAND ${CMAKE_COMMAND} -E echo "This platform does not support building of MacPorts packages")
  endif(MACPORTSBUILD)
endfunction(cx_packaging_macports)
