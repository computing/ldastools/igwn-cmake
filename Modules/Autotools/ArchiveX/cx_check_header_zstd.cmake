#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )
include( CheckLibraryExists )
include( Autotools/cm_check_headers )

## /**
## @igwn_group{Particular Headers,}
## @igwn_group_begin
## */
##
## /**
## @fn cx_check_header_zstd( )
## @brief Check for the presense of the Zstandard header file
##
## @code{.cmake}
## cx_check_header_zstd( )
## @endcode
##
## @author Edward Maros
## @date   2022
## @igwn_copyright
## */
## /** @igwn_group_end */
function(CX_CHECK_HEADER_ZSTD)
  cm_check_headers( HEADERS zstd.h )
endfunction( )
