#========================================================================
# -*- mode: cmake; -*-
#------------------------------------------------------------------------
#
# cm_aux_variable_normalize( VARIABLE PATTERN )
#
#------------------------------------------------------------------------
include(CheckTypeSize)

function(CM_AUX_VARIABLE_NORMALIZE VARIABLE PATTERN )
  string( TOUPPER ${PATTERN} var )
  string( REPLACE "*" "P" var ${var} )
  string( REGEX REPLACE "[^A-Za-z0-9]" "_" var ${var} )
  set( ${VARIABLE} ${var} PARENT_SCOPE )
endfunction(CM_AUX_VARIABLE_NORMALIZE )
