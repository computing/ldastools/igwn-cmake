# ---------------------------------------------------------------------
#  Check for the Zstd package
# ---------------------------------------------------------------------
include( Autotools/cm_define )
include( Autotools/ArchiveX/cx_find_pkg )

function( CX_FIND_PKG_ZSTD )
  cx_find_pkg( libzstd
    PREFIX Zstd
    HEADER_FILENAME zstd.h
    )
endfunction( )
