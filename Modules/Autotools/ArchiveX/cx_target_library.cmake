#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )
include( GNUInstallDirs )
include( GNUPkgInstallDirs )
include( Autotools/ArchiveX/cx_libtool_version_calculation )
include( Autotools/ArchiveX/cx_msg_debug_variable )

function( _cx_make_convenience_library )
  add_library(${ARG_TARGET} OBJECT ${ARG_SOURCES} )
  if ( ARG_INCLUDE_DIRECTORIES )
    target_include_directories( ${ARG_TARGET}
      ${ARG_INCLUDE_DIRECTORIES} )
  endif( )
  if ( ARG_DEFINES )
    set_target_properties( ${ARG_TARGET}
      PROPERTIES
      COMPILE_DEFINITIONS "${ARG_DEFINES}"
      )
  endif( )
  set_target_properties(
    ${ARG_TARGET}
    PROPERTIES
      COMPILE_FLAGS "${SHARED_FLAGS}" )
endfunction( _cx_make_convenience_library )

function( _cx_make_library )
  set( installed_library_targets ${ARG_TARGET} )
  if (NOT RUNTIME_DESTINATION)
    #--------------------------------------------------------------------
    # Figure out where to install libraries
    #--------------------------------------------------------------------
    if ( WIN32 )
      set( RUNTIME_DESTINATION ${CMAKE_INSTALL_BINDIR} )
    else ( WIN32 )
      set( RUNTIME_DESTINATION ${CMAKE_INSTALL_LIBDIR} )
    endif( WIN32 )
  endif (NOT RUNTIME_DESTINATION)
  if (NOT LIBRARY_DESTINATION)
    #--------------------------------------------------------------------
    # Figure out where to install libraries
    #--------------------------------------------------------------------
    set(LIBRARY_DESTINATION ${RUNTIME_DESTINATION})
  endif (NOT LIBRARY_DESTINATION)
  set( libraries_purge_ "" )
  if ( ARG_LIBRARIES )
    foreach(lib_ ${ARG_LIBRARIES})
      if ( ${lib_} MATCHES "^[$][<]TARGET_OBJECTS:.*[>]" )
	      list(APPEND CONVENIENCE_LIBS ${lib_})
      elseif ( TARGET ${lib_} )
        get_target_property(target_type ${lib_} TYPE)
        if (target_type STREQUAL "OBJECT_LIBRARY")
          list(APPEND CONVENIENCE_LIBS $<TARGET_OBJECTS:${lib_}> )
          list(APPEND libraries_purge_ ${lib_} )
        endif ()
      endif(${lib_} MATCHES "^[$][<]TARGET_OBJECTS:.*[>]" )
    endforeach(lib_)
    if( CONVENIENCE_LIBS )
      list( REMOVE_ITEM ARG_LIBRARIES ${CONVENIENCE_LIBS} ${libraries_purge_} )
    endif( CONVENIENCE_LIBS )
  endif ( ARG_LIBRARIES )


  add_library(${ARG_TARGET} SHARED ${ARG_SOURCES} ${CONVENIENCE_LIBS})
  if ( ARG_DEFINES )
    set_target_properties( ${ARG_TARGET}
      PROPERTIES
      COMPILE_DEFINITIONS "${ARG_DEFINES}"
      )
  endif( )
  if ( APPLE )
    set_target_properties( ${ARG_TARGET}
      PROPERTIES
      INSTALL_NAME_DIR ${CMAKE_INSTALL_FULL_LIBDIR}
      MACOSX_RPATH OFF
      SKIP_RPATH TRUE )
  endif( )
  if ( ARG_INCLUDE_DIRECTORIES )
    target_include_directories( ${ARG_TARGET}
      ${ARG_INCLUDE_DIRECTORIES} )
  endif( )
  if ( ARG_STATIC )
    add_library(${ARG_TARGET}Static STATIC ${ARG_SOURCES} ${CONVENIENCE_LIBS})
    if ( ARG_INCLUDE_DIRECTORIES )
      target_include_directories( ${ARG_TARGET}Static
        ${ARG_INCLUDE_DIRECTORIES} )
    endif( )
    set_target_properties(${ARG_TARGET}Static PROPERTIES OUTPUT_NAME ${ARG_TARGET})
    list( APPEND installed_library_targets ${ARG_TARGET}Static )
  endif( )

  if ( UNIX )
    #--------------------------------------------------------------------
    # Calculate the versioning based on what libtool would have done
    #--------------------------------------------------------------------
    string( TOUPPER ${ARG_TARGET} TARGET_UPPER )
    cx_libtool_version_calculation(
      ${TARGET_UPPER}
      ${ARG_CURRENT}
      ${ARG_REVISION}
      ${ARG_AGE} )
    set_target_properties(
      ${ARG_TARGET}
      PROPERTIES
        VERSION ${${TARGET_UPPER}_VERSION}
        SOVERSION ${${TARGET_UPPER}_SOVERSION} )
    if ( ARG_LIBRARIES )
      target_link_libraries(${ARG_TARGET} ${ARG_LIBRARIES})
      if ( ARG_STATIC )
        target_link_libraries(${ARG_TARGET}Static ${ARG_LIBRARIES})
      endif( )
    endif ( ARG_LIBRARIES )
  endif ( UNIX )


  cx_msg_debug_variable( RUNTIME_DESTINATION )
  cx_msg_debug_variable( LIBRARY_DESTINATION )
  cx_msg_debug_variable( LIB_INSTALL_DIR )

  if ( ARG_PKG_CONFIG_FILE )
    #--------------------------------------------------------------------
    # Handle pkg_config file intended for library
    #--------------------------------------------------------------------
    string(REGEX REPLACE "[.]in$" "" pkg_config_out ${ARG_PKG_CONFIG_FILE})
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/${ARG_PKG_CONFIG_FILE}
      ${CMAKE_CURRENT_BINARY_DIR}/${pkg_config_out}
      @ONLY)
    install(
      FILES ${CMAKE_CURRENT_BINARY_DIR}/${pkg_config_out}
      DESTINATION "${CMAKE_INSTALL_LIBDIR}/pkgconfig")

  endif ( ARG_PKG_CONFIG_FILE )
  install(
    TARGETS ${installed_library_targets}
    RUNTIME
      DESTINATION ${RUNTIME_DESTINATION}
      COMPONENT ${ARG_RUNTIME_COMPONENT}
    LIBRARY
      DESTINATION ${LIBRARY_DESTINATION}
      COMPONENT ${ARG_LIBRARY_COMPONENT}
    ARCHIVE
      DESTINATION ${CMAKE_INSTALL_LIBDIR}
      COMPONENT ${ARG_ARCHIVE_COMPONENT})
endfunction( _cx_make_library )


function(cx_target_library)
  set(options
    CONVENIENCE
    NO_ABI_CHECK
    STATIC)
  set(oneValueArgs
    TARGET
    CURRENT
    REVISION
    AGE
    RUNTIME_COMPONENT
    LIBRARY_COMPONENT
    ARCHIVE_COMPONENT
    PKG_CONFIG_FILE
    ABI_MESSAGE
    ABI_HEADER_DIR
    ABI_LOCAL_INCLUDE_DIR
    )
  set(multiValueArgs
    DEFINES
    INCLUDE_DIRECTORIES
    LIBRARIES
    SOURCES
    ABI_HEADERS
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if(NOT ARG_RUNTIME_COMPONENT)
    set(ARG_RUNTIME_COMPONENT "Applications")
  endif( NOT ARG_RUNTIME_COMPONENT)
  if(NOT ARG_LIBRARY_COMPONENT)
    set(ARG_LIBRARY_COMPONENT ${ARG_RUNTIME_COMPONENT})
  endif( NOT ARG_LIBRARY_COMPONENT)
  if(NOT ARG_ARCHIVE_COMPONENT)
    set(ARG_ARCHIVE_COMPONENT "Development")
  endif( NOT ARG_ARCHIVE_COMPONENT)

  if( "${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "x86_64" )
    set( SHARED_FLAGS "-fPIC" CACHE INTERNAL "Specify how to create position independent code" )
  endif( "${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "x86_64" )

  if ( ARG_CONVENIENCE )
    _cx_make_convenience_library( )
  else ( ARG_CONVENIENCE )
    _cx_make_library( )
    if ( NOT ARG_NO_ABI_CHECK )
      cx_target_abi_check(
        LIBRARY ${ARG_TARGET}
        MESSAGE ${ARG_ABI_MESSAGE}
        HEADER_DIR ${ARG_ABI_HEADER_DIR}
        LOCAL_INCLUDE_DIR ${ARG_ABI_LOCAL_INCLUDE_DIR}
        HEADERS ${ARG_ABI_HEADERS}
        )
    endif ( )
  endif ( ARG_CONVENIENCE )

endfunction(cx_target_library)
