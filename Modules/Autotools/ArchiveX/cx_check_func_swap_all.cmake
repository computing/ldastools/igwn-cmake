# -*- mode: cmake -*-
# ---------------------------------------------------------------------
#  CX_CHECK_FUNC_SWAP_ALL
# ---------------------------------------------------------------------
include( Autotools/ArchiveX/cx_check_header_for_subst )
include( Autotools/ArchiveX/cx_check_func_swap )

function( cx_check_func_swap_all)
  cx_check_header_for_subst(byteswap.h            HAVE_BYTESWAP_H)
  cx_check_header_for_subst(sys/byteorder.h       HAVE_SYS_BYTEORDER_H)
  cx_check_header_for_subst(machine/bswap.h       HAVE_MACHINE_BSWAP_H)
  cx_check_header_for_subst(libkern/OSByteOrder.h HAVE_LIBKERN_OSBYTEORDER_H)

  cx_check_func_swap("bswap_16" HAVE_BSWAP_16 "int16")
  cx_check_func_swap("bswap_32" HAVE_BSWAP_32 "int32")
  cx_check_func_swap("bswap_64" HAVE_BSWAP_64 "int64")

  cx_check_func_swap("bswap16" HAVE_BSWAP16 "int16")
  cx_check_func_swap("bswap32" HAVE_BSWAP32 "int32")
  cx_check_func_swap("bswap64" HAVE_BSWAP64 "int64")

  cx_check_func_swap("BSWAP_16" HAVE_BSWAP_16_MACRO "int16")
  cx_check_func_swap("BSWAP_32" HAVE_BSWAP_32_MACRO "int32")
  cx_check_func_swap("BSWAP_64" HAVE_BSWAP_64_MACRO "int64")

  cx_check_func_swap("OSSwapInt16" HAVE_OSSWAPINT16 "int16")
  cx_check_func_swap("OSSwapInt32" HAVE_OSSWAPINT32 "int32")
  cx_check_func_swap("OSSwapInt64" HAVE_OSSWAPINT64 "int64")
endfunction( )
