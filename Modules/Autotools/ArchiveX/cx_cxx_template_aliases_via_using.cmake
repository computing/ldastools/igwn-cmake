# -*- mode: cmake -*-
# ---------------------------------------------------------------------
#  CX_CXX_TEMPLATE_ALIASES_VIA_USING
#     Check if C++ supports template aliasing via using
# ---------------------------------------------------------------------
include( Autotools/cm_define )
include( Autotools/cm_try_compile )
include( Autotools/ArchiveX/cm_string_special_characters )

function( CX_CXX_TEMPLATE_ALIASES_VIA_USING )
  set(inc "
#if HAVE_MEMORY
#include <memory>
#define __ldas_ns__ std
#elif HAVE_TR1_MEMORY
#include <tr1/memory>
#define __ldas_ns__ std::tr1
#endif

namespace MySpace {
#define SharedPtr shared_ptr
  using __ldas_ns__::SharedPtr;
}
")

  set(body "
 MySpace::SharedPtr< int >	a;
")

  cm_try_compile("${inc}" "${body}" HAVE_CXX_TEMPLATE_ALIASES_VIA_USING
    DEFINES -DHAVE_MEMORY=${HAVE_MEMORY}
    -DHAVE_TR1_MEMORY=${HAVE_TR1_MEMORY})
  if(HAVE_CXX_TEMPLATE_ALIASES_VIA_USING)
    set(result_var "yes")
  else(HAVE_CXX_TEMPLATE_ALIASES_VIA_USING)
    set(result_var "no")
  endif(HAVE_CXX_TEMPLATE_ALIASES_VIA_USING)
  message( STATUS "Checking if the C++ compiler supports template aliasing via using ... ${result_var}")
  cm_define(
    VARIABLE HAVE_CXX_TEMPLATE_ALIASES_VIA_USING
    DESCRIPTION "Defined if C++ supports template aliasing via using directive" )
endfunction( CX_CXX_TEMPLATE_ALIASES_VIA_USING )
