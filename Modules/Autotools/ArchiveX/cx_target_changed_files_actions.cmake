# -----------------------------------------------------------------------
# -----------------------------------------------------------------------
include( Autotools/ArchiveX/cx_target_changed_files_list )
include( Autotools/ArchiveX/cx_target_changed_files_diff )
include( Autotools/ArchiveX/cx_target_changed_files_pull )
include( Autotools/ArchiveX/cx_target_changed_files_push )

function( cx_target_changed_files_actions )
  cx_target_changed_files_diff( ${ARGN} )
  cx_target_changed_files_list( ${ARGN} )
  cx_target_changed_files_pull( ${ARGN} )
  cx_target_changed_files_push( ${ARGN} )
endfunction( )
