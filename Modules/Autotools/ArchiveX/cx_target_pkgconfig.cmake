#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )
include( GNUInstallDirs )
include( GNUPkgInstallDirs )

function( cx_target_pkgconfig target )
  set(options
    )
  set(oneValueArgs
    DESTINATION
    )
  set(multiValueArgs
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if( NOT DEFINED ARG_DESTINATION )
    set( ARG_DESTINATION ${CMAKE_INSTALL_LIBDIR} )
  endif( )
  if ( EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${target}.pc.in)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/${target}.pc.in
      ${CMAKE_CURRENT_BINARY_DIR}/${target}.pc
      @ONLY
      )
    install(
      FILES ${CMAKE_CURRENT_BINARY_DIR}/${target}.pc
      DESTINATION ${ARG_DESTINATION}/pkgconfig
      )
  endif( )
  if ( EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${target}-uninstalled.pc.in)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/${target}-uninstalled.pc.in
      ${CMAKE_CURRENT_BINARY_DIR}/${target}-uninstalled.pc
      @ONLY
      )
  endif( )
endfunction( )
