#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )

function(cx_ldas_library)
  set(options)
  set(oneValueArgs TARGET CURRENT REVISION AGE)
  set(multiValueArgs SOURCES)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  add_library(${ARG_TARGET} SHARED ${ARG_SOURCES})

  if ( UNIX )
    #--------------------------------------------------------------------
    # Calculate the versioning based on what libtool would have done
    #--------------------------------------------------------------------
    string( TOUPPER ${ARG_TARGET} TARGET_UPPER )
    cx_libtool_version_calculation(
      ${TARGET_UPPER}
      ${ARG_CURRENT}
      ${ARG_REVISION}
      ${ARG_AGE} )
    set_target_properties(
      ${target}
      PROPERTIES
        VERSION ${${TARGET_UPPER}_VERSION}
        SOVERSION ${${TARGET_UPPER}_SOVERSION} )
  endif ( UNIX )

  

endfunction(cx_ldas_library)