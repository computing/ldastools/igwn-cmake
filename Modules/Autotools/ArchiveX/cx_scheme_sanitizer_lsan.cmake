# LSan-specific options
set(LSAN_OPTIONS_LIST
  ENABLE
  DISABLE_LEAK_DETECTION
  DISABLE_RECOVERY
)
set(LSAN_ONE_VALUE_ARGS
  OUTPUT
  SUPPRESSION_FILE
)
set(LSAN_MULTI_VALUE_ARGS
  FLAGS
)

function(cx_scheme_sanitizer_lsan)
  # Parse arguments for LeakSanitizer
  cmake_parse_arguments(CX_LSAN "${LSAN_OPTIONS_LIST}" "${LSAN_ONE_VALUE_ARGS}"
    "${LSAN_MULTI_VALUE_ARGS}" ${ARGN}
  )

  # Append LeakSanitizer to the global list if enabled
  if(CX_LSAN_ENABLE)
    list(APPEND CX_SCHEME_SANITIZERS_ENABLED "leak")
  endif()

  # Handle LSan-specific options
  if(CX_LSAN_DISABLE_LEAK_DETECTION)
    set(ENV{LSAN_OPTIONS} "$ENV{LSAN_OPTIONS}:detect_leaks=0")
  endif()

  if(CX_LSAN_DISABLE_RECOVERY)
    list(APPEND ASAN_FLAGS "-fno-sanitize-recover=leak")
  endif()

  if(CX_LSAN_FLAGS)
    set(LSAN_FLAGS "${CX_LSAN_FLAGS}")
  endif()

  # Set suppression file if provided
  if(CX_LSAN_SUPPRESSION_FILE)
    set(ENV{LSAN_OPTIONS} "$ENV{LSAN_OPTIONS}:suppressions=${CX_LSAN_SUPPRESSION_FILE}")
  endif()

  # Set debugging verbosity level
  if (SANITIZERS_DEBUG_LEVEL)
    set(ENV{ASAN_OPTIONS} "$ENV{ASAN_OPTIONS}:verbosity=${SANTIZER_DEBUG_LEVEL}")
  endif( )

  # Set the generated LSan flags to the output or globally
  if(CX_LSAN_OUTPUT)
    set(${CX_LSAN_OUTPUT} "${LSAN_FLAGS}" PARENT_SCOPE)
  else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${LSAN_FLAGS}")
  endif()
  if ( ENV{LSAN_OPTIONS} )
    set( ENV{LSAN_OPTIONS} "$ENV{LSAN_OPTIONS}" PARENT_SCOPE )
  endif( )
endfunction()
