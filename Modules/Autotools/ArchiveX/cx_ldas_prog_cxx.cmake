#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_ldas_prog_cxx
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/Internal/ci_cache_list )

function(cx_ldas_prog_cxx)
  set(options )
  set(oneValueArgs
    )
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  set( compiler ${CMAKE_CXX_COMPILER_ID} )
  if ( ( compiler STREQUAL "GNU" ) OR
      ( compiler MATCHES "^.*Clang$" ) )
    if ( ${CMAKE_CXX_STANDARD} GREATER 13 )
      ci_cache( CXXSTDFLAGS VALUE "--std=c++14" )
    elseif ( ${CMAKE_CXX_STANDARD} GREATER 10 )
      ci_cache( CXXSTDFLAGS VALUE "--std=c++11" )
    endif( )
  endif( )
endfunction(cx_ldas_prog_cxx)
