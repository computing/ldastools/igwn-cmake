#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_msg_debug_variable( )
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/ArchiveX/cx_msg_debug )

## /**
## @igwn_group_PrintingMessages
## @igwn_group_begin
## */
## /**
## @fn cx_msg_debug_variable( variable )
## @brief Display debugging message
## @details
## This command will display the variable name followed by its value
## if the global variable CM_MSG_DEBUG_VERBOSE is True
##
## @param  txt
##    If a list, then a collection of items to be displayed
##    If a string, then a text string to be displayed
##
## @sa cx_msg_debug
##
## @code{.cmake}
## cx_msg_debug_variable( foo )
## @endcode
## @author Edward Maros
## @date   2019-2020
## @igwn_copyright
## */
## /** @igwn_group_end */
## cx_msg_debug_variable( variable );
macro(cx_msg_debug_variable variable)
  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  cx_msg_debug( "${variable}: ${${variable}}" )
endmacro()
