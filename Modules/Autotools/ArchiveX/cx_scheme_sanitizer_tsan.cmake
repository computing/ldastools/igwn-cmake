# TSan-specific options
set(TSAN_OPTIONS_LIST
  ENABLE
  DISABLE_RECOVERY
)
set(TSAN_ONE_VALUE_ARGS
  OUTPUT
  SUPPRESSION_FILE
)
set(TSAN_MULTI_VALUE_ARGS
  FLAGS
)

function(cx_scheme_sanitizer_tsan)
  # Parse arguments for ThreadSanitizer
  cmake_parse_arguments(CX_TSAN "${TSAN_OPTIONS_LIST}" "${TSAN_ONE_VALUE_ARGS}"
    "${TSAN_MULTI_VALUE_ARGS}" ${ARGN}
  )

  # Append ThreadSanitizer to the global list if enabled
  if(CX_TSAN_ENABLE)
    list(APPEND CX_SCHEME_SANITIZERS_ENABLED "thread")
  endif()

  # Handle TSan-specific flags
  if(CX_TSAN_DISABLE_RECOVERY)
    set(THREAD_FLAGS "-fno-sanitize-recover=thread")
  endif()

  if(CX_TSAN_FLAGS)
    list(APPEND THREAD_FLAGS "${CX_TSAN_FLAGS}")
  endif()

  # Set suppression file if provided
  if(CX_TSAN_SUPPRESSION_FILE)
    set(ENV{TSAN_OPTIONS} "$ENV{TSAN_OPTIONS}:suppressions=${CX_TSAN_SUPPRESSION_FILE}")
  endif()

  # Set debugging verbosity level
  if (SANITIZERS_DEBUG_LEVEL)
    set(ENV{ASAN_OPTIONS} "$ENV{ASAN_OPTIONS}:verbosity=${SANTIZER_DEBUG_LEVEL}")
  endif( )

  # Set the generated TSan flags to the output or globally
  if(CX_TSAN_OUTPUT)
    set(${CX_TSAN_OUTPUT} "${THREAD_FLAGS}" PARENT_SCOPE)
  else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${THREAD_FLAGS}")
  endif()
  if ( ENV{TSAN_OPTIONS} )
    set( ENV{TSAN_OPTIONS} "$ENV{TSAN_OPTIONS}" PARENT_SCOPE )
  endif( )
endfunction()
