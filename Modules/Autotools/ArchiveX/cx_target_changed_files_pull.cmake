# -----------------------------------------------------------------------
# -----------------------------------------------------------------------
include( Autotools/ArchiveX/cx_prog_changed_files )


function( cx_target_changed_files_pull )
  cx_prog_changed_files( )

  set( options
    )
  set( oneValueArgs
    GIT_PREFIX
    INSTALLED_PREFIX
    TARGET_PREFIX
    )
  set( multiValueArgs
    FILES
    )

  cmake_parse_arguments( ARG
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  set( target ${ARG_TARGET_PREFIX}-pull-changed-files )
  add_custom_target(
    ${target}
    ${CMAKE_COMMAND}
    -DMODE=pull
    "-DGIT_PREFIX=${ARG_GIT_PREFIX}"
    "-DINSTALLED_PREFIX=${ARG_INSTALLED_PREFIX}"
    "-DFILES='${ARG_FILES}'"
    -P ${PROGRAM_CHANGED_FILES}
    )
  if ( NOT TARGET pull-changed-files )
    add_custom_target( pull-changed-files )
  endif( )
  add_dependencies( pull-changed-files ${target} )
endfunction( )
