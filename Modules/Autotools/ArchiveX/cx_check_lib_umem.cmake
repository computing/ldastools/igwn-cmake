#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# CX_CHECK_LIB_UMEM()
# ---------------------------------------------------------------------
#  Detect if the OS supplies a version of libumem
# ---------------------------------------------------------------------
function( CX_CHECK_LIB_UMEM )
  if ( EXISTS "/usr/lib${LIB_64_DIR}/libumem.so" )
    set( LIBUMEM "/usr/lib${LIB_64_DIR}/libumem.so"
      CACHE INTERNAL "Internal" )
    set( HAVE_LIBUMEM 1
      CACHE INTERNAL "Internal" )
  else ( EXISTS "/usr/lib${LIB_64_DIR}/libumem.so" )
    set( HAVE_LIBUMEM 0
      CACHE INTERNAL "Internal" )
  endif ( EXISTS "/usr/lib${LIB_64_DIR}/libumem.so" )
endfunction( CX_CHECK_LIB_UMEM )
