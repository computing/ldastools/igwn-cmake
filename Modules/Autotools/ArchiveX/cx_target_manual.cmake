#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_target_manual( )
#
# TARGET - Intended audience for the document
#    * Developer - Developer of the library being described
#    * Admin - Person responsible for installation of the software
#        and the services which may be part of the package
#    * User - Application developer who uses the library or tools
# TEMPLATE - Doxygen configuration file which has place holders
#    (ex: DiskCacheManual.cfg.in)
#------------------------------------------------------------------------
include( Autotools/cm_msg_error )
include( CMakeParseArguments )

macro( cp_doxygen_options )
  set(option_tags
    OPTIONS
    ONE_VALUE_ARGS
    MULTI_VALUE_ARGS
    )

  cmake_parse_arguments(ARG "" "" "${option_tags}" ${ARGN} )

  if ( ARG_OPTIONS )
    list( APPEND options ${ARG_OPTIONS} )
  endif ( )
  if ( ARG_ONE_VALUE_ARGS )
    list( APPEND oneValueArgs ${ARG_ONE_VALUE_ARGS} )
  endif ( )
  if ( ARG_MULTI_VALUE_ARGS )
    list( APPEND multiValueArgs ${ARG_MULTI_VALUE_ARGS} )
  endif ( )
endmacro( )

macro( cp_doxygen_build_options )
  cp_doxygen_options(${CP_VARIABLE_BUILD_OPTIONS}
    )
endmacro( )

macro( cp_doxygen_eval_options )
  set(option_tags
    OPTIONS
    ONE_VALUE_ARGS
    MULTI_VALUE_ARGS
    )

  cmake_parse_arguments(ARG "" "" "${option_tags}" ${ARGN} )

  foreach(option ${ARG_OPTIONS})
    cp_set_boolean( ${option} )
  endforeach()
  foreach(one_value ${ARG_ONE_VALUE_ARGS})
    cp_set_one_value( ${one_value} )
  endforeach()
  foreach(multi_value ${ARG_MULTI_VALUE_ARGS})
    cp_set_multi_value( ${multi_value} )
  endforeach()
endmacro( )

macro(cp_set_boolean VAR )
  if ( ${ARG_${VAR}} )
    set( ${VAR} YES )
  else( )
    set( ${VAR} NO )
  endif( )
endmacro()

macro(cp_set_one_value VAR )
  set(options
    )
  set(oneValueArgs
    DEFAULT
    )
  set(multiValueArgs
    )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  if ( DEFINED ARG_${VAR} )
    set( ${VAR} ${ARG_${VAR}} )
  else( )
    if ( DEFINED ARG_DEFAULT )
      set( ${VAR} ${ARG_DEFAULT} )
    elseif ( DEFINED ${VAR}_DEFAULT_VALUE)
      set( ${VAR} ${${VAR}_DEFAULT_VALUE} )
    else( )
      set( ${VAR} )
    endif( )
  endif( )
  string(REPLACE ";" " " ${VAR} "${${VAR}}")
endmacro()

macro(cp_set_multi_value VAR )
  set(options
    QUOTE
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    DEFAULT
    )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  if ( DEFINED ARG_${VAR} )
    set( ${VAR} ${ARG_${VAR}} )
  else( )
    if ( ARG_DEFAULT )
      set( ${VAR} ${ARG_DEFAULT} )
    elseif ( DEFINED ${VAR}_DEFAULT_VALUE)
      set( ${VAR} ${${VAR}_DEFAULT_VALUE} )
    else( )
      set( ${VAR} )
    endif( )
  endif( )
  if ( ARG_QUOTE OR ${VAR}_QUOTE )
    set(QUOTE_CHAR "\"")
  endif()
  string(REPLACE ";" " " ${VAR} "${QUOTE_CHAR}${${VAR}}${QUOTE_CHAR}")
endmacro()

macro( cp_set_template_string VAR )
  set( ${VAR} [=[
#--------------------------------------------
# Project characteristics
#--------------------------------------------
PROJECT_NAME=@PROJECT_NAME@
PROJECT_NUMBER=@PROJECT_VERSION@
@ALIASES@
#--------------------------------------------
# Build related options
#--------------------------------------------
EXTRACT_ALL=@EXTRACT_ALL@
EXTRACT_PRIVATE=@EXTRACT_PRIVATE@
EXTRACT_STATIC=@EXTRACT_STATIC@
EXTRACT_LOCAL_CLASSES=@EXTRACT_LOCAL_CLASSES@
EXTRACT_LOCAL_METHODS=@EXTRACT_LOCAL_METHODS@
EXTRACT_ANON_NSPACES=@EXTRACT_ANON_NSPACES@
HIDE_UNDOC_MEMBERS=@HIDE_UNDOC_MEMBERS@
HIDE_UNDOC_CLASSES=@HIDE_UNDOC_CLASSES@
SORT_BY_SCOPE_NAME=YES
ENABLED_SECTIONS=@ENABLED_SECTIONS@
#--------------------------------------------
# Configuration options related to the input files
#--------------------------------------------
FILTER_PATTERNS=@FILTER_PATTERNS@
INPUT=@INPUT@
INPUT_FILTER=@INPUT_FILTER@
EXTENSION_MAPPING=@EXTENSION_MAPPING@
EXCLUDE=@EXCLUDE@
EXCLUDE_PATTERNS=@EXCLUDE_PATTERNS@
EXCLUDE_SYMBOLS=@EXCLUDE_SYMBOLS@
FILE_PATTERNS=@FILE_PATTERNS@
RECURSIVE=YES
#--------------------------------------------
# Quiet
#--------------------------------------------
QUIET=@QUIET@
#--------------------------------------------
# How to handle warnings
#--------------------------------------------
WARNINGS=YES
WARN_NO_PARAMDOC=YES
WARN_IF_UNDOCUMENTED=YES
WARN_LOGFILE=doxygen_warnings.log
#--------------------------------------------
# Filtering of the source (Converting from Perceps to doxygen)
#--------------------------------------------
EXCLUDE_PATTERNS=@EXCLUDE_PATTERNS@
FILTER_SOURCE_FILES=NO
EXAMPLE_PATH=@EXAMPLE_PATH@
VERBATIM_HEADERS=NO
#--------------------------------------------
# Where to output
#--------------------------------------------
OUTPUT_DIRECTORY=@OUTPUT_DIRECTORY@
STRIP_FROM_PATH=@STRIP_FROM_PATH@
#--------------------------------------------
# Types of output
#--------------------------------------------
GENERATE_MAN=@GENERATE_MAN@
#--------------------------------------------
# Source browsing related options
#--------------------------------------------
INLINE_SOURCES=NO
INLINE_INHERITED_MEMB=YES
#--------------------------------------------
# Preprocessor related options
#--------------------------------------------
ENABLE_PREPROCESSING=YES
MACRO_EXPANSION=YES
EXPAND_ONLY_PREDEF=YES
EXPAND_AS_DEFINED=@EXPAND_AS_DEFINED@
SEARCH_INCLUDES=YES
INCLUDE_PATH=@INCLUDE_PATH@
PREDEFINED= __cplusplus=1 DOCUMENTATION_DOXYGEN=1 LDASTOOLS_DEPRICATED(msg)=/** \depricated msg */
#--------------------------------------------
# External reference options
#--------------------------------------------
GENERATE_TAGFILE=@GENERATE_TAGFILE@
TAGFILES=@TAGFILES@
#--------------------------------------------
# HTML Options
#--------------------------------------------
GENERATE_HTML=@GENERATE_HTML@
#--------------------------------------------
# Latex Options
#--------------------------------------------
GENERATE_LATEX=@GENERATE_LATEX@
#--------------------------------------------
# DOT Options
#--------------------------------------------
HAVE_DOT=@HAVE_DOT@
COLLABORATION_GRAPH=@COLLABORATION_GRAPH@
GROUP_GRAPHS=@GROUP_GRAPHS@
    ]=])
endmacro( )

function(cx_target_manual)

  find_package(Doxygen)

  if (NOT DOXYGEN_FOUND)
    #--------------------------------------------------------------------
    # Nothing to do so leave now
    #--------------------------------------------------------------------
    return( )
  endif()
  if ( DOXYGEN_DOT_FOUND )
    set( HAVE_DOT YES )
  else( )
    set( HAVE_DOT NO )
  endif( )
  #----------------------------------------------------------------------
  # With doxygen available, a manual can be generated
  #----------------------------------------------------------------------
  set(options
    VERBOSE
    )
  set(oneValueArgs
    CONFIGURATION
    OUTPUT_DIRECTORY
    STRIP_FROM_PATH
    TARGET
    TEMPLATE
    ENABLE_SECTIONS
    GENERATE_TAGFILE
    INSTALL_SUBDIR
    )
  set(multiValueArgs
    ALIASES
    INCLUDE_PATH
    EXAMPLE_PATH
    EXPAND_AS_DEFINED
    TAGFILES
    )
  #----------------------------------------------------------------------
  #
  #----------------------------------------------------------------------
  set(CP_VARIABLE_BUILD_OPTIONS
    # -------------------------
    OPTIONS
    EXTRACT_ALL
    EXTRACT_PRIVATE
    EXTRACT_STATIC
    EXTRACT_LOCAL_CLASSES
    EXTRACT_LOCAL_METHODS
    EXTRACT_ANON_NSPACES
    HIDE_UNDOC_MEMBERS
    HIDE_UNDOC_CLASSES
    # -------------------------
    ONE_VALUE_ARGS
    # -------------------------
    MULTI_VALUE_ARGS
    ENABLED_SECTIONS
    )
  #----------------------------------------------------------------------
  set(CP_VARIABLE_INPUT_OPTIONS
    # -------------------------
    MULTI_VALUE_ARGS
    INPUT
    INPUT_FILTER
    EXTENSION_MAPPING
    EXCLUDE
    EXCLUDE_PATTERNS
    EXCLUDE_SYMBOLS
    FILTER_PATTERNS
    FILE_PATTERNS
    )
  set( FILTER_PATTERNS_QUOTE YES )
  set( INPUT_FILTER_QUOTE YES )
  set( INPUT_DEFAULT_VALUE ${PROJECT_SOURCE_DIR} )
  set( EXCLUDE_DEFAULT_VALUE CMakeLists.txt )
  set( EXCLUDE_PATTERNS_DEFAULT_VALUE */sqlite3.* )
  set( FILE_PATTERNS_DEFAULT_VALUE *.tcc *.icc *.cc *.hh *.c *.h *.txt *.dox )
  #----------------------------------------------------------------------
  set(CP_VARIABLE_GRAPH_OPTIONS
    # -------------------------
    OPTIONS
    # -------------------------
    ONE_VALUE_ARGS
    COLLABORATION_GRAPH
    GROUP_GRAPHS
    # -------------------------
    MULTI_VALUE_ARGS
    )
  #----------------------------------------------------------------------
  set(CP_VARIABLE_HTML_OPTIONS
    # -------------------------
    OPTIONS
    GENERATE_HTML
    # -------------------------
    ONE_VALUE_ARGS
    # -------------------------
    MULTI_VALUE_ARGS
    )
  #----------------------------------------------------------------------
  set(CP_VARIABLE_LATEX_OPTIONS
    # -------------------------
    OPTIONS
    GENERATE_LATEX
    # -------------------------
    ONE_VALUE_ARGS
    # -------------------------
    MULTI_VALUE_ARGS
    )
  #----------------------------------------------------------------------
  set(CP_VARIABLE_MAN_OPTIONS
    # -------------------------
    OPTIONS
    GENERATE_MAN
    # -------------------------
    ONE_VALUE_ARGS
    # -------------------------
    MULTI_VALUE_ARGS
    )
  #----------------------------------------------------------------------

  cp_doxygen_options(${CP_VARIABLE_BUILD_OPTIONS})
  cp_doxygen_options(${CP_VARIABLE_INPUT_OPTIONS})
  cp_doxygen_options(${CP_VARIABLE_GRAPH_OPTIONS})
  cp_doxygen_options(${CP_VARIABLE_HTML_OPTIONS})
  cp_doxygen_options(${CP_VARIABLE_LATEX_OPTIONS})
  cp_doxygen_options(${CP_VARIABLE_MAN_OPTIONS})

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if(ARG_ALIASES)
    foreach(alias ${ARG_ALIASES})
      list(APPEND ALIASES "ALIASES += ${alias}")
    endforeach()
    string(REPLACE ";" "\n" ALIASES "${ALIASES}")
  else()
    set(ALIASES \#ALIASES=)
  endif()
  if (ARG_VERBOSE)
    set(QUIET NO)
  else()
    set(QUIET YES)
  endif()
  if ( ARG_INSTALL_SUBDIR )
    set( DOC_SUBDIR "/${ARG_INSTALL_SUBDIR}" )
  endif( )
  if ( NOT ARG_TARGET )
    cm_msg_error( "cx_target_manual called without TARGET argument" )
  endif( )
  if ( ARG_TEMPLATE )
    get_filename_component( output_ ${ARG_TEMPLATE} NAME )
    string( REGEX REPLACE "[.]in$" "" output_ ${output_} )
  else( )
    cp_set_template_string( TEMPLATE_STRING )
    set( output_ doxygen.cfg )
  endif( )
  set( cfg_ ${CMAKE_CURRENT_BINARY_DIR}/${ARG_TARGET}/${output_} )
  file( MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${ARG_TARGET} )

  if ( ARG_CONFIGURATION STREQUAL "Developer" )
    set( ARG_EXTRACT_ALL YES )
    set( ARG_EXTRACT_PRIVATE YES )
    set( ARG_EXTRACT_STATIC YES )
    set( ARG_EXTRACT_LOCAL_CLASSES YES )
    set( ARG_EXTRACT_LOCAL_METHODS YES )
    set( ARG_EXTRACT_ANON_NSPACES YES )
  elseif ( ARG_CONFIGURATION STREQUAL "User" )
    set( ARG_HIDE_UNDOC_MEMBERS YES )
    set( ARG_HIDE_UNDOC_CLASSES YES )
  elseif ( ARG_CONFIGURATION STREQUAL "Admin" )
    set( ARG_HIDE_UNDOC_MEMBERS YES )
    set( ARG_HIDE_UNDOC_CLASSES YES )
  endif( )

  cp_set_one_value( ENABLE_SECTIONS )
  cp_set_one_value( OUTPUT_DIRECTORY DEFAULT ${CMAKE_CURRENT_BINARY_DIR}/${ARG_TARGET} )
  cp_set_one_value( STRIP_FROM_PATH DEFAULT ${CMAKE_BINARY_DIR}/src )
  cp_set_one_value( TARGET )

  cp_set_one_value( INCLUDE_PATH )
  cp_set_multi_value( EXAMPLE_PATH )

  cp_doxygen_eval_options( ${CP_VARIABLE_BUILD_OPTIONS} )
  cp_doxygen_eval_options( ${CP_VARIABLE_INPUT_OPTIONS} )
  cp_doxygen_eval_options( ${CP_VARIABLE_GRAPH_OPTIONS} )
  cp_doxygen_eval_options( ${CP_VARIABLE_HTML_OPTIONS} )
  cp_doxygen_eval_options( ${CP_VARIABLE_LATEX_OPTIONS} )
  cp_doxygen_eval_options( ${CP_VARIABLE_MAN_OPTIONS} )

  if ( ARG_TEMPLATE )
    configure_file( ${ARG_TEMPLATE} ${cfg_} @ONLY )
  else( )
    string( CONFIGURE ${TEMPLATE_STRING} TEMPLATE_STRING @ONLY )
    file( WRITE ${cfg_} ${TEMPLATE_STRING} )
  endif ( )

  add_custom_target( ${ARG_TARGET}
    COMMAND ${DOXYGEN_EXECUTABLE} ${cfg_}
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    DEPENDS ${cfg_}
    COMMENT "Generating ${ARG_TARGET} documentation"
    VERBATIM)


  if ( NOT TARGET doc )
    add_custom_target( doc )
  endif( )
  add_dependencies( doc ${ARG_TARGET} )
  install(CODE
    "execute_process(COMMAND \"${CMAKE_COMMAND}\" --build \"${CMAKE_CURRENT_BINARY_DIR}\" --target ${ARG_TARGET})")
  install(
    DIRECTORY ${OUTPUT_DIRECTORY}/html/
    DESTINATION ${CMAKE_INSTALL_DOCDIR}${DOC_SUBDIR}/${ARG_TARGET})
endfunction()
