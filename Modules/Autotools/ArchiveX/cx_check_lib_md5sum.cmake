#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_check_lib_md5sum
#------------------------------------------------------------------------
include( CMakeParseArguments )
include( Autotools/cm_msg_error )

function(cx_check_lib_md5sum)
  set(options)
  set(oneValueArgs)
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  cm_check_headers( "CommonCrypto/CommonDigest.h" )
  cm_check_headers("openssl/md5.h")
  cm_check_headers( "md5.h" )
  cm_check_headers( "md5global.h" )
  if ( ${CMAKE_SYSTEM_NAME} STREQUAL "Linux" )
    cm_check_lib( "crypto" "MD5_Init" )
    if ( HAVE_LIBCRYPTO )
	    set( HAVE_MD5_IN_CRYPTO 1 )
      find_library( LIBMD5 "crypto" )
    else ( HAVE_LIBCRYPTO )
      cm_check_lib( "md5" "MD5_Init" )
      if ( HAVE_LIBMD5 )
	      find_library( LIBMD5 "md5" )
      endif ( HAVE_LIBMD5 )
    endif ( HAVE_LIBCRYPTO )
  else ( ${CMAKE_SYSTEM_NAME} STREQUAL "Linux" )
    cm_check_lib( "md5" "MD5Init" )
    if ( HAVE_LIBMD5 )
      find_library( LIBMD5 "md5" )
    else ( HAVE_LIBMD5 )
      cm_check_lib( "crypto" "MD5_Init" )
      if ( HAVE_LIBCRYPTO )
	      set( HAVE_MD5_IN_CRYPTO 1 )
	      find_library( LIBMD5 "crypto" )
      endif ( HAVE_LIBCRYPTO )
    endif ( HAVE_LIBMD5 )
  endif ( ${CMAKE_SYSTEM_NAME} STREQUAL "Linux" )
  if ( NOT LIBMD5 )
    cm_msg_error( "*** Missing md5 support ***" )
  endif ( NOT LIBMD5 )
  cm_define(
    VARIABLE HAVE_MD5_IN_CRYPTO 
    DESCRIPTION "Defined if have md5 routines in libcrypto" )

endfunction(cx_check_lib_md5sum)
