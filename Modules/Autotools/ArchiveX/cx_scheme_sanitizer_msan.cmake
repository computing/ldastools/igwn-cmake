# MSan-specific options
set(MSAN_OPTIONS_LIST
  ENABLE
  DISABLE_RECOVERY
)
set(MSAN_ONE_VALUE_ARGS
  OUTPUT
  SUPPRESSION_FILE
)
set(MSAN_MULTI_VALUE_ARGS
  FLAGS
)

function(cx_scheme_sanitizer_msan)
  # Parse arguments for MemorySanitizer
  cmake_parse_arguments(CX_MSAN "${MSAN_OPTIONS_LIST}" "${MSAN_ONE_VALUE_ARGS}"
    "${MSAN_MULTI_VALUE_ARGS}" ${ARGN}
  )

  # Append MemorySanitizer to the global list if enabled
  if(CX_MSAN_ENABLE)
    list(APPEND CX_SCHEME_SANITIZERS_ENABLED "memory")
  endif()

  # Handle MSan-specific flags
  if(CX_MSAN_DISABLE_RECOVERY)
    set(MEMORY_FLAGS "-fno-sanitize-recover=memory")
  endif()

  if(CX_MSAN_FLAGS)
    list(APPEND MEMORY_FLAGS "${CX_MSAN_FLAGS}")
  endif()

  # Set suppression file if provided
  if(CX_MSAN_SUPPRESSION_FILE)
    set(ENV{MSAN_OPTIONS} "$ENV{MSAN_OPTIONS}:suppressions=${CX_MSAN_SUPPRESSION_FILE}")
  endif()

  # Set debugging verbosity level
  if (SANITIZERS_DEBUG_LEVEL)
    set(ENV{ASAN_OPTIONS} "$ENV{ASAN_OPTIONS}:verbosity=${SANTIZER_DEBUG_LEVEL}")
  endif( )

  # Set the generated MSan flags to the output or globally
  if(CX_MSAN_OUTPUT)
    set(${CX_MSAN_OUTPUT} "${MEMORY_FLAGS}" PARENT_SCOPE)
  else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${MEMORY_FLAGS}")
  endif()
  if ( ENV{MSAN_OPTIONS} )
    set( ENV{MSAN_OPTIONS} "$ENV{MSAN_OPTIONS}" PARENT_SCOPE )
  endif( )
endfunction()
