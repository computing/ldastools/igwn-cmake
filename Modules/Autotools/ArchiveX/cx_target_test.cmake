#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )
include( GNUInstallDirs )
include( GNUPkgInstallDirs )

function( cx_target_test target )
  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    COMMAND
    DEPENDS
    ENVIRONMENT
    SETUP_COMMAND
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  add_test( NAME ${target} COMMAND ${ARG_COMMAND} )
  if ( ARG_ENVIRONMENT )
    set_tests_properties(
      ${target}
      PROPERTIES
      ENVIRONMENT "${ARG_ENVIRONMENT}"
      DEPENDS "${ARG_DEPENDS}"
      )
  endif( )
  if ( ARG_SETUP_COMMAND )
    add_test(
      NAME setup_${target}
      COMMAND ${ARG_SETUP_COMMAND}
      )
    set_tests_properties(
      setup_${target}
      PROPERTIES
      FAIL_REGULAR_EXPRESSION "__NEVER_FAILS__"
      )
    set_tests_properties(
      ${target}
      PROPERTIES
      DEPENDS setup_${target}
      )
  endif( )
endfunction( )
