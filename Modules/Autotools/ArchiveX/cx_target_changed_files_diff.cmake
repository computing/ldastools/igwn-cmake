# -----------------------------------------------------------------------
# -----------------------------------------------------------------------

include( Autotools/ArchiveX/cx_prog_changed_files )

function( cx_target_changed_files_diff )
  cx_prog_changed_files( )
  if ( NOT PROGRAM_DIFF )
    find_program( PROGRAM_DIFF diff )
  endif( )

  set( options
    )
  set( oneValueArgs
    GIT_PREFIX
    INSTALLED_PREFIX
    TARGET_PREFIX
    )
  set( multiValueArgs
    FILES
    )

  cmake_parse_arguments( ARG
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  set( target ${ARG_TARGET_PREFIX}-diff-changed-files )
  add_custom_target(
    ${target}
    ${CMAKE_COMMAND}
    -DMODE=diff
    -DPROGRAM=${PROGRAM_DIFF}
    "-DGIT_PREFIX=${ARG_GIT_PREFIX}"
    "-DINSTALLED_PREFIX=${ARG_INSTALLED_PREFIX}"
    "-DFILES='${ARG_FILES}'"
    -P ${PROGRAM_CHANGED_FILES}
    )
  if ( NOT TARGET diff-changed-files )
    add_custom_target( diff-changed-files )
  endif( )
  add_dependencies( diff-changed-files ${target} )
endfunction( )
