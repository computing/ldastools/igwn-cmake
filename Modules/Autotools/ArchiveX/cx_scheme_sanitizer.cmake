#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_scheme_sanitizer( )
#
#------------------------------------------------------------------------
cmake_minimum_required( VERSION 3.3 )
if ( POLICY CMP0057 )
  cmake_policy(SET CMP0057 NEW)
endif( )

include( CMakeParseArguments )
include( CheckCXXCompilerFlag )

include( Autotools/cm_msg_notice )
include( Autotools/cm_msg_error )
include( Autotools/cm_msg_failure )
include( Autotools/ArchiveX/cx_c_append_flags )
include( Autotools/ArchiveX/cx_cxx_append_flags )
include( Autotools/ArchiveX/cx_msg_debug )
include( Autotools/ArchiveX/cx_msg_debug_variable )

include( Autotools/ArchiveX/cx_scheme_sanitizer_asan )
include( Autotools/ArchiveX/cx_scheme_sanitizer_lsan )
include( Autotools/ArchiveX/cx_scheme_sanitizer_msan )
include( Autotools/ArchiveX/cx_scheme_sanitizer_tsan )
include( Autotools/ArchiveX/cx_scheme_sanitizer_ubsan )
# include( Autotools/ArchiveX/cx_scheme_sanitizer_utilities )

# Define a cache variable for the sanitizers, allowing the user to pass sanitizers at configuration time
set(SANITIZERS "address" CACHE STRING "List of sanitizers to enable (e.g., address,undefined)")

# Example call to format_sanitizer_list with ALLOW_WARNINGS enabled
#format_sanitizer_list(AVAILABLE_SANITIZERS "address;leak;undefined;thread"
#                      OUTPUT_VAR MY_SANITIZER_FLAGS                     ALLOW_WARNINGS)
##
# @brief Configures both AddressSanitizer (ASan) and LeakSanitizer (LSan)
# options for the project, including suppression of known errors.
#
# This function allows enabling and customizing both AddressSanitizer and LeakSanitizer
# flags for the project, including toggling specific checks, handling recovery behavior,
# specifying unique suppression files for ASan and LSan, and appending custom flags.
#
# @param ENABLE_ASAN
#   Enables the use of AddressSanitizer. If this option is not specified,
#   AddressSanitizer will not be enabled.
#   @see cx_scheme_sanitizer_asan()
#   @example
#     cx_scheme_sanitizer(ENABLE_ASAN)
#     # Enables AddressSanitizer with default settings.
#
# @param ENABLE_LSAN
#   Enables the use of LeakSanitizer. If this option is not specified,
#   LeakSanitizer will not be enabled.
#   @see cx_scheme_sanitizer_lsan()
#   @example
#     cx_scheme_sanitizer(ENABLE_LSAN)
#     # Enables LeakSanitizer with default settings.
#
# @param ENABLE_MSAN
#   Enables the use of LeakSanitizer. If this option is not specified,
#   LeakSanitizer will not be enabled.
#   @see cx_scheme_sanitizer_msan()
#   @example
#     cx_scheme_sanitizer(ENABLE_MSAN)
#     # Enables LeakSanitizer with default settings.
#
# @param ENABLE_TSAN
#   Enables the use of LeakSanitizer. If this option is not specified,
#   LeakSanitizer will not be enabled.
#   @see cx_scheme_sanitizer_tsan()
#   @example
#     cx_scheme_sanitizer(ENABLE_TSAN)
#     # Enables LeakSanitizer with default settings.
#
# @param ENABLE_UBSAN
#   Enables the use of LeakSanitizer. If this option is not specified,
#   LeakSanitizer will not be enabled.
#   @see cx_scheme_sanitizer_ubsan()
#   @example
#     cx_scheme_sanitizer(ENABLE_UBSAN)
#     # Enables LeakSanitizer with default settings.
#
# @param SUPPRESSION_FILE_ASAN
#   Specifies the path to a suppression file for AddressSanitizer, where known
#   errors (e.g., use-after-free or buffer overflows) will be ignored.
#   @see cx_scheme_sanitizer_asan()
#   @example
#     cx_scheme_sanitizer(ENABLE_ASAN SUPPRESSION_FILE_ASAN "/path/to/asan_suppressions.txt")
#     # Enables AddressSanitizer and applies a suppression file for known issues.
#
# @param SUPPRESSION_FILE_LSAN
#   Specifies the path to a suppression file for LeakSanitizer, where known
#   memory leaks will be ignored.
#   @see cx_scheme_sanitizer_lsan()
#   @example
#     cx_scheme_sanitizer(ENABLE_LSAN SUPPRESSION_FILE_LSAN "/path/to/lsan_suppressions.txt")
#     # Enables LeakSanitizer and applies a suppression file for known memory leaks.
#
# @param SUPPRESSION_FILE_MSAN
#   Specifies the path to a suppression file for LeakSanitizer, where known
#   memory leaks will be ignored.
#   @see cx_scheme_sanitizer_msan()
#   @example
#     cx_scheme_sanitizer(ENABLE_MSAN SUPPRESSION_FILE_MSAN "/path/to/msan_suppressions.txt")
#     # Enables LeakSanitizer and applies a suppression file for known memory leaks.
#
# @param SUPPRESSION_FILE_TSAN
#   Specifies the path to a suppression file for LeakSanitizer, where known
#   memory leaks will be ignored.
#   @see cx_scheme_sanitizer_tsan()
#   @example
#     cx_scheme_sanitizer(ENABLE_TSAN SUPPRESSION_FILE_TSAN "/path/to/tsan_suppressions.txt")
#     # Enables LeakSanitizer and applies a suppression file for known memory leaks.
#
# @param SUPPRESSION_FILE_UBSAN
#   Specifies the path to a suppression file for LeakSanitizer, where known
#   memory leaks will be ignored.
#   @see cx_scheme_sanitizer_ubsan()
#   @example
#     cx_scheme_sanitizer(ENABLE_UBSAN SUPPRESSION_FILE_UBSAN "/path/to/ubsan_suppressions.txt")
#     # Enables LeakSanitizer and applies a suppression file for known memory leaks.
#
# @param OUTPUT
#   The name of the variable where the constructed sanitizer flags should
#   be stored. If not specified, the flags will be applied globally to
#   CMAKE_CXX_FLAGS.
#   @example
#     cx_scheme_sanitizer(ENABLE_ASAN OUTPUT MY_SANITIZER_FLAGS)
#     # Stores the generated sanitizer flags in the MY_SANITIZER_FLAGS variable.
#
# @see cx_scheme_sanitizer_asan()
# @see cx_scheme_sanitizer_lsan()
# @see cx_scheme_sanitizer_msan()
# @see cx_scheme_sanitizer_tsan()
# @see cx_scheme_sanitizer_ubsan()
#
# @example
#   # Enable both AddressSanitizer and LeakSanitizer with separate suppression files:
#   cx_scheme_sanitizer(ENABLE_ASAN ENABLE_LSAN SUPPRESSION_FILE_ASAN "/path/to/asan_suppressions.txt"
#   SUPPRESSION_FILE_LSAN "/path/to/lsan_suppressions.txt" FLAGS "-fsanitize=undefined")
#
# @note This function is dependent on the availability of both AddressSanitizer and
# LeakSanitizer, typically available in GCC and Clang compilers.
#
# @note Sanitizer Compatibility Table:
# <table>
# <thead>
#   <tr>
#     <th>Sanitizer</th>
#     <th>Compatible with ASan/LSan</th>
#     <th>Compatible with UBSan</th>
#     <th>Compatible with TSan</th><th>Compatible with MSan</th>
#   </tr>
# </thead>
# <tbody>
#   <tr>
#     <td><b>AddressSanitizer (ASan)</b></td>
#     <td>Yes</td>
#     <td>Yes</td>
#     <td>No</td>
#     <td>No</td>
#   </tr>
#   <tr>
#     <td><b>LeakSanitizer (LSan)</b></td>
#     <td>Yes (included with ASan)</td>
#     <td>Yes</td>
#     <td>No</td>
#     <td>No</td>
#   </tr>
#   <tr>
#     <td><b>UndefinedBehaviorSanitizer (UBSan)</b></td>
#     <td>Yes</td>
#     <td>N/A</td>
#     <td>No</td>
#     <td>No</td>
#   </tr>
#   <tr>
#     <td><b>ThreadSanitizer (TSan)</b></td>
#     <td>No</td>
#     <td>No</td>
#     <td>N/A</td>
#     <td>No</td>
#   </tr>
#   <tr>
#     <td><b>MemorySanitizer (MSan)</b></td>
#     <td>No</td><td>No</td>
#     <td>No</td>
#     <td>N/A</td>
#   </tr>
# </tbody>
# </table>
#
# @note
#   <b>Result Variables</b>
#   CX_SCHEME_SANITIZERS_ACTIVE - a list of currently active sanitizers
function(cx_scheme_sanitizer)
  ##
  # @brief Retrieves the function name corresponding to a specified sanitizer.
  #
  # This function maps a given sanitizer to its corresponding function name
  # (e.g., "ASAN" maps to "address") and assigns the result to the specified
  # output variable. If the sanitizer is not recognized, the function raises
  # a fatal error.
  #
  # @param[in] SANITIZER The name of the sanitizer (e.g., "ASAN", "LSAN", etc.).
  #                      Recognized sanitizers are:
  #                      - "ASAN" (Address Sanitizer)
  #                      - "LSAN" (Leak Sanitizer)
  #                      - "MSAN" (Memory Sanitizer)
  #                      - "TSAN" (Thread Sanitizer)
  #                      - "UBSAN" (Undefined Behavior Sanitizer)
  # @param[out] VARIABLE The name of the variable that will hold the corresponding
  #                      sanitizer function name. The value is set in the parent
  #                      scope.
  #
  # @par Output:
  # - The function sets the specified `VARIABLE` in the parent scope to the
  #   sanitizer function name corresponding to the given `SANITIZER` input.
  #   If the sanitizer is not found, a fatal error is raised.
  #
  # @note If the `SANITIZER` is not one of the recognized values, the function will
  #       stop execution with an error message using `cm_msg_failure()`.
  #
  # @throws FATAL_ERROR If the `SANITIZER` is not recognized.
  #
  # Example:
  # @code
  # get_sanitizer_function("ASAN" sanitizer_var)
  # message(STATUS "Sanitizer function: ${sanitizer_var}")  # Outputs: address
  #
  # get_sanitizer_function("XYZ" sanitizer_var)
  # # This will result in an error: "Error: Sanitizer 'XYZ' not recognized."
  # @endcode
  ##
  function(get_sanitizer_function SANITIZER VARIABLE)
    # List of sanitizer-function pairs
    set(sanitizer_pairs
      "ASAN" "address"
      "LSAN" "leak"
      "MSAN" "memory"
      "TSAN" "thread"
      "UBSAN" "undefined"
    )

    # Initialize an empty variable for the function name
    set(sanitizer_function "")

    # Iterate through the list and find the matching sanitizer
    list(LENGTH sanitizer_pairs len)
    math(EXPR max_index "${len} - 1")

    set(found_sanitizer FALSE)

    foreach(current_sanitizer_index RANGE 0 ${max_index} 2)
      math(EXPR current_function_index "${current_sanitizer_index} + 1")
      list(GET sanitizer_pairs ${current_sanitizer_index} current_sanitizer)
      list(GET sanitizer_pairs ${current_function_index} current_function)

      if(SANITIZER STREQUAL current_sanitizer)
        set(sanitizer_function "${current_function}")
        set(found_sanitizer TRUE)
        break()
      endif()
    endforeach()

    # Error handling: If sanitizer is not found, display an error and stop execution
    if(NOT found_sanitizer)
      cm_msg_failure("Error: Sanitizer '${SANITIZER}' not recognized.")
    endif()

    # Set the result in the parent scope
    set(${VARIABLE} "${sanitizer_function}" PARENT_SCOPE)
  endfunction()

  ##
  # @brief Checks if a specified sanitizer is enabled and sets a corresponding
  #        enable flag.
  #
  # This macro checks if a given sanitizer is enabled by examining the variable
  # `CX_SANITIZER_<SANITIZER>`. If the sanitizer is enabled, it sets a corresponding
  # enable flag in the format `ENABLE_<SANITIZER>` to the value `"ENABLE"`.
  #
  # @param[in] SANITIZER The name of the sanitizer to check (e.g., Address, Memory,
  #                      etc.).
  #
  # @pre The calling scope must define a variable named `CX_SANITIZER_<SANITIZER>`,
  #      where `<SANITIZER>` is the name of the sanitizer passed to this macro.
  #      This variable should be set to a truthy value if the sanitizer is enabled.
  #
  # @par Input:
  # - `SANITIZER`: The name of the sanitizer to check.
  # - `CX_SANITIZER_<SANITIZER>`: A truthy variable that indicates whether
  #   the sanitizer is enabled.
  #
  # @par Output:
  # - If the sanitizer is enabled, the macro sets a variable `ENABLE_<SANITIZER>`
  #   with the value `"ENABLE"`.
  #
  # Example:
  # @code
  # check_if_enabled(ASAN)
  # message(STATUS "ENABLE_ASAN: ${ENABLE_ASAN}")
  # @endcode
  ##
  macro(check_if_enabled SANITIZER)
    if ( CX_SANITIZER_${SANITIZER} )
      set(ENABLE_${SANITIZER} ENABLE)
    endif( )
  endmacro( )

  ##
  # @brief Appends a sanitizer function to the list of available sanitizers.
  #
  # This macro checks if a specified sanitizer is enabled and, if it is, retrieves
  # the associated sanitizer function and appends it to the global list of
  # available sanitizers.
  #
  # @param[in] SANITIZER The name of the sanitizer to check and append (e.g.,
  #                      Address, Memory, etc.).
  #
  # @pre The calling scope must define a variable named `CX_SANITIZER_ENABLE_<SANITIZER>`,
  #      where `<SANITIZER>` is the name of the sanitizer passed to this macro.
  #      This variable should be set to a truthy value if the sanitizer is enabled.
  #      Additionally, the macro `get_sanitizer_function()` must be available in
  #      the calling scope to retrieve the sanitizer function.
  #
  # @par Input:
  # - `SANITIZER`: The name of the sanitizer to check.
  # - `CX_SANITIZER_ENABLE_<SANITIZER>`: A truthy variable that indicates whether
  #   the sanitizer is enabled.
  #
  # @par Output:
  # - Appends the sanitizer function to the global list `available_sanitizers`.
  #
  # @note This macro internally calls the `get_sanitizer_function()` macro to
  #       retrieve the sanitizer function for the given sanitizer.
  #
  # Example:
  # @code
  # set(CX_SANITIZER_ENABLE_Address ON)
  # append_available_sanitizer(Address)
  # @endcode
  ##
  macro(append_available_sanitizer SANITIZER)
    if ( CX_SANITIZER_ENABLE_${SANITIZER} )
      get_sanitizer_function( ${SANITIZER} SANITIZER_FUNCTION )
      list(APPEND available_sanitizers ${SANITIZER_FUNCTION} )
    endif( )
  endmacro( )

  ##
  # @brief Combines multiple sanitizer ignorelists into a single file
  #        located in the CMake binary directory.
  #
  # This function scans the provided list of sanitizer flags for any
  # `-fsanitize-ignorelist` options. It removes these options from the
  # list, collects their file paths, and combines the contents of each
  # specified ignorelist into a single file named
  # `combined_ignorelist.txt` located in the CMake binary directory.
  # The function then appends a new `-fsanitize-ignorelist` flag
  # pointing to this combined file back into the sanitizer flag list.
  #
  # @param output_list [in, out] The list of sanitizer flags. This list
  #        is modified by removing existing `-fsanitize-ignorelist`
  #        flags, creating a combined ignorelist, and adding a new
  #        `-fsanitize-ignorelist` flag that points to the combined
  #        ignorelist file.
  #
  # @note The combined ignorelist file is always stored in
  #       `${CMAKE_BINARY_DIR}/combined_ignorelist.txt`.
  #
  # @note If no ignorelist files are specified in the input list, the
  #       function simply appends the new `-fsanitize-ignorelist` flag
  #       pointing to an empty combined ignorelist file.
  ##
  function(create_combined_ignorelist)
    set(options)
    set(one_value_args OUTPUT_LIST)
    set(multi_value_args OPTIONS ONE_VALUE MULTI_VALUE)

    # Parse arguments
    cmake_parse_arguments(
      ARG
      "${options}"
      "${one_value_args}"
      "${multi_value_args}"
      ${ARGN}
    )
    set(compiler_option "-fsanitize-blacklist" )

    # Initialize the modified flags list and a list to collect ignorelist paths
    set(new_flags "")
    set(ignorelists "")

    # Iterate over the input list to remove -fsanitize-ignorelist flags and collect paths
    foreach(flag IN LISTS ${ARG_OUTPUT_LIST})
      if(flag MATCHES "${compiler_option}=(.*)")
        # Extract the path from the flag and add it to the ignorelists
        string(REGEX REPLACE "${compiler_option}=(.*)" "\\1" ignorelist_path "${flag}")
        list(APPEND ignorelists "${ignorelist_path}")
      else()
        # Keep other flags as they are
        list(APPEND new_flags "${flag}")
      endif()
    endforeach()
    if( NOT ignorelists )
      return( )
    endif( )

    # Set the combined ignorelist file path in the CMake binary directory
    set(combined_ignorelist "${CMAKE_BINARY_DIR}/combined_ignorelist.txt")

    # Initialize the combined ignorelist file content
    file(WRITE "${combined_ignorelist}" "# Combined Ignorelist\n\n")

    # Iterate over the collected ignorelists and append their contents to the combined file
    foreach(ignorelist IN LISTS ignorelists)
      if(EXISTS "${ignorelist}")
        file(APPEND "${combined_ignorelist}" "# Contents of ${ignorelist}\n")
        file(READ "${ignorelist}" content)
        file(APPEND "${combined_ignorelist}" "${content}\n")
      endif()
    endforeach()

    # Add the new -fsanitize-ignorelist flag pointing to the combined file
    list(APPEND new_flags "${compiler_option}=${combined_ignorelist}")

    # Set the output list with the modified flags
    set(${ARG_OUTPUT_LIST} "${new_flags}" PARENT_SCOPE)
  endfunction()

  ##
  # @brief Retrieves and sets options for a given sanitizer.
  #
  # This macro retrieves various options, one-value arguments, and multi-value
  # arguments related to a specified sanitizer and sets them based on pre-existing
  # variables in the calling scope.
  #
  # @param[in] SANITIZER The name of the sanitizer (e.g., ASAN, UBSAN, etc.).
  # @param[in] OPTIONS A list of flag-like options that do not require values.
  # @param[in] ONE_VALUE A list of options that require exactly one value.
  # @param[in] MULTI_VALUE A list of options that can accept multiple values.
  # @param[in] ARGN Additional arguments passed to the macro to be parsed as
  #                 options, one-value arguments, or multi-value arguments.
  #
  # @pre The calling scope must define variables of the form
  #      `CX_SANITIZER_<SANITIZER>_<OPTION>`, where `<SANITIZER>` is the value
  #      passed as the `SANITIZER` argument, and `<OPTION>` corresponds to possible
  #      options for that sanitizer.
  #
  # @note This macro uses `cmake_parse_arguments()` to handle the parsing of
  #       optional arguments. It parses the provided arguments into three
  #       categories: options (without values), one-value arguments, and
  #       multi-value arguments. The parsed results are stored in the following
  #       special variables:
  #       - `GSO_OPTIONS`: Stores flag-like options without values.
  #       - `GSO_ONE_VALUE`: Stores options that take exactly one value.
  #       - `GSO_MULTI_VALUE`: Stores options that take multiple values.
  #
  # @par Input:
  # - `SANITIZER`: The sanitizer name.
  # - `OPTIONS`: A list of options that do not require values (parsed into
  #              `GSO_OPTIONS`).
  # - `ONE_VALUE`: A list of one-value arguments (parsed into `GSO_ONE_VALUE`).
  # - `MULTI_VALUE`: A list of multi-value arguments (parsed into
  #                  `GSO_MULTI_VALUE`).
  # - `ARGN`: A list of additional arguments passed when calling this macro
  #           (handled by `cmake_parse_arguments()`).
  #
  # @par Output:
  # - Variables are set based on pre-existing variables in the calling scope,
  #   following the format `SANITIZER_<OPTION>`, `SANITIZER_<ONE_VALUE_OPTION>`,
  #   and `SANITIZER_<MULTI_VALUE_OPTION>`.
  #
  # Example:
  # @code
  # get_sanitizer_options(ASAN OPTIONS ENABLE)
  # @endcode
  ##
  macro( get_sanitizer_options SANITIZER )
    set(options)
    set(one_value_args)
    set(multi_value_args OPTIONS ONE_VALUE MULTI_VALUE)

    # Parse arguments
    cmake_parse_arguments(
      GSO
      "${options}"
      "${one_value_args}"
      "${multi_value_args}"
      ${ARGN}
    )
    foreach( option ${GSO_OPTIONS})
      if ( CX_SANITIZER_${option}_${SANITIZER} )
        set(
          "${option}_${SANITIZER}"
          ${option}
        )
      elseif ( CX_SANITIZER_${option} )
        set(
          "${option}"
          ${option}
        )
        cx_msg_debug_variable( "${option}_${SANITIZER}" )
      endif( )
    endforeach( )
    foreach( one_value_option ${GSO_ONE_VALUE})
      if ( CX_SANITIZER_${one_value_option}_${SANITIZER} )
        set(
          "${one_value_option}_${SANITIZER}"
          ${one_value_option} ${CX_SANITIZER_${one_value_option}_${SANITIZER}}
        )
        cx_msg_debug_variable( "${one_value_option}_${SANITIZER}" )
      endif( )
    endforeach( )
    foreach( multi_value_option ${GSO_MULTI_VALUE})
      if ( CX_SANITIZER_${multi_value_option}_${SANITIZER} )
        set(
          "${multi_value_option}_${SANITIZER}"
          ${multi_value_option} ${CX_SANITIZER_${multi_value_option}_${SANITIZER}}
        )
        cx_msg_debug_variable( "${multi_value_option}_${SANITIZER}" )
      endif( )
    endforeach( )
  endmacro( )

  ##
  # @brief Formats the sanitizer list for use in CMake flags and validates
  #        compatibility using multiple sets of compatible sanitizers.
  #
  # This function checks the requested sanitizers against multiple predefined
  # sets of compatible sanitizers (e.g., ASan, MSan, etc.), and generates CMake
  # sanitizer flags. If no `REQUESTED_SANITIZERS` are provided, it falls back
  # to the cached `SANITIZERS` variable.
  # The function also supports warnings instead of errors via `ALLOW_WARNINGS`.
  #
  # @param [in] AVAILABLE_SANITIZERS
  #   List of sanitizers available in the current environment
  #   (e.g., supported by the compiler).
  #
  # @param [in] REQUESTED_SANITIZERS
  #   List of sanitizers requested by the user during configuration.
  #   If not provided, the function will fall back to the cached
  #   `SANITIZERS` variable.
  #
  # @param [in] ALLOW_WARNINGS
  #   Optional flag to convert fatal errors into warnings, allowing the build
  #   to continue even if incompatible sanitizers are specified.
  #
  # @param [out] OUTPUT_VAR
  #   Name of the variable where the generated sanitizer flags will be stored.
  #   If not provided, the flags will be appended to `CMAKE_CXX_FLAGS`.
  #
  # @param [in,out] ARGN
  #   Variable to capture all arguments passed to the function.
  ##
  function(format_sanitizer_list)

    ##
    # @brief Handles errors or warnings based on the ALLOW_WARNINGS flag.
    #
    # This helper function determines whether to raise a fatal error or issue
    # a warning based on whether `ALLOW_WARNINGS` is set.
    #
    # @param [in] MESSAGE
    #   The message to display as either a fatal error or a warning.
    ##
    macro(handle_error_or_warning MESSAGE)
      if(FSL_ALLOW_WARNINGS)
        cm_msg_notice("${MESSAGE}")
      else()
        cm_msg_failure("${MESSAGE}")
      endif()
    endmacro()

    ##
    # @brief Filters the requested sanitizers based on the available sanitizers.
    #
    # This helper function filters the list of user-requested sanitizers,
    # ensuring only the sanitizers that are available in the current environment
    # are used.
    #
    # @param [in] AVAILABLE_SANITIZERS
    #   List of sanitizers available in the current environment.
    #
    # @param [in] REQUESTED_SANITIZERS
    #   List of sanitizers requested by the user during configuration.
    #
    # @param [out] OUTPUT_VAR
    #   Name of the variable where the filtered valid sanitizers will be stored.
    #
    # @note
    #   Upon successful completion, the variable ACTIVE_SANITIZERS is set in
    #   the parent scope.
    #   This is a list of the human readable versions of the active sanitizers
    #   (ex: address).
    ##
    function(filter_requested_sanitizers
        AVAILABLE_SANITIZERS
        REQUESTED_SANITIZERS
        OUTPUT_VAR)
      set(VALID_SANITIZER_LIST "")

      foreach(SANITIZER IN LISTS REQUESTED_SANITIZERS)
        list(FIND AVAILABLE_SANITIZERS "${SANITIZER}" INDEX)
        if(INDEX GREATER -1)
          list(APPEND VALID_SANITIZER_LIST "${SANITIZER}")
        else()
          cm_msg_notice("Requested sanitizer ${SANITIZER} is not available. "
            "It will be ignored.")
        endif()
      endforeach()

      # Set the filtered list of valid sanitizers to the output variable
      set(${OUTPUT_VAR} "${VALID_SANITIZER_LIST}" PARENT_SCOPE)
    endfunction()

    #====================================================================
    # format_sanitizer_list - function body
    #====================================================================
    # Define options, one_value_args, and multi_value_args for cmake_parse_arguments
    set(options
      ALLOW_WARNINGS
    )
    set(one_value_args OUTPUT_VAR)
    set(multi_value_args AVAILABLE_SANITIZERS REQUESTED_SANITIZERS)

    # Parse arguments
    cmake_parse_arguments(FSL "${options}" "${one_value_args}" "${multi_value_args}" ${ARGN})

    # Ensure available sanitizers are provided
    if(NOT FSL_AVAILABLE_SANITIZERS)
      handle_error_or_warning("No available sanitizers specified. Please provide "
        "a list of available sanitizers.")
      return()  # Exit if no available sanitizers
    endif()

    # If REQUESTED_SANITIZERS is not provided, use the cache variable SANITIZERS
    if(NOT FSL_REQUESTED_SANITIZERS)
      if(DEFINED SANITIZERS)
        string(REPLACE "," ";" sanitizers_list "${SANITIZERS}")
        set(FSL_REQUESTED_SANITIZERS "${sanitizers_list}")
      else()
        handle_error_or_warning("No requested sanitizers provided. Please specify "
          "REQUESTED_SANITIZERS or set the SANITIZERS cache.")
        return()  # Exit if no requested sanitizers
      endif()
    endif()

    # Filter the requested sanitizers based on the available sanitizers
    filter_requested_sanitizers(
      "${FSL_AVAILABLE_SANITIZERS}"
      "${FSL_REQUESTED_SANITIZERS}"
      VALID_SANITIZER_LIST)

    # If no valid sanitizers are left after filtering, raise an error or warning
    if(NOT VALID_SANITIZER_LIST)
      handle_error_or_warning("None of the requested sanitizers are available.")
      return()
    endif()

    ##
    # @var STRING SANITIZER_COMPATIBILITY_LIST
    # Each set defines a group of sanitizers that are compatible with each other.
    # These sets include both multi-sanitizer combinations and standalone
    # sanitizers.
    ##
    set(SANITIZER_COMPATIBILITY_LIST
      "address;leak;undefined"
      "memory"
      "thread"
    )

    # Sort the valid sanitizers for easier comparison
    list(SORT VALID_SANITIZER_LIST)

    # Check if requested sanitizers are a subset of any defined sanitizer set
    set(IS_COMPATIBLE FALSE)
    foreach(SANITIZER IN LISTS SANITIZER_COMPATIBILITY_LIST)
      string(REPLACE ";" " " SANITIZER_LIST "${SANITIZER}")
      string(REPLACE ";" " " VALID_SANITIZER_STR "${VALID_SANITIZER_LIST}")

      if("${VALID_SANITIZER_STR}" MATCHES "${SANITIZER_LIST}")
        set(IS_COMPATIBLE TRUE)
        break()
      endif()
    endforeach()

    # If no compatible combination is found, raise an error or warning
    if(NOT IS_COMPATIBLE)
      handle_error_or_warning("The requested sanitizers (${VALID_SANITIZER_LIST}) "
        "are not compatible. Use compatible combinations from "
        "${SANITIZER_COMPATIBILITY_LIST}.")
      return()
    endif()

    # After validation, append the valid sanitizers with -fsanitize flag
    cx_msg_debug_variable( VALID_SANITIZER_LIST )
    set(FINAL_SANITIZER_LIST "")
    foreach(SANITIZER IN LISTS VALID_SANITIZER_LIST)
      list(APPEND FINAL_SANITIZER_LIST "-fsanitize=${SANITIZER}")
    endforeach()
    cx_msg_debug_variable( FINAL_SANITIZER_LIST )
    if ( CMAKE_BUILD_TYPE STREQUAL "Debug" )
      set(ACTIVE_SANITIZERS "${VALID_SANITIZER_LIST}" PARENT_SCOPE)
      cx_msg_debug_variable( ACTIVE_SANITIZERS )
    endif( )

    # Combine the list into a single string
    string(REPLACE ";" " " FINAL_SANITIZER_FLAGS "${FINAL_SANITIZER_LIST}")
    cx_msg_debug_variable( FINAL_SANITIZER_FLAGS )

    cx_msg_debug_variable( CMAKE_BUILD_TYPE )
    if ( CMAKE_BUILD_TYPE STREQUAL "Debug" )
      # Handle the output: If OUTPUT_VAR is specified, set the variable; otherwise,
      # modify CMAKE_CXX_FLAGS globally
      if(FSL_OUTPUT_VAR)
        cx_msg_debug("Setting ${FLS_OUTPUT_VAR} to ${FINAL_SANITIZER_FLAGS}")
        set(${FSL_OUTPUT_VAR} "${FINAL_SANITIZER_FLAGS}" PARENT_SCOPE)
      else()
        cx_msg_debug( "Calling cx_cxx_append_flags about to be called" )
        cx_c_append_flags( OPTIONS ${FINAL_SANITIZER_FLAGS} )
        cx_cxx_append_flags( OPTIONS ${FINAL_SANITIZER_FLAGS} )
        cx_msg_debug("Extending CMAKE_CXX_FLAGS to contain ${FINAL_SANITIZER_FLAGS}")
        cx_msg_debug_variable( CMAKE_CXX_FLAGS )
      endif()
    endif()
  endfunction()

  #======================================================================
  # cx_scheme_sanitizer - function body
  #======================================================================


  # Union of options for ASan and LSan
  set(ALL_OPTIONS_LIST
    ALLOW_WARNINGS
    ENABLE_ASAN
    DISABLE_RECOVERY_ASAN
    DISABLE_LEAK_DETECTION_ASAN
    ${ASAN_OPTIONS_LIST}
    ENABLE_LSAN
    DISABLE_RECOVERY_LSAN
    ${LSAN_OPTIONS_LIST}
    ENABLE_MSAN
    DISABLE_RECOVERY_MSAN
    DISABLE_LEAK_DETECTION_MSAN
    ${MSAN_OPTIONS_LIST}
    ENABLE_TSAN
    DISABLE_RECOVERY_TSAN
    ${TSAN_OPTIONS_LIST}
    ENABLE_UBSAN
    DISABLE_RECOVERY_UBSAN
    ${UBSAN_OPTIONS_LIST}
  )
  set(ALL_ONE_VALUE_ARGS
    IGNORELIST_FILE_ASAN
    SUPPRESSION_FILE_ASAN
    ${ASAN_ONE_VALUE_ARGS}
    SUPPRESSION_FILE_LSAN
    ${LSAN_ONE_VALUE_ARGS}
    SUPPRESSION_FILE_MSAN
    ${MSAN_ONE_VALUE_ARGS}
    SUPPRESSION_FILE_TSAN
    ${TSAN_ONE_VALUE_ARGS}
    SUPPRESSION_FILE_UBSAN
    ${UBSAN_ONE_VALUE_ARGS}
  )
  set(ALL_MULTI_VALUE_ARGS
    FLAGS_ASAN
    ${ASAN_MULTI_VALUE_ARGS}
    FLAGS_LSAN
    ${LSAN_MULTI_VALUE_ARGS}
    FLAGS_MSAN
    ${MSAN_MULTI_VALUE_ARGS}
    FLAGS_TSAN
    ${TSAN_MULTI_VALUE_ARGS}
    FLAGS_UBSAN
    ${UBSAN_MULTI_VALUE_ARGS}
  )

  list(REMOVE_ITEM ALL_OPTIONS_LIST
    ENABLE
    DISABLE_LEAK_DETECTION
    DISABLE_RECOVERY
  )
  list(REMOVE_ITEM ALL_ONE_VALUE_ARGS
    IGNORELIST_FILE
    OUTPUT
    SUPPRESSION_FILE
  )
  list(REMOVE_ITEM ALL_MULTI_VALUE_ARGS
    FLAGS
  )

  # Parse the union of arguments for both ASan and LSan
  cmake_parse_arguments(CX_SANITIZER "${ALL_OPTIONS_LIST}" "${ALL_ONE_VALUE_ARGS}"
    "${ALL_MULTI_VALUE_ARGS}" ${ARGN}
  )
  #--------------------------------------------------------------------
  # Parse out the requested sanitizers
  #--------------------------------------------------------------------
  append_available_sanitizer(ASAN)
  get_sanitizer_options( ASAN
    OPTIONS
    DISABLE_DETECT_HEAP_USE_AFTER_FREE
    DISABLE_DETECT_ODR_VIOLATION
    DISABLE_LEAK_DETECTION
    DISABLE_RECOVERY
    DISABLE_STACK_USE_AFTER_RETURN
    DISABLE_USE_AFTER_FREE
    ENABLE
    ONE_VALUE IGNORELIST_FILE SUPPRESSION_FILE
    MULTI_VALUE FLAGS
  )
  append_available_sanitizer(LSAN)
  get_sanitizer_options( LSAN
    OPTIONS DISABLE_RECOVERY ENABLE
    ONE_VALUE SUPPRESSION_FILE
    MULTI_VALUE FLAGS
  )
  append_available_sanitizer(MSAN)
  get_sanitizer_options( MSAN
    OPTIONS DISABLE_RECOVERY ENABLE
    ONE_VALUE SUPPRESSION_FILE
    MULTI_VALUE FLAGS
  )
  append_available_sanitizer(TSAN)
  get_sanitizer_options( TSAN
    OPTIONS DISABLE_RECOVERY ENABLE
    ONE_VALUE SUPPRESSION_FILE
    MULTI_VALUE FLAGS
  )
  append_available_sanitizer(UBSAN)
  get_sanitizer_options( UBSAN
    OPTIONS DISABLE_RECOVERY ENABLE
    ONE_VALUE SUPPRESSION_FILE
    MULTI_VALUE FLAGS
  )

  if ( CX_SANITIZER_ALLOW_WARNINGS )
    set( ALLOW_WARNINGS "ALLOW_WARNINGS" )
  endif( )

  format_sanitizer_list(
    ${ALLOW_WARNINGS}
    AVAILABLE_SANITIZERS ${available_sanitizers}
  )
  #--------------------------------------------------------------------
  cx_msg_debug_variable( ACTIVE_SANITIZERS )
  if ( "address" IN_LIST ACTIVE_SANITIZERS)
    # Call the ASan sanitizer function if ENABLE_ASAN is specified
    cx_scheme_sanitizer_asan(
      ${ENABLE_ASAN}
      ${DISABLE_DETECT_ODR_VIOLATION}
      ${DISABLE_RECOVERY_ASAN}
      ${DISABLE_LEAK_DETECTION_ASAN}
      ${DISABLE_STACK_USE_AFTER_RETURN}
      ${IGNORELIST_FILE_ASAN}
      ${SUPPRESSION_FILE_ASAN}
      ENABLE_USE_AFTER_SCOPE ${CX_SANITIZER_ENABLE_USE_AFTER_SCOPE}
      ${FLAGS_ASAN}
      OUTPUT ASAN_FLAGS
    )
    cx_msg_debug_variable( ASAN_FLAGS )
  endif( )
  if (ENV{ASAN_OPTIONS})
    set (ENV{ASAN_OPTIONS} $ENV{ASAN_OPTIONS} PARENT_SCOPE)
  endif ()

  #--------------------------------------------------------------------
  if ( "leak" IN_LIST ACTIVE_SANITIZERS)
    # Call the LSan sanitizer function if ENABLE_LSAN is specified
    cx_scheme_sanitizer_lsan(
      ${ENABLE_LSAN}
      ${DISABLE_RECOVERY_LSAN}
      ${SUPPRESSION_FILE_LSAN}
      ${FLAGS_LSAN}
      OUTPUT LSAN_FLAGS
    )
    cx_msg_debug_variable( LSAN_FLAGS )
  endif( )

  #--------------------------------------------------------------------
  if ( "memory" IN_LIST ACTIVE_SANITIZERS)
    # Call the MSan sanitizer function if ENABLE_MSAN is specified
    cx_scheme_sanitizer_msan(
      ${ENABLE_MSAN}
      ${DISABLE_RECOVERY_MSAN}
      ${DISABLE_LEAK_DETECTION_MSAN}
      ${SUPPRESSION_FILE_MSAN}
      ${FLAGS_MSAN}
      OUTPUT MSAN_FLAGS
      cx_msg_debug_variable( MSAN_FLAGS )
    )
  endif( )

  #--------------------------------------------------------------------
  if ( "thread" IN_LIST ACTIVE_SANITIZERS)
    # Call the TSan sanitizer function
    cx_scheme_sanitizer_tsan(
      ${ENABLE_TSAN}
      ${DISABLE_RECOVERY_TSAN}
      ${SUPPRESSION_FILE_TSAN}
      ${FLAGS_TSAN}
      OUTPUT TSAN_FLAGS
    )
    cx_msg_debug_variable( TSAN_FLAGS )
  endif( )

  #--------------------------------------------------------------------
  if ( "undefined" IN_LIST ACTIVE_SANITIZERS)
    # Call the UBSan sanitizer function
    cx_scheme_sanitizer_ubsan(
      ${ENABLE_UBSAN}
      ${DISABLE_RECOVERY_UBSAN}
      ${SUPPRESSION_FILE_UBSAN}
      ${FLAGS_UBSAN}
      OUTPUT UBSAN_FLAGS
    )
    cx_msg_debug_variable( UBSAN_FLAGS )
  endif( )

  # Combine the individual sanitizer flags
  if ( CMAKE_BUILD_TYPE STREQUAL "Debug" )
    set(SANITIZER_FLAGS "${ASAN_FLAGS} ${LSAN_FLAGS} ${MSAN_FLAGS} ${TSAN_FLAGS} ${UBSAN_FLAGS}")
    create_combined_ignorelist( OUTPUT_LIST SANITIZER_FLAGS )
  endif( )
  cx_msg_debug_variable( SANITIZER_FLAGS )

  # Apply the flags globally or to an output variable
  set(sanitizer_flags "${SANITIZER_FLAGS}")
  string(REPLACE ";" " " sanitizer_flags "${sanitizer_flags}")
  if(CX_SANITIZER_OUTPUT)
    set(${CX_SANITIZER_OUTPUT} "${sanitizer_flags}" PARENT_SCOPE)
  else( )
    cx_c_append_flags( OPTIONS ${sanitizer_flags} )
    cx_cxx_append_flags( OPTIONS ${sanitizer_flags} )
    cx_msg_debug_variable( CMAKE_CXX_FLAGS )
  endif()
  set( CX_SCHEME_SANITIZERS_ACTIVE "${ACTIVE_SANITIZERS}"
    CACHE STRING "Collection of active sanitizers" FORCE
  )
  mark_as_advanced(CX_SCHEME_SANITIZERS_ACTIVE)
  cx_msg_debug_variable( CX_SCHEME_SANITIZERS_ACTIVE )
endfunction()
