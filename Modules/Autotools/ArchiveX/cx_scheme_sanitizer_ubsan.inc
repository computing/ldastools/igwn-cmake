<H1>UndefinedBehaviorSanitizer (UBSan)</H1>

<H2>Purpose</H2>
<p>UndefinedBehaviorSanitizer (UBSan) detects <b>undefined behavior</b>
in C++ programs, such as integer overflows, illegal pointer dereferencing,
and various other issues that can lead to unpredictable program behavior.
UBSan helps ensure your program conforms to the C++ standard, catching
subtle bugs that may otherwise go unnoticed during development.</p>

<H2>When to Use</H2>
<p>UBSan is useful when you want to ensure your code is free from undefined
behavior, such as integer overflow or invalid type casts. It is especially
helpful in finding issues in low-level code where behavior might be
compiler-specific or platform-specific.</p>

<H2>Key Options</H2>
<ul>
  <li><b>ENABLE</b>: Turn on UndefinedBehaviorSanitizer.</li>
  <li><b>DISABLE_RECOVERY</b>: Stops program execution immediately when
  undefined behavior is detected.</li>
  <li><b>SUPPRESSION_FILE</b>: Use a suppression file to ignore known
  undefined behavior issues or false positives from external libraries.</li>
</ul>

<H2>Typical Use Case</H2>
\code
cx_scheme_sanitizer_ubsan(ENABLE)
\endcode

<H2>Benefits</H2>
<ul>
  <li>Catch subtle undefined behaviors that may cause unpredictable program
  behavior or security vulnerabilities.</li>
  <li>Ensure compliance with the C++ standard, preventing platform-specific
  undefined behaviors.</li>
  <li>Detect issues like integer overflow, invalid memory accesses, or
  improper type casting.</li>
</ul>
