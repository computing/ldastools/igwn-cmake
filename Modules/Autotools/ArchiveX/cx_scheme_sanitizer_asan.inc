<H1>AddressSanitizer (ASan)</H1>

<H2>Purpose</H2>
<p>AddressSanitizer (ASan) is a comprehensive tool to detect <b>memory-related
errors</b> like buffer overflows, use-after-free, and use-after-scope.
It’s one of the most widely used sanitizers in C++ projects because of its
ability to detect various memory issues that can cause undefined behavior
or security vulnerabilities.</p>

<H2>When to Use</H2>
<p>Use ASan when your project involves a lot of pointer arithmetic, dynamic
memory management, or complex array manipulations where buffer overflows or
out-of-bound accesses might occur.</p>

<H2>Key Options</H2>
<ul>
  <li><b>ENABLE</b>: Turn on AddressSanitizer.</li>
  <li><b>ENABLE_USE_AFTER_SCOPE</b>: Detects when a variable is accessed
  after its scope has ended.</li>
  <li><b>DISABLE_RECOVERY</b>: Forces the program to terminate immediately
  when a memory issue is detected.</li>
  <li><b>DISABLE_LEAK_DETECTION</b>: Temporarily disables the leak detection
  feature.</li>
  <li><b>SUPPRESSION_FILE</b>: Use a suppression file to manage known issues
  and false positives.</li>
</ul>

<H2>Typical Use Case</H2>
\code
cx_scheme_sanitizer_asan(ENABLE)
\endcode

<H2>Benefits</H2>
<ul>
  <li>Detect memory errors that can cause crashes or security
  vulnerabilities.</li>
  <li>Address common memory issues like buffer overflows or use-after-free
  early in the development cycle.</li>
</ul>
