<H1>ThreadSanitizer (TSan)</H1>

<H2>Purpose</H2>
<p>ThreadSanitizer (TSan) detects <b>data races</b> and <b>thread
synchronization issues</b> in multi-threaded C++ programs.
It helps identify errors where two or more threads access shared data
simultaneously without proper synchronization.</p>

<H2>When to Use</H2>
<p>If your project involves complex multi-threading and you suspect there
might be race conditions or improper locking mechanisms, TSan will help
surface those issues early.</p>

<H2>Key Options</H2>
<ul>
  <li><b>ENABLE</b>: Turn on ThreadSanitizer.</li>
  <li><b>DISABLE_RECOVERY</b>: Tells the program to terminate immediately
  upon detecting a thread-related issue.</li>
  <li><b>SUPPRESSION_FILE</b>: Specify known issues in a suppression file
  to avoid noise in the results.</li>
</ul>

<H2>Typical Use Case</H2>
\code
cx_scheme_sanitizer_tsan(ENABLE)
\endcode

<H2>Benefits</H2>
<ul>
  <li>Find thread-related bugs in a multi-threaded application.</li>
  <li>Prevent hard-to-reproduce race conditions that can lead to erratic
  behavior.</li>
</ul>
