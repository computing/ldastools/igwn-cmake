<H1>LeakSanitizer (LSan)</H1>

<H2>Purpose</H2>
<p>LeakSanitizer (LSan) helps detect <b>memory leaks</b>, which occur when
a program allocates memory dynamically but fails to release it.
Over time, memory leaks can cause performance degradation and even lead to
program crashes due to memory exhaustion.</p>

<H2>When to Use</H2>
<p>LSan is crucial in long-running applications where memory leaks can
accumulate and significantly affect performance over time, such as servers
or GUI applications with complex memory management.</p>

<H2>Key Options</H2>
<ul>
  <li><b>ENABLE</b>: Turn on LeakSanitizer.</li>
  <li><b>DISABLE_LEAK_DETECTION</b>: Temporarily disables leak detection if
  you don't want to track memory leaks.</li>
  <li><b>SUPPRESSION_FILE</b>: Suppress known memory leaks from external
  libraries or false positives.</li>
</ul>

<H2>Typical Use Case</H2>
\code
cx_scheme_sanitizer_lsan(ENABLE)
\endcode

<H2>Benefits</H2>
<ul>
  <li>Quickly identify and fix memory leaks to prevent performance
  degradation in long-running applications.</li>
  <li>Ensure efficient use of memory by catching unfreed memory
  allocations.</li>
</ul>
