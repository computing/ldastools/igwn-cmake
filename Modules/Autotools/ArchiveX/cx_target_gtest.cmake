#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )
include( GNUInstallDirs )
include( GNUPkgInstallDirs )

function( cx_target_gtest target )
  if ( NOT BUILD_TESTING OR ( CMAKE_CROSSCOMPILING AND NOT CMAKE_CROSSCOMPILING_EMULATOR ) )
    return( )
  endif( )

  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    COMMAND_ARGS
    DEPENDENCIES
    LDADD
    SOURCES
    EXTRA_GTEST_SOURCES
    INCLUDES
    )

  cmake_parse_arguments( ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT ARG_LDADD )
    set( ARG_LDADD ${LDADD} )
  endif( )
  if ( NOT ARG_INCLUDES )
    set( ARG_INCLUDES ${AM_CPPFLAGS} )
  endif( )

    set( THREADS_PREFER_PTHREAD_FLAG ON)
  find_package( Threads REQUIRED )

  cx_target_executable(
    ${target}
    NOINST
    SOURCES ${ARG_SOURCES}
    LDADD ${ARG_LDADD}
    INCLUDE_DIRECTORIES BEFORE PRIVATE
    ${ARG_INCLUDES}
    )
  if(ARG_DEPENDENCIES)
    add_dependencies( ${target} ${ARG_DEPENDENCIES} )
  endif( )
  if(ARG_EXTRA_GTEST_SOURCES)
    get_target_property(executable_sources ${target} SOURCES)
    set(scan_sources "SOURCES ${executable_sources} ${ARG_EXTRA_GTEST_SOURCES}")
  endif( )
  gtest_add_tests(
    TARGET ${target}
    ${scan_sources}
    EXTRA_ARGS
    --gtest_death_test_style=fast
    --gtest_catch_exceptions=1
    ${ARG_COMMAND_ARGS}
    TEST_LIST gtest_test_names
  )
  cx_scheme_sanitizer_set_environment(
    TEST_TARGETS ${gtest_test_names}
  )
endfunction( )
