include( Autotools/ArchiveX/cx_cxx_append_flags )

##
# @brief Adds compiler flags to treat warnings as errors based on specified build types.
#
# This function checks if the current build type matches any of the specified build styles.
# If it does, it adds the appropriate flag to treat all compiler warnings as errors:
# - For GCC, Clang, and AppleClang compilers, it adds `-Werror`.
# - For MSVC compilers, it adds `/WX`.
#
# @param build_styles List of build types (e.g., Debug, Release) in which to treat warnings as errors.
#
# @note This function relies on the `CMAKE_BUILD_TYPE` variable being set, typically in single-configuration builds.
# In multi-configuration generators (like Visual Studio or Xcode), you may need to set `CMAKE_CONFIGURATION_TYPES` instead.
#
# @usage Example usage to treat warnings as errors in Debug and Release builds:
# @code
# cx_cxx_warnings_as_errors(Debug Release)
# @endcode
#
# @example
# @code
# cx_cxx_warnings_as_errors(Debug)
# @endcode
# This example will only apply `-Werror` or `/WX` in the Debug build type.
#
function(cx_cxx_warnings_as_errors)
  # The first argument is a list of build styles
  set(build_styles ${ARGV})

  # Check if the current build type is in the provided list
  list(FIND build_styles "${CMAKE_BUILD_TYPE}" found_index)

  if (found_index GREATER -1)
    if (CMAKE_CXX_COMPILER_ID MATCHES "GNU|Clang|AppleClang")
      # For GCC, Clang, and AppleClang, add -Werror
      cx_cxx_append_flags( OPTIONS "-Werror")
    elseif (CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
      # For MSVC, add /WX
      cx_cxx_append_flags( OPTIONS "/WX")
    endif()
  endif()
endfunction( )
