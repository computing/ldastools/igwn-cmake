#------------------------------------------------------------------------
# -*- mode: cmake -*-
#
# MODULE_NAME - module name to use when checking for pkg_config
# HEADER_FILENAME - The name of the header file for library
#------------------------------------------------------------------------
include( CMakeParseArguments )
include( CheckLibraryExists )
include( FindPackageHandleStandardArgs )
include( Autotools/cm_define )
include( Autotools/cm_msg_checking )
include( Autotools/cm_msg_result )

function(CX_FIND_PKG MODULE_NAME )
  set(options)
  set(oneValueArgs PREFIX SEARCH_PREFIX)
  set(multiValueArgs)

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( ARG_PREFIX EQUAL "" )
    string( TOUPPER ${MODULE_NAME} _prefix )
  else( )
    set( _prefix ${ARG_PREFIX} )
  endif( )
  if ( ARG_SEARCH_PREFIX EQUAL "" )
    string( TOUPPER ${_prefix} _search_prefix )
  else( )
    set( _search_prefix ${ARG_SEARCH_PREFIX} )
  endif( )

  set(VERSION_STRING ${PREFIX}_VERSION_STRING)
  set(INCLUDE_DIR ${PREFIX}_INCLUDE_DIR)
  set(LIBRARY ${PREFIX}_LIBRARY)

  #----------------------------------------------------------------------
  # Check if there is a pkg_config for requested library
  #----------------------------------------------------------------------

  find_package(PkgConfig QUIET)
  set(PC_MODULE_NAME PC_${MODULE_NAME})
  if (PKG_CONFIG_FOUND)
    pkg_check_modules(${PC_MODULE_NAME} QUIET ${MODULE_NAME})
    if (${PC_LIBRARY}_FOUND)
      set(${VERSION_STRING} ${${PC_LIBRARY_NAME}_VERSION})
    endif()
  endif()

  #----------------------------------------------------------------------
  # Search for the header file
  #----------------------------------------------------------------------

  if ( NOT ARG_HEADER_FILENAME EQUAL "" )
    find_path( ${INCLUDE_DIR}
      NAMES ${ARG_HEADER_FILENAME}
      HINTS ${PC_MODULE_NAME}_INCLUDE_DIR
      )
    mark_as_advanced(${INCLUDE_DIR})
  endif( )

  #----------------------------------------------------------------------
  # Ensure that the version string is set
  #----------------------------------------------------------------------

  if (${INCLUDE_DIR} AND NOT ${VERSION_STRING}
      AND NOT ARG_HEADER_FILENAME EQUAL "")
    if (EXISTS "${${INCLUDE_DIR}}/${ARG_HEADER_FILENAME}")
      foreach(_version_component MAJOR MINOR RELEASE)
        file(STRINGS
          "${${INCLUDE_DIR}}/zstd.h"
          _${_version_component}_str
          REGEX "^#define ${_search_prefix}_VERSION_${_version_component}")
        string(REGEX REPLACE "^#define ${_search_prefix}_VERSION_${_version_component}.+([0-9]+)"
          "\\1"
          _${_version_component} "${_${_version_component}_str}")
        unset(_${_version_component}_str)
      endforeach()
      set(${VERSION_STRING} "${_MAJOR}.${_MINOR}.${_RELEASE}")
      unset(_MAJOR)
      unset(_MINOR)
      unset(_RELEASE)
    endif( )
  endif()

  #----------------------------------------------------------------------
  # Find library using CMake Constructs
  #----------------------------------------------------------------------

  find_package_handle_start_args(
    ${MODULE_NAME}
    REQUIRED_VARS
    ${LIBRARY}
    ${INCLUDE_DIR}
    VERSION_VAR
    ${VERSION_STRING}
    )

  #----------------------------------------------------------------------
  # TODO
  #  Bellow still needs to be translated
  #----------------------------------------------------------------------

  string(TOUPPER ${_prefix} _prefix_upper)
  unset( _RESULT CACHE )
  unset( _LIB CACHE )

  set( _HAVE_LIBRARY "no" )
  if (${_LIBRARY}_FOUND)
    find_library( _LIB ${_LIBRARY} )
    if ( _LIB )
      check_library_exists( ${_LIB} ${_FUNCTION} "" _RESULT)
      if ( _RESULT )
        set( LIBS ${_LIB} ${LIBS} )
        set( _HAVE_LIBRARY "yes" )
      endif ( _RESULT )
    endif( )
    unset( _LIB CACHE )
    cm_define(
      VARIABLE HAVE_LIB${_LIBRARY_UPPER}
      DESCRIPTION "Define to 1 if you have `${_LIBRARY}' library (-l${_LIBRARY})"  )
    set(${_LIBRARY}_LIBRARIES ${${_LIBRARY}_LIBRARY})
    set(${_LIBRARY}_INCLUDE_DIRS ${${_LIBRARY}_INCLUDE_DIR})
    set( HAVE_LIB${_LIBRARY_UPPER} 1 CACHE INTERNAL "Internal" )

    if (NOT TARGET ${_LIBRARY}::${_LIBRARY})
      add_library(${_LIBRARY}::${_LIBRARY} UNKNOWN IMPORTED)
      set_target_properties(${_LIBRARY}::${_LIBRARY}
        PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${${_LIBRARY}_INCLUDE_DIRS}"
        )
      if (EXISTS "${${_LIBRARY}_LIBRARY}")
        set_target_properties(${_LIBRARY}::${_LIBRARY}
          PROPERTIES
          IMPORTED_LINK_INTERFACE_LANGUAGES ${_INTERFACE_LANGUAGE}
          IMPORTED_LOCATION "${${_LIBRARY}_LIBRARY}")
      endif( )
    endif( )
  endif( )

  cm_msg_checking( "if library ${_LIBRARY} contains ${_FUNCTION}" )
  cm_msg_result( ${_HAVE_LIBRARY} )

endfunction( )
