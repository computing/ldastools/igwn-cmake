#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/ArchiveX/cx_msg_debug_variable )
include( Autotools/ArchiveX/cx_set_variable_default )

include( Autotools/ArchiveX/cx_packaging_deb )
include( Autotools/ArchiveX/cx_packaging_macports )
include( Autotools/ArchiveX/cx_packaging_rpm )

function(cx_packaging)
  set(options)
  set(oneValueArgs)
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  set(STAGING_DIR "${CMAKE_BINARY_DIR}/staging")
  set(TAR_DIR_NAME "${PROJECT_NAME}-${PROJECT_VERSION}")
  set(TAR_NAME "${CPACK_SOURCE_PACKAGE_FILE_NAME}${CPACK_SOURCE_PACKAGE_FILE_EXTENSION}")
  if(NOT PROJECT_SPEC_FILENAME)
    set(PROJECT_SPEC_FILENAME ${CMAKE_BINARY_DIR}/config/${PROJECT_NAME}.spec)
  endif(NOT PROJECT_SPEC_FILENAME)

  # cx_msg_debug_variable( STAGING_DIR )
  # cx_msg_debug_variable( TAR_NAME )
  # cx_msg_debug_variable( PROJECT_SPEC_FILENAME )

  string( REPLACE "-" "" TAR_COMPRESSION_OPTION "${CPACK_SOURCE_PACKAGE_COMPRESSION_OPTION}")
  add_custom_target(dist_pre
    COMMAND cpack -G ${CPACK_SOURCE_GENERATOR} --config CPackSourceConfig.cmake
    COMMAND ${CMAKE_COMMAND} -E make_directory ${STAGING_DIR}
    COMMAND ${CMAKE_COMMAND} -E chdir ${STAGING_DIR} ${CMAKE_COMMAND} -E tar "x${TAR_COMPRESSION_OPTION}f" ${CMAKE_BINARY_DIR}/${TAR_NAME} )
  add_custom_target( dist_body_begin
    DEPENDS dist_pre )
  add_custom_target( dist_body
    DEPENDS dist_body_begin )
  add_custom_target( dist_post
    COMMAND ${CMAKE_COMMAND} -E chdir ${STAGING_DIR} ${CMAKE_COMMAND} -E tar "c${TAR_COMPRESSION_OPTION}f" ${TAR_NAME} ${TAR_DIR_NAME}
    COMMAND ${CMAKE_COMMAND} -E rename ${STAGING_DIR}/${TAR_NAME} ./${TAR_NAME}
    COMMAND ${CMAKE_COMMAND} -E chdir ${STAGING_DIR} ls -R .
    COMMAND ${CMAKE_COMMAND} -E chdir . pwd
    COMMAND ${CMAKE_COMMAND} -E chdir . find . -name ${TAR_NAME}
    COMMAND ${CMAKE_COMMAND} -E tar "t${TAR_COMPRESSION_OPTION}f" ${TAR_NAME}
    COMMAND ${CMAKE_COMMAND} -E remove_directory ${STAGING_DIR}
    WORKING_DIRECTORY ${CMAKE_CURRENT_DIR}
    DEPENDS dist_body
    )
  add_custom_target( dist
    DEPENDS dist_post )

  # cx_msg_debug_variable( ARG_UNPARSED_ARGUMENTS )

  cx_packaging_deb(${ARG_UNPARSED_ARGUMENTS})
  cx_packaging_macports(${ARG_UNPARSED_ARGUMENTS})
  cx_packaging_rpm(${ARG_UNPARSED_ARGUMENTS})
endfunction(cx_packaging)
