# -----------------------------------------------------------------------
# -----------------------------------------------------------------------

function( cx_prog_changed_files )
  find_file( PROGRAM_CHANGED_FILES
    NAMES changed_files.cmake
    HINTS ${IGWN_CMAKE_SCRIPTDIR}
    PATHS ${IGWN_CMAKE_SCRIPTDIR}
    )
endfunction( )
