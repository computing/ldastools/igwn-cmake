#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
#
# cm_conditional( <variable> EXISTS <path> )
#
#------------------------------------------------------------------------
include( CMakeParseArguments )

# include( Autotools/Internal/ci_project_config )
include( Autotools/ch_template )

function(CM_CONDITIONAL variable)
  set(options)
  set(oneValueArgs EXISTS )
  set(multiValueArgs EXPRESSION )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  set( value TRUE )
  if ( ARG_EXISTS )
    if ( NOT EXISTS ${ARG_EXISTS} )
      set( value FALSE )
    endif( )
  endif ( )
  if ( ARG_EXPRESSION )
    if ( ${ARG_EXPRESSION} )
      # Nothing as this is the expected case
    else( )
      set( value FALSE )
    endif( )
  endif( )

  set( ${variable} ${value} PARENT_SCOPE )

endfunction()
