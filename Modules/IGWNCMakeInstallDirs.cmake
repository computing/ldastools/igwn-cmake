# -*- conding: utf-8; cmake-tab-width: 4; indent-tabs-mode: nil; -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
include(GNUInstallDirs)

if ( NOT IGWNCMAKE_INSTALL_DATADIR)
    set(IGWNCMAKE_INSTALL_DATADIR "" CACHE PATH "package specific read-only architecture-independent data (DATADIR/PROJECT_NAME)")
    set(IGWNCMAKE_INSTALL_DATADIR "${CMAKE_INSTALL_DATADIR}/igwn-cmake")
endif ( NOT IGWNCMAKE_INSTALL_DATADIR)

mark_as_advanced(
    IGWNCMAKE_INSTALL_DATADIR
    )
