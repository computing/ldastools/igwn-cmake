# Release 1.6.0 - December 6, 20224
  - Added Support for Fedora 39
  - Removed support for Scientific Linux 7
  - Added macro to support sanitizers (closes computing/ldastools/igwn-cmkae#22)
  - Improved support for sanitizers (closes computing/ldastools/igwn-cmake#21)
  - Added macro to treat compiler warnings as errors (closes computing/ldastools/igwn-cmake#23)

# Release 1.5.0 - December 8, 2023
  - Updated use of sysconfig for Python 3.11
  - Added Support for Fedora 36 (closes computing/ldastools/igwn-cmake#8)
  - Added Support for Fedora 37 (closes computing/ldastools/igwn-cmake#9)
  - Added Support for Fedora 38 (closes computing/ldastools/igwn-cmake#10)
  - Added Support for Rocky Linux 9 (closes computing/ldastools/igwn-cmake#11)
  - For Debian based system, use mk\_build\_deps to install build dependencies (closes #6)
  - Added Support for Ubuntu 18.04 (closes computing/ldastools/igwn-cmake#14)
  - Added support for Debian 12 - Bookworm (closes computing/ldastools/igwn-cmake#13)
  - Updated handling installation of Debian build dependencies (closes computing/ldastools/igwn-cmake#17)
  - Added macros to support syncronization with an outside source (closes #15)

# Release 1.4.0 - July 19, 2022
  - Added macro to find Zstd library (Closes #3)
  - Added generic macro to find a package
  - Modified cm_pkg_check_modules to add fully qualified libraries to _LIBRARIES_FULL_PATH
  - Added support for building Debian Buster i386

# Release 1.3.2 - Septempber 28, 2021
  - Updated ABI check macros to use package installation directory to prefix scripts
  - Added CI pipeline to test on Fedora 34
  - Added CI pipeline to test on Fedora 35

# Release 1.3.1 - August 5, 2021
  - Corrected installation configuration

# Release 1.3.0 - August 2, 2021
  - Changed package name to igwn-cmake-macros from igwn-cmake

# Release 1.2.3 - July 30, 2021
  - Corrections to cx_struct_procfs_stat function to make it more C compliant
  - Corrected variable names used in some debugging statements

# Release 1.2.2 - March 3, 2021
  - Modified epel stript to take defines for epel repository
  - Better support for MacPorts
  - Updated cx_check_libframel to use library framel instead of Frame

# Release 1.1.1 - October 23, 2019
  - Modified cx_check_sizes() to not insert extra semicolon for 32-bit
    use of long long (Closes #83)
  - Added cx_ldas_prog_cxx() which checks some compiler characteristics.
  - Added ci_cache_list() to provide CACHE lists.
  - Renamed cm_cache() to ci_cache() and provided cm_cache() as alias
    for backwards compatibility

# Release 1.1.0 - Septempber 26, 2019
  - Moved the installation of .pc file from /usr/lib to /usr/share

# Release 1.0.8 - August 14, 2019
  - Added function cm_header_stdc
  - Added function cx_thread
  - Added function cx_lib_socket_nsl
  - Corrections to function cm_try_compile
  - Corrections to function cm_search_libs
  - Corrections to function cm_func_strerror_r
  - Corrections to function cx_check_pthread_read_write_lock
  - Corrected Portfile to have proper description (Closes #55)

# Release 1.0.7 - February  2, 2019
  - Corrected cm_init to provide Autotools defines as strings
  - Added cx_target_test
  - Moved most cm_ functions from Autotools/ArchiveX to Autotools

# Release 1.0.6 - January  8, 2019
  - Addeed EXTEND_SWIG_FLAGS to cx_swig_python_module

# Release 1.0.5 - December 6, 2018
  - Allow quieting of debug messages
  - For OSX shared libraries, use only the major number
  - Addressed packaging issues

# Release 1.0.4 - October 22, 2018
  - Added cx_target_pkgconfig

# Release 1.0.3 - August 27, 2018
  - Enhancements to further support for Pythyon 3

# Release 1.0.2 - June 22, 2018
  - Corrected Description syntax inside of debian/control

# Release 1.0.1 - June 19, 2018
  - Initial Release
