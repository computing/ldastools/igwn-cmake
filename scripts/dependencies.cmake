macro(ci_set_policy ID VALUE)
  if( POLICY ${ID} )
    cmake_policy( SET ${ID} ${VALUE})
  endif( )
endmacro( )

ci_set_policy( CMP0053 NEW )

set( base_dir ${CMAKE_CURRENT_LIST_FILE} )
file( TO_CMAKE_PATH ${base_dir} base_dir )
string(REGEX REPLACE "/[^/]+/[^/]+$" "" base_dir ${base_dir})

# message( WARNING "DEBUG: INFO: base_dir: ${base_dir}")

foreach( path IN ITEMS
    ${base_dir}
    ${CMAKE_MODULE_SOURCE_DIR}
    $ENV{CI_PROJECT_DIR}
    "/opt/local/shareigwn-cmake/cmake"
    "/usr/share/igwn-cmake/cmake"
    )
  # message( WARNING "DEBUG: INFO: trying path: ${path}")
  if ( EXISTS "${path}/Modules" )
    set( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${path}/Modules" )
  endif( )
endforeach( )
# message( WARNING "DEBUG: INFO: CMAKE_MODULE_PATH: ${CMAKE_MODULE_PATH}")

include( CMakeParseArguments )
include( Autotools/ArchiveX/cx_cmake_language )
include( Autotools/ArchiveX/cx_cmake_list_join )

find_program( PROG_RPM rpm )
find_program( PROG_DNF dnf )
find_program( PROG_YUM yum )
find_program( PROG_APT_GET apt-get )

set( PROJECT_NAME $ENV{CI_PROJECT_NAME} )
set( PROJECT_URL $ENV{CI_PROJECT_URL} )
set( PROJECT_DESCRIPTION $ENV{CI_PROJECT_NAME} )
set( PROJECT_DESCRIPTION_LONG $ENV{CI_PROJECT_NAME} )

function( echo )
  execute_process( COMMAND ${CMAKE_COMMAND} -E echo ${ARGN})
endfunction( )

function( exclude DEPENDENCIES )
  if ( DEFINED EXCLUDE_PATTERN )
    if ( NOT "${EXCLUDE_PATTERN}" MATCHES "^[\\^]")
      set(EXCLUDE_PATTERN "^.*${EXCLUDE_PATTERN}")
    endif( )
    if ( NOT "${EXCLUDE_PATTERN}" MATCHES "[\\$]$")
      set(EXCLUDE_PATTERN "${EXCLUDE_PATTERN}.*$")
    endif( )
    foreach( dep ${${DEPENDENCIES}} )
      # message( INFO "Comparing ${dep} to ${EXCLUDE_PATTERN}" )
      if ( NOT "${dep}" MATCHES "${EXCLUDE_PATTERN}" )
        list(APPEND new_depends "${dep}" )
      endif( )
    endforeach( )
    # message( INFO ": new_depends:  ${new_depends}" )
    set( ${DEPENDENCIES} ${new_depends} PARENT_SCOPE )
  endif( )
endfunction( )

function(extract_set_calls file_path output_variable)
  # message( WARNING "DEBUG: INFO: file_path: ${file_path}")
  file(READ "${file_path}" file_contents)
  # message( WARNING "DEBUG: INFO: file_contents: ${file_contents}")

  # Split the contents into lines
  string(REGEX REPLACE "\r\n" "\n" file_contents "${file_contents}")
  string(REGEX REPLACE "\r" "\n" file_contents "${file_contents}")
  string(REPLACE "\n\n" "\n" file_contents "${file_contents}")
  string(REPLACE "\n" " " file_contents "${file_contents}")
  string(REPLACE ";" "\\;" file_contents "${file_contents}")
  separate_arguments(file_contents)
  # message( WARNING "DEBUG: INFO: parsing for set commands: ${file_contents}")

  # Iterate through lines to identify multiline set calls
  set(set_calls "")
  set(in_multiline_set_call FALSE)
  set( in_function FALSE )
  set(multiline_set_call "")

  foreach(line IN LISTS file_contents)
    if ( line MATCHES "^[ \\t]*#" )
      # Ignore comments
    else( )
      # message( WARNING "DEBUG: INFO: parsing line for set commands: ${line}")
      if ( in_function )
        if ( line MATCHES "^endfunction\\(")
          set( in_function FALSE )
        endif( )
      elseif (in_multiline_set_call)
        set(multiline_set_call "${multiline_set_call} ${line}")
        if (line MATCHES "\\)$")
          set(in_multiline_set_call FALSE)
          list(APPEND set_calls "${multiline_set_call}")
        endif()
      elseif (line MATCHES "^set\\(")
        set(in_multiline_set_call TRUE)
        set(multiline_set_call "${line}")
        if (line MATCHES "\\)$")
          set(in_multiline_set_call FALSE)
          list(APPEND set_calls "${multiline_set_call}")
        endif()
      elseif ( line MATCHES "^function\\(")
        set( in_function TRUE )
      endif()
    endif( )
  endforeach()

  list(APPEND set_calls
    "if ( DEFINED PACKAGE_DESCRIPTION_LONG )"
    "  string( REPLACE \":semicolon:\" \"\\n\" PACKAGE_DESCRIPTION_LONG \"\${PACKAGE_DESCRIPTION_LONG}\")"
    "endif( )"
    "if ( DEFINED PROJECT_DESCRIPTION_LONG )"
    "  string( REPLACE \":semicolon:\" \"\\n\" PROJECT_DESCRIPTION_LONG \"\${PROJECT_DESCRIPTION_LONG}\")"
    "endif( )"
    )
  cx_cmake_list_join( set_calls "\n" set_calls )
  string( REPLACE ":semicolon:" ";" set_calls "${set_calls}" )
  # message( WARNING "DEBUG: INFO: set_calls: ${set_calls}")
  # Output the result to the specified variable
  set(${output_variable} "${set_calls}" PARENT_SCOPE)
endfunction()

macro( get_project_variables )
  message( NOTIICE "ENTRY: get_project_variables" )
  get_top_dir( topdir )
  set( project_file FALSE )
  set( project_file_canidates
    "${topdir}/CMakeLists.txt"
    "$ENV{CI_PROJECT_DIR}${project_dir}/CMakeLists.txt"
    )
  foreach( file ${project_file_canidates} )
    # message( WARNING "DEBUG: INFO: trying file: ${file}" )
    if ( EXISTS "${file}" )
      # message( WARNING "DEBUG: INFO: found file: ${file}" )
      set(project_file ${file})
      break()
    endif( )
  endforeach( )
  message( "Checking top level CMakeLists.txt file" )
  message( "  found ... ${project_file}" )
  if ( project_file )
    #----------------------------------------------------------------------
    # Knowing the configuration file, can process it for project variables
    #----------------------------------------------------------------------
    # message( WARNING "DEBUG: INFO: Extracting project variables from project_file: ${project_file}" )
    extract_set_calls( "${project_file}" set_calls )
    # message( WARNING "DEBUG: INFO: set_calls: ${set_calls}" )
    cx_cmake_language( EVAL CODE "${set_calls}" )
  endif( )
  message( "EXIT: get_project_variables" )
endmacro( )

function( get_top_dir output_variable )
  if ( TOP_DIR )
    set( topdir "${TOP_DIR}" )
  elseif( ENV{TOP_DIR })
    set( topdir "$ENV{TOP_DIR}" )
  endif( )
  if ( PROJECT_DIR )
    set( topdir "${topdir}/${PROJECT_DIR}" )
  elseif ( ENV{PROJECT_DIR} )
    set( topdir "${topdir}/$ENV{PROJECT_DIR}" )
  endif( )
  string(REGEX REPLACE "/\\.$" "" topdir ${topdir})
  set( ${output_variable} ${topdir} PARENT_SCOPE )
endfunction( )

function( DebInstallDependencies )
  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs DEPENDENCIES
    )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  message( "DebInstallDependencies: Installing: ${ARG_DEPENDENCIES}")
  foreach( _dependency ${ARG_DEPENDENCIES} )
    string(REPLACE "|" ";"  _dependency ${_dependency} )
    set( installed FALSE )
    foreach( dep ${_dependency} )
      message( "DebInstallDependencies: Installing: ${PACKAGE_MANAGER} install -y ${dep}" )
      execute_process( COMMAND ${PACKAGE_MANAGER} install -y ${dep}
        RESULT_VARIABLE exit_status
        OUTPUT_VARIABLE output_msg
        ERROR_VARIABLE error_msg
        )
      if ( exit_status EQUAL 0 )
        set( installed TRUE )
        break( )
      endif( )
    endforeach( )
    if ( NOT installed )
      message( FATAL_ERROR "Unable to install any of: ${_dependency}" )
    endif( )
  endforeach( )
endfunction( )

function( DebMakeBuildDependenciesPackage CONTROL_FILE )
  file( GLOB_RECURSE files "*-build-deps_*.deb" )
  if ( files )
    # Command has already been run and produced the files
    return( )
  endif( )
  foreach ( pkg devscripts equivs dpkg-dev )
    execute_process(
      COMMAND ${pkg_mgr}
      -o Debug::pkgProblemResolver=yes
      --no-install-recommends
      --yes
      install ${pkg}
      OUTPUT_VARIABLE cout
      ERROR_VARIABLE cerr
      RESULT_VARIABLE EXIT_STATUS
      )
    # message( "DEBUG: installing ${pkg}" )
    # message( "DEBUG: exit status: ${EXIT_STATUS}" )
    # message( "DEBUG: output: ${cout}" )
    # message( "DEBUG: error output: ${cerr}" )
  endforeach( )
  find_program( PROG_MK_BUILD_DEPS mk-build-deps )
  if ( PROG_MK_BUILD_DEPS )
    file(READ "${CONTROL_FILE}" file_contents)
    # message( WARNING "DEBUG: Getting  debian build dependencies file from: ${CONTROL_FILE}\n${file_contents}" )
    execute_process(
      COMMAND ${PROG_MK_BUILD_DEPS} ${CONTROL_FILE}
      )
    file( GLOB_RECURSE files "*-build-deps_*.deb" )
    file( READ "${files}" file_contents )
    # message( WARNING "DEBUG: ${files} contains:\n${file_contents}")
  endif( )
endfunction( )

function( DebInstallBuildDependencies CONTROL_FILE )
  if ( NOT INSTALL )
    return( )
  endif( )
  DebMakeBuildDependenciesPackage( ${CONTROL_FILE} )
  file( GLOB_RECURSE files "*-build-deps_*.deb" )
  foreach( file ${files} )
    # message( WARNING "DEBUG: installing debian package file: ${file}" )
    execute_process(
      COMMAND ${pkg_mgr}
      install -y ${file}
      OUTPUT_VARIABLE cout
      ERROR_VARIABLE cerr
      RESULT_VARIABLE EXIT_STATUS
      )
    message( ${cout} )
  endforeach( )
endfunction( )

function( DebListBuildDependencies CONTROL_FILE VAR )
  set( deps ${VAR} )
  DebMakeBuildDependenciesPackage( ${CONTROL_FILE} )
  file( GLOB_RECURSE files "*-build-deps_*.deb" )
  find_program( PROG_DPKG_DEB dpkg-deb )
  if ( PROG_DPKG_DEB )
    foreach( package_file ${files} )
      execute_process(
        COMMAND ${PROG_DPKG_DEB}
          -I
          ${package_file}
        OUTPUT_VARIABLE cout
        ERROR_VARIABLE cerr
        RESULT_VARIABLE EXIT_STATUS
        )
      if ( NOT EXIT_STATUS EQUAL 0  )
        continue( )
      endif( )
      # Use regular expression to match lines starting with "Depends:"
      string(REGEX MATCHALL "^Depends:.*$" depends_lines "${cout}")
      foreach ( dep_line IN LISTS dependslines )
        string( REGEX REPLACE "^Depends: " "" dep_line ${dep_line})
        string( REPLACE "," " " dep_line ${dep_line} )
        list( APPEND deps ${dep_line} )
      endforeach( )
    endforeach( )
  endif( )
  list( REMOVE_DUPLICATES deps )
  set( ${VAR} ${deps} PARENT_SCOPE )
endfunction( )

function( DebBuildDependencies )
  get_top_dir( topdir )
  message( "Looking for control files: ${topdir}/control ${topdir}/control.in")
  file( GLOB_RECURSE files "${topdir}/control" "${topdir}/control.in" )
  message( "Found control file canidates: ${files}" )
  set( keepers )
  foreach( file ${files})
    if ( NOT file MATCHES /legacy/ )
      list( APPEND keepers ${file} )
    endif( )
  endforeach( )
  set( files ${keepers} )
  # message( "DEBUG: control files: ${files}" )
  unset(deps)
  foreach( SPEC_FILE ${files} )
    file( READ ${SPEC_FILE} file_contents )
    string( CONFIGURE "${file_contents}" file_contents @ONLY )
    # Remove any undefined substitution variables
    string( REGEX REPLACE '@[A-Za-z0-9_]*@' "_junk_" file_contents "${file_contents}" )
    string( REGEX REPLACE "^[ \t]*[^ \t]+:[ \t]*$"
      "" file_contents "${file_contents}"
      )
    get_filename_component( SPEC_FILE_EXPANDED "${SPEC_FILE}" NAME)
    set( SPEC_FILE_EXPANDED "/tmp/${SPEC_FILE_EXPANDED}" )
    string(REGEX REPLACE "[.]in$" "" SPEC_FILE_EXPANDED "${SPEC_FILE_EXPANDED}" )
    message( "Writing ${SPEC_FILE_EXPANDED} with ${file_contents}" )
    file( WRITE ${SPEC_FILE_EXPANDED} "${file_contents}" )
    DebListBuildDependencies( ${SPEC_FILE_EXPANDED} deps )
    DebInstallBuildDependencies( ${SPEC_FILE_EXPANDED} )
    # message( WARNING "INFO: ${SPEC_FILE_EXPANDED}\n${file_contents}")
  endforeach( )
  return( deps )
endfunction( )

function( RPMInstallPackage PACKAGE )
  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( PROG_DNF )
    execute_process(
      COMMAND ${PROG_DNF} install --assumeyes ${PACKAGE}
      OUTPUT_VARIABLE local_output
      ERROR_VARIABLE local_error
      )
  elseif( PROG_YUM )
    execute_process(
      COMMAND ${PROG_YUM} install --assumeyes ${PACKAGE}
      OUTPUT_VARIABLE local_output
      ERROR_VARIABLE local_error
      )
  endif( )
  unset( local_output )
  unset( local_error )

endfunction( )

##
# @fn RPMListDependencies( SPEC_FILE VAR )
#
# @brief List build dependencies from given SPEC_FILE
#
# @details
# This function will list BuildRequires dependencies
# specified in the SPEC_FILE.
#
# @param[in] SPEC_FILE
#   RPM spec file; must have the extension .spec
# @param[out] VAR
#   name of variable to substitute with result
#
# @see @ref RPMInstallDependencies
#
function( RPMListDependencies SPEC_FILE VAR )
  RPMInstallPackage( rpm-build )
  find_program( PROG_RPMSPEC rpmspec )

  message( INFO "PROG_RPMSPEC: ${PROG_RPMSPEC}")
  message( INFO "PROG_RPM: ${PROG_RPM}")
  set( dependencies "" )
  if ( PROG_RPMSPEC )
    execute_process(
      COMMAND ${PROG_RPMSPEC} --buildrequires --query "${SPEC_FILE}"
      OUTPUT_VARIABLE dependencies
      ERROR_VARIABLE dependencies_error
      )
  elseif ( PROG_RPM )
    file( READ ${SPEC_FILE} dependencies )
    execute_process(
      COMMAND ${PROG_RPM} --eval "${dependencies}"
      OUTPUT_VARIABLE raw_dependencies
      ERROR_VARIABLE dependencies_error
      )
    set( deps "" )
    foreach(line ${raw_dependencies})
      if( "${line}" MATCHES "^[Bb]uild[Rr]equires:" )
        string(REGEX REPLACE "^[Bb]uild[Rr]equires:[ \t]*" "" line ${line})
        list(APPEND dependencies ${line})
      endif( )
    endforeach( )
  endif( )
  message(WARNING "dependencies ${dependencies}")
  message(WARNING "dependencies_error ${dependencies_error}")
  string( REGEX REPLACE "\n" ";" dependencies "${dependencies}")
  foreach(line ${dependencies})
    string(REGEX REPLACE ">*=[ \t]*((([0-9]+)[.])*[0-9]+)*[ \t]*," "," line ${line})
    string(REGEX REPLACE ">*=[ \t]*[@][^@]*[@][ \t]*," "," line ${line})
    string(REGEX REPLACE ">*=[ \t]*((([0-9]+)[.])*[0-9]+)*$" "" line ${line})
    string(REGEX REPLACE ">*=[ \t]*[@][^@]*[@]$" "" line ${line})
    string(REGEX REPLACE "," ";" line ${line})
    foreach(pkg_ ${line})
      string(STRIP "${pkg_}" pkg_)
      list(APPEND deps ${pkg_})
    endforeach()
  endforeach( )
  set( ${VAR} ${deps} PARENT_SCOPE )
endfunction( )

##
## @fn RPMInstallDependencies( SPEC_FILE VAR )
##
## @brief Installs build dependencies from given SPEC_FILE
##
## @details
## This function will attempt to install BuildRequires dependencies
## specified in the SPEC_FILE.
##
## @param[in] SPEC_FILE
##   RPM spec file; must have the extension .spec
## @param[out] VAR
##   name of variable to substitute with result
##
## @note
##   If there is no known way to install the dependencies,
##   a list of packages to be installed will be returned
##
## @see @ref RPMListDependencies
##
function( RPMInstallDependencies SPEC_FILE VAR )
  if( INSTALL )
    message( INFO "INFO: DEP: PROG_DNF: ${PROG_DNF}")
    message( INFO "INFO: DEP: PROG_YUM: ${PROG_YUM}")
    execute_process(
      COMMAND rpmspec --buildrequires -q "${SPEC_FILE}"
      OUTPUT_VARIABLE dependencies
      ERROR_VARIABLE dependencies_error
      )
    message(WARNING "INFO: DEP: Dependencies to install: ${dependencies}" )
    if ( PROG_DNF )
      execute_process(
        COMMAND dnf builddep -y "${SPEC_FILE}"
        OUTPUT_VARIABLE dependencies
        ERROR_VARIABLE dependencies_error
        )
    elseif( PROG_YUM )
      RPMInstallPackage( yum-utils )
      find_program( PROG_YUM_BUILDDEP yum-builddep )
      if ( PROG_YUM_BUILDDEP )
        message( INFO " COMMAND: ${PROG_YUM_BUILDDEP} --assumeyes --verbose --debuglevel=10 \"${SPEC_FILE}\"" )
        execute_process(
          COMMAND ${PROG_YUM_BUILDDEP} --assumeyes --verbose --debuglevel=10 "${SPEC_FILE}"
          OUTPUT_VARIABLE dependencies
          ERROR_VARIABLE dependencies_error
          )
      endif( )
    else( )
      RPMListDependencies( ${SPEC_FILE} dependencies )
    endif( )
    set( ${VAR} ${dependencies} PARENT_SCOPE )
  endif( )
endfunction( )

function( RPMBuildDependencies VAR )
  get_top_dir( topdir )
  file( GLOB_RECURSE files "${topdir}/*.spec" "${topdir}/*.spec.in" )
  set( keepers )
  foreach( file ${files})
    if ( NOT file MATCHES /legacy/
        AND NOT file MATCHES "[/][.][^/]*$" )
      list( APPEND keepers ${file} )
    endif( )
  endforeach( )
  set( files ${keepers} )
  message( WARNING "files: ${files}" )
  unset(deps)
  foreach( SPEC_FILE ${files} )
    file( READ ${SPEC_FILE} dependencies )
    string( CONFIGURE "${dependencies}" dependencies @ONLY )
    # Remove any undefined substitution variables
    string( REGEX REPLACE '@[A-Za-z0-9_]*@' "_junk_" dependencies "${dependencies}" )
    message(WARNING "configured dependencies ${dependencies}")
    get_filename_component(SPEC_FILE_EXPANDED "${SPEC_FILE}" NAME)
    set( SPEC_FILE_EXPANDED "/tmp/${SPEC_FILE_EXPANDED}" )
    string(REGEX REPLACE "[.]in$" "" SPEC_FILE_EXPANDED "${SPEC_FILE_EXPANDED}" )
    file( WRITE ${SPEC_FILE_EXPANDED} "${dependencies}" )
    file( READ ${SPEC_FILE_EXPANDED} dependencies )
    message(WARNING "INFO: DEP: Contents of ${SPEC_FILE_EXPANDED}: ${dependencies}")
    message(WARNING "INFO: DEP: INSTALL: ${INSTALL}")
    if ( INSTALL )
      RPMInstallDependencies( ${SPEC_FILE_EXPANDED} deps )
    else( )
      RPMListDependencies( ${SPEC_FILE_EXPANDED} deps )
    endif( )
    file( REMOVE ${SPEC_FILE_EXPANDED} )
  endforeach( )
  if ( deps )
    list( SORT deps )
    list( REMOVE_DUPLICATES deps )
  endif( )
  message( WARNING "deps ${deps}" )
  set( ${VAR} ${deps} PARENT_SCOPE )
endfunction( )

#========================================================================
#   M A I N
#========================================================================

get_project_variables( )
set( VERBOSE True )
if( NOT PYTHON3_VERSION_NODOTS )
  if ( PYTHON3_VERSION )
    set( PYTHON3_VERSION_NODOTS ${PYTHON3_VERSION} )
    string( REPLACE "." "" PYTHON3_VERSION_NODOTS "${PYTHON3_VERSION_NODOTS}" )
    set( PYTHON3_VERSION_NODOTS "${PYTHON3_VERSION_NODOTS}" CACHE INTERNAL "" )
  endif( )
endif( )
if( NOT PYTHON2_VERSION_NODOTS )
  if ( PYTHON2_VERSION )
    set( PYTHON2_VERSION_NODOTS ${PYTHON2_VERSION} )
    string( REPLACE "." "" PYTHON2_VERSION_NODOTS "${PYTHON2_VERSION_NODOTS}" )
    set( PYTHON2_VERSION_NODOTS ${PYTHON2_VERSION_NODOTS} CACHE INTERNAL "" )
  endif( )
endif( )
message( "Working on package manager for: ${PACKAGER}" )
if ( PACKAGER STREQUAL "rpm" )
  find_program( pkg_mgr NAMES yum )
  set( PACKAGE_MANAGER ${pkg_mgr} CACHE STRING "Package Manager used to install packages" )
  RPMBuildDependencies( depends )
elseif ( PACKAGER STREQUAL "deb" )
  find_program( pkg_mgr NAMES apt-get )
  set( PACKAGE_MANAGER ${pkg_mgr} CACHE STRING "Package Manager used to install packages" )
  DebBuildDependencies( depends )
else( )
  set( VERBOSE False )
endif( )
message( "Have calculated dependencies" )
if ( VERBOSE )
  if ( DEFINED INCLUDE_PATTERN )
    if ( NOT "${INCLUDE_PATTERN}" MATCHES "^[\\^]")
      set(INCLUDE_PATTERN "^.*${INCLUDE_PATTERN}")
    endif( )
    if ( NOT "${INCLUDE_PATTERN}" MATCHES "[\\$]$")
      set(INCLUDE_PATTERN "${INCLUDE_PATTERN}.*$")
    endif( )
    foreach( dep ${depends} )
      if ( "${dep}" MATCHES "${INCLUDE_PATTERN}" )
        list(APPEND new_depends "${dep}" )
      endif( )
    endforeach( )
    set(depends ${new_depends})
  endif( )
  if ( NOT DEFINED INSTALL )
    set(INSTALL FALSE )
  endif( )
  exclude( depends )
  foreach( dep ${depends} )
    echo( ${dep} )
  endforeach( )
endif( )
