function(echo)
  execute_process(COMMAND ${CMAKE_COMMAND} -E echo "${ARGN}")
endfunction( )

if( CMAKE_MAJOR_VERSION GREATER 2 )
  echo(cmake)
else( )
  echo(cmake3)
endif( )
