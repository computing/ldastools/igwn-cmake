# -----------------------------------------------------------------------
# -----------------------------------------------------------------------
cmake_minimum_required( VERSION 3.13 FATAL_ERROR )

function(list_changed_files)
  foreach( file ${FILES} )
    if ( NOT EXISTS ${INSTALLED_PREFIX}/${file} )
      message( "NEW:    ${GIT_PREFIX}/${file}" )
      continue( )
    endif( )
    set( source_file "${GIT_PREFIX}/${file}" )
    set( destination_file ${INSTALLED_PREFIX}/${file} )
    execute_process( COMMAND ${CMAKE_COMMAND} -E
      compare_files ${source_file} ${destination_file}
      RESULT_VARIABLE compare_result
      OUTPUT_VARIABLE ignore_output
      ERROR_VARIABLE ignore_error
      )
    if( compare_result EQUAL 0)
    elseif( compare_result EQUAL 1)
      message("DIFFER: ${source_file}")
    else()
      message("Error while comparing the files.")
    endif()
  endforeach( )
endfunction( )

function(diff_changed_files)
  foreach( file ${FILES} )
    if ( NOT EXISTS ${INSTALLED_PREFIX}/${file} )
      message( "${file} is new" )
      # continue( )
    endif( )
    set( source_file "${GIT_PREFIX}/${file}" )
    set( destination_file ${INSTALLED_PREFIX}/${file} )
    execute_process(
      COMMAND ${PROGRAM} -Nu ${destination_file} ${source_file}
      )
  endforeach( )
endfunction( )

function(push_changed_files)
  if ( USER_OPTION )
    set( owner ${USER_OPTION} )
  endif( )
  if ( GROUP_OPTION )
    set( owner "${owner}:${GROUP_OPTION}" )
  endif( )
  string( REPLACE " " ";" PERMISSIONS_OPTION "${PERMISSIONS_OPTION}" )
  file( MAKE_DIRECTORY ${DESTDIR}${INSTALLED_PREFIX} )
  if ( LINK_PREFIX )
    file( MAKE_DIRECTORY ${DESTDIR}${LINK_PREFIX} )
  endif( )
  foreach( file ${FILES} )
    set( source_file "${GIT_PREFIX}/${file}" )
    set( destination_file ${INSTALLED_PREFIX}/${file} )
    message( STATUS "Installing ${source_file} into ${DESTDIR}${INSTALLED_PREFIX}" )
    file( INSTALL ${source_file}
      DESTINATION ${DESTDIR}${INSTALLED_PREFIX}
      FILE_PERMISSIONS ${PERMISSIONS_OPTION}
      )
    if ( LINK_PREFIX )
      file( CREATE_LINK
        ${source_file} ${LINK_PREFIX}/${file}
        SYMBOLIC
        )
    endif( )
    if ( $ENV{USER} STREQUAL "root" AND
        EXISTS ${destination_file} )
      execute_process(
        COMMAND ${CHOWN_COMMAND} ${owner} ${destination_file}
        )
    endif( )
  endforeach( )
endfunction( )

function(pull_changed_files)
  foreach( file ${FILES} )
    set( source_file "${GIT_PREFIX}/${file}" )
    set( destination_file ${INSTALLED_PREFIX}/${file} )
    execute_process(
      COMMAND ${CMAKE_COMMAND} -E copy_if_different ${destination_file} ${source_file}
      )
  endforeach( )
endfunction( )

find_program( CHOWN_COMMAND chown )

if ( MODE STREQUAL "list" )
  list_changed_files( )
elseif( MODE STREQUAL "diff" )
  diff_changed_files( )
elseif( MODE STREQUAL "push" )
  push_changed_files( )
elseif( MODE STREQUAL "pull" )
  pull_changed_files( )
endif( )
