#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
#
# USAGE: cmake [-D<option>=<value>] -P check_api_abi.cmake
#
#________________________________________________________________________
# option        :                   description
#________________________________________________________________________
#   HEADER_DIR  :  Path to directory containing header files.
#________________________________________________________________________
#   HEADERS     :  Semicolon separated list of header files relative to
#                  HEADER_DIR
#
#------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.12)
find_package(Python3 COMPONENTS Interpreter)

set( DEBUG_OFF 0 )
set( DEBUG_LOW 10 )
set( DEBUG_MEDIUM 20 )
set( DEBUG_HIGH 30 )
set( DEBUG_EXTREAM 40 )

set(command_line_options_stride 2)
set(command_line_options
  HEADER_DIR "Path to the directory containing header files"
  HEADERS "Semi-colon separated list of header files."
  INCLUDE_NEW_DIR "Path to the directory containing the new version of the header files."
  LIBRARY "Name of library"
  LIBRARY_NEW_DIR "Path to the directory containing the new version of the library"
  LIBRARY_OLD_DIR "Path to the directory containing the old version of the library"
  MESSAGE "Message to be displayed when doing comparison"
  )

#------------------------------------------------------------------------
# *** WARNING ***
# Setting CMAKE variables needed by some functions as the following
#   commands are not scriptable:
#   enable_language()
#------------------------------------------------------------------------
if (UNIX)
  if (APPLE)
    # OSX
    set(CMAKE_FIND_LIBRARY_PREFIXES lib)
    set(CMAKE_FIND_LIBRARY_SUFFIXES .dylib .so .a)
    if(NOT CMAKE_INSTALL_PREFIX)
      if(EXISTS /opt/local)
        # MacPorts
        set(CMAKE_INSTALL_PREFIX /opt/local)
      else( )
        set(CMAKE_INSTALL_PREFIX /usr)
      endif()
    endif()
  else( )
    # Unix/Linux
    set(CMAKE_FIND_LIBRARY_PREFIXES lib)
    set(CMAKE_FIND_LIBRARY_SUFFIXES .so .a)
    if(NOT CMAKE_INSTALL_PREFIX)
      set(CMAKE_INSTALL_PREFIX /usr)
    endif()
  endif( )
else( )
endif( )

include( CMakeParseArguments )
include( GNUInstallDirs )

set(CMAKE_COMMAND_ECHO ${CMAKE_COMMAND} -E echo)

#------------------------------------------------------------------------
# These are the critical programs needed
#------------------------------------------------------------------------
find_program( ABI_DUMPER_PROGRAM
  NAMES abi-dumper
  )
find_program( ABI_COMPLIANCE_CHECKER_PROGRAM
  NAMES abi-compliance-checker
  )

function( _msg_debug_variable VARIABLE)
  _message(DEBUG ${DEBUG_MEDIUM} "${VARIABLE} := ${${VARIABLE}}" )
endfunction( )

function(_command_line_variable VARIABLE )
  if ( DEFINED ${VARIABLE} )
    list(LENGTH ARGN num_extra_args)
    if (${num_extra_args} GREATER 0)
      list(GET ${ARGN} 0 VARIABLE_NAME)
    else( )
      set(VARIABLE_NAME ${VARIABLE})
    endif( )
    list(INSERT ${VARIABLE} 0 ${VARIABLE_NAME})
    set(${VARIABLE_NAME} ${${VARIABLE_NAME}} PARENT_SCOPE)
  endif( )
endfunction( )

function( _dump_library )
  #
  # Parse parameters passed to the function
  #
  set(options
    NEW
    )
  set(oneValueArgs
    HEADER_DIR
    INSTALL_PREFIX_DIR
    LIBRARY
    LIBRARY_DIR
    MESSAGE
    OUTPUTFILE
    VERSION
    )
  set(multiValueArgs
    HEADERS
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  if(ARG_NEW)
    set(_version 1)
  else()
    set(_version 0)
    if(NOT ARG_INSTALL_PREFIX_DIR)
      set(ARG_INSTALL_PREFIX_DIR ${CMAKE_INSTALL_PREFIX})
    endif( )
    set(_debug_info_dir "${ARG_INSTALL_PREFIX_DIR}/lib/debug/${INSTALL_PREFIX_DIR}/${CMAKE_INSTALL_LIBDIR}")
    file(TO_CMAKE_PATH "${_debug_info_dir}" _debug_info_dir )
    _msg_debug_variable(_debug_info_dir)
    if(EXISTS "${_debug_info_dir}")
      # Pull out debug information
      list(APPEND _options "--search-debuginfo=${_debug_info_dir}")
    endif()
  endif()

  _msg_debug_variable(ARG_LIBRARY)
  _msg_debug_variable(ARG_LIBRARY_DIR)

  #----------------------------------------------------------------------
  # Locate the library
  #----------------------------------------------------------------------
  set(paths_ ${ARG_LIBRARY_DIR}64 ${ARG_LIBRARY_DIR}/64 ${ARG_LIBRARY_DIR})
  unset(library_full_path CACHE)
  find_library(library_full_path
    NAMES ${ARG_LIBRARY}
    PATHS ${paths_}
    HINTS ${paths_}
    NO_DEFAULT_PATH
    NO_CMAKE_PATH
    NO_CMAKE_SYSTEM_PATH
    )
    _msg_debug_variable(library_full_path)
    _get_shared_library_version(_version ${library_full_path})

  #----------------------------------------------------------------------
  # Dump ABI/API information
  #----------------------------------------------------------------------
  if ( library_full_path )
    string( REGEX REPLACE "\\.[^.]*$" "" base_name "${ARG_LIBRARY}")
    set(base_name "${base_name}_ABI_${_version}")
    list(APPEND _options --lver ${_version})
    # list(APPEND _options --debug)
    # list(APPEND _options --use-tu-dump)
    list(APPEND _options --sort --dir --all)
    #--------------------------------------------------------------------
    # Produce a collection of header files to be used
    #--------------------------------------------------------------------
    if (ARG_HEADERS)
      foreach(hdr ${ARG_HEADERS})
        list(APPEND _headers "${ARG_HEADER_DIR}/${hdr}")
      endforeach()
      string(REPLACE ";" "\n" _headers "${_headers}")
      file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/${base_name}.hdrs" ${_headers})
      list(APPEND _options --public-headers ${base_name}.hdrs)
    endif()
    set(_output_filename "${CMAKE_CURRENT_BINARY_DIR}/${base_name}.dump")
    if(ARG_OUTPUTFILE)
      set(${ARG_OUTPUTFILE} "${_output_filename}" PARENT_SCOPE)
    endif( )
    message( STATUS
      "Dumping ABI/API information for ${library_full_path}"
      )
    execute_process(
      COMMAND ${ABI_DUMPER_PROGRAM}
      ${library_full_path}  -o ${_output_filename} ${_options}
      #OUTPUT_QUIET
      #ERROR_QUIET
      RESULT_VARIABLE exit_code
      OUTPUT_VARIABLE msg_stout
      ERROR_VARIABLE msg_stderr
      OUTPUT_STRIP_TRAILING_WHITESPACE
      ERROR_STRIP_TRAILING_WHITESPACE
      )
    unset(library_full_path CACHE)
    if (NOT "${ARG_VERSION}" STREQUAL "")
      set(${ARG_VERSION} ${_version} PARENT_SCOPE)
    endif( )
  else( )
    execute_process(
      COMMAND ${CMAKE_COMMAND_ECHO}
      "Unable to find ${ARG_LIBRARY} in ${ARG_LIBRARY_DIR}"
      )
  endif( )
endfunction( )

function( _gen_compatibility_report )
  #
  # Parse parameters passed to the function
  #
  set(options
    )
  set(oneValueArgs
    DUMP_INFO_NEW
    DUMP_INFO_OLD
    LIBRARY_NAME
    REPORT
    VERSION_NUMBER_NEW
    VERSION_NUMBER_OLD
    )
  set(multiValueArgs
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  _set_default( ARG_VERSION_NUMBER_OLD  0 )
  _set_default( ARG_VERSION_NUMBER_NEW  1 )

  execute_process(
    COMMAND ${ABI_COMPLIANCE_CHECKER_PROGRAM}
    -quiet
    -l ${ARG_LIBRARY_NAME}
    -old ${ARG_DUMP_INFO_OLD}
    -new ${ARG_DUMP_INFO_NEW}
    RESULT_VARIABLE compliance_exit_code
    #OUTPUT_VARIABLE msg_compliance_stout
    #ERROR_VARIABLE msg_compliance_stderr
    #OUTPUT_STRIP_TRAILING_WHITESPACE
    #ERROR_STRIP_TRAILING_WHITESPACE
    )
  set(report "${CMAKE_CURRENT_BINARY_DIR}/compat_reports/${ARG_LIBRARY_NAME}/${ARG_VERSION_NUMBER_OLD}_to_${ARG_VERSION_NUMBER_NEW}/compat_report.html")
  _msg_debug_variable(report)
  if ( EXISTS ${report} AND (NOT "${ARG_REPORT}" STREQUAL "") )
    set(${ARG_REPORT} ${report} PARENT_SCOPE)
  endif( )
endfunction( )

function(_get_shared_library_version VARIABLE FILENAME)
  get_filename_component(real_name ${FILENAME} REALPATH)
  get_filename_component(base_name ${real_name} NAME)
  _msg_debug_variable(base_name)
  string(REGEX REPLACE "^.*[.]so[.]" "" base_name "${base_name}")
  string(REGEX REPLACE "^.*[.]([0-9][.0-9]*)[.]dylib$" "\\1" base_name "${base_name}")
  string(REGEX REPLACE "[.]" "_" base_name "${base_name}")
  _msg_debug_variable(base_name)
  set( ${VARIABLE} ${base_name} PARENT_SCOPE )
endfunction( )

function(_message )
  #
  # Parse parameters passed to the function
  #
  set(options
    FATAL
    STATUS
    )
  set(oneValueArgs
    DEBUG
    )
  set(multiValueArgs
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  if ( NOT ARG_UNPARSED_ARGUMENTS )
    return( )
  endif( )
  set(mode False)
  set(message_prefix "")
  if ( ARG_FATAL )
    set(mode "FATAL_ERROR")
  elseif ( ARG_STATUS )
    set(mode "STATUS")
  elseif ( NOT "${ARG_DEBUG}" STREQUAL "" )
    if ( NOT DEFINED DEBUG OR DEBUG LESS ${ARG_DEBUG} )
      return( )
    endif( )
    set(message_prefix "DEBUG: ")
    set(mode "STATUS" )
  endif( )
  if ( mode )
    string(REPLACE ";" " " message_body "${ARG_UNPARSED_ARGUMENTS}")
    if ( "${LOG_FORMAT}" STREQUAL "cmake" )
      message(${mode} ${message_prefix}${message_body})
    elseif ( "${LOG_FORMAT}" STREQUAL "autotools" )
      execute_process(
        COMMAND ${CMAKE_COMMAND_ECHO} "${message_prefix}${message_body}")
    endif( )
  endif( )
endfunction( )

function(append_option LIST_VARIABLE ARGUMENT )
  if ( ${ARGUMENT} )
    list(LENGTH ARGN num_extra_args)
    if (${num_extra_args} GREATER 0)
      list(GET ${ARGN} 0 ARGUMENT_NAME)
    else( )
      set(ARGUMENT_NAME ${ARGUMENT})
    endif( )
    list(APPEND ${LIST_VARIABLE} ${ARGUMENT_NAME} ${${ARGUMENT}})
  endif( )
endfunction( )

macro(_set_default VARIABLE)
  if(NOT ${VARIABLE})
    set(${VARIABLE} ${ARGN})
  endif( )
endmacro( )

function(_set_default_with_env VARIABLE ENV_VARIABLE)
  # _message(DEBUG ${DEBUG_LOW} "INSTALL_PREFIX: $ENV{CMAKE_INSTALL_PREFIX}")
  # _message(DEBUG ${DEBUG_LOW} "${ENV_VARIABLE}: $ENV{${ENV_VARIABLE}}")
  if(NOT "$ENV{${ENV_VARIABLE}}" STREQUAL "")
    _message(DEBUG ${DEBUG_LOW} "${VARIABLE} is being set to environment variable ${ENV_VARIABLE}")
    set(${VARIABLE} $ENV{${ENV_VARIABLE}})
    _message(DEBUG ${DEBUG_LOW} "${VARIABLE} is now ${${VARIABLE}}")
    set(${VARIABLE} ${${VARIABLE}} PARENT_SCOPE)
  endif( )
  if(NOT ${VARIABLE})
    _message(DEBUG ${DEBUG_LOW} "${VARIABLE} is being set default value ${ARGN}")
    set(${VARIABLE} ${ARGN} PARENT_SCOPE)
  endif( )
endfunction( )

#------------------------------------------------------------------------
# _verify - 
#------------------------------------------------------------------------
function( _verify)
  #
  # Parse parameters passed to the function
  #
  set(options
    )
  set(oneValueArgs
    INSTALL_PREFIX_OLD_DIR
    HEADER_DIR
    INCLUDE_NEW_DIR
    INCLUDE_OLD_DIR
    LIBRARY
    LIBRARY_NAME
    MESSAGE
    )
  set(multiValueArgs
    HEADERS
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  _set_default_with_env(ARG_INSTALL_PREFIX_OLD_DIR CMAKE_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX})
  _msg_debug_variable(ARG_INSTALL_PREFIX_OLD_DIR)

  if (NOT ARG_INLCUDE_OLD_DIR)
    if(NOT ARG_INSTALL_PREFIX_OLD_DIR)
      set(ARG_INSTALL_PREFIX_OLD_DIR ${CMAKE_INSTALL_PREFIX})
    endif( )
    set(ARG_INCLUDE_OLD_DIR "${ARG_INSTALL_PREFIX_OLD_DIR}/${CMAKE_INSTALL_INCLUDEDIR}/${ARG_HEADER_DIR}")
  endif( )
  #if (EXISTS ${CMAKE_INSTALL_FULL_LIBDIR}64)
  #  set(CMAKE_INSTALL_LIBDIR ${CMAKE_INSTALL_LIBDIR}64)
  #  set(CMAKE_INSTALL_FULL_LIBDIR ${CMAKE_INSTALL_FULL_LIBDIR}64)
  #elseif (EXISTS ${CMAKE_INSTALL_FULL_LIBDIR}/64)
  #  set(CMAKE_INSTALL_LIBDIR ${CMAKE_INSTALL_LIBDIR}/64)
  #  set(CMAKE_INSTALL_FULL_LIBDIR ${CMAKE_INSTALL_FULL_LIBDIR}/64)
  #else()
  #endif()
  if(ARG_MESSAGE)
    set(_msg_begin "STARTED: ${ARG_MESSAGE} ${ARG_LIBRARY}")
    set(_msg_end "FINISHED: ${ARG_MESSAGE} ${ARG_LIBRARY}")
  endif()

  _set_default(ARG_LIBRARY_OLD_DIR "${ARG_INSTALL_PREFIX_OLD_DIR}/${CMAKE_INSTALL_LIBDIR}")
  _set_default(ARG_LIBRARY_NEW_DIR ${CMAKE_CURRENT_BINARY_DIR})

  _msg_debug_variable(ARG_INSTALL_PREFIX_OLD)
  _msg_debug_variable(ARG_LIBRARY_OLD_DIR)

  set(library_name ${ARG_LIBRARY_NAME})

  #list(APPEND LIBRARY_ARGUMENTS LIBRARY ${ARG_LIBRARY})
  append_option(APPEND_LIBRARY_ARGUMENTS ARG_LIBRARY LIBRARY)
  if ( ARG_HEADER_DIR )
    list(APPEND OLD_LIBRARY_ARGUMENTS HEADER_DIR ${ARG_HEADER_DIR})
  endif( )

  _dump_library(
    INSTALL_PREFIX_DIR ${INSTALL_PREFIX_OLD_DIR}
    HEADER_DIR ${ARG_INCLUDE_OLD_DIR}
    HEADERS ${ARG_HEADERS}
    LIBRARY ${ARG_LIBRARY}
    LIBRARY_DIR ${ARG_LIBRARY_OLD_DIR}
    OUTPUTFILE DUMP_INFO_OLD
    VERSION VERSION_OLD
    )
  _dump_library(
    NEW
    HEADER_DIR ${ARG_INCLUDE_NEW_DIR}
    HEADERS ${ARG_HEADERS}
    LIBRARY ${ARG_LIBRARY}
    LIBRARY_DIR ${ARG_LIBRARY_NEW_DIR}
    OUTPUTFILE DUMP_INFO_NEW
    VERSION VERSION_NEW
    )
  _message(STATUS ${_msg_begin})
  _message(STATUS "Doing comparison ...")
  _gen_compatibility_report(
    VERSION_NUMBER_OLD ${VERSION_OLD}
    DUMP_INFO_OLD ${DUMP_INFO_OLD}
    VERSION_NUMBER_NEW ${VERSION_NEW}
    DUMP_INFO_NEW ${DUMP_INFO_NEW}
    LIBRARY_NAME ${ARG_LIBRARY}
    REPORT compat_report
    )
  if ( EXISTS "${compat_report}" )
    execute_process(
      COMMAND ${ABI_API_ANALYSER_EXECUTABLE} "${compat_report}"
      RESULT_VARIABLE analyser_exit_code
      OUTPUT_VARIABLE msg_analyser_stdout
      ERROR_VARIABLE msg_analyser_stderr
      OUTPUT_STRIP_TRAILING_WHITESPACE
      ERROR_STRIP_TRAILING_WHITESPACE
      )
    if ( NOT ${analyser_exit_code} EQUAL 0 )
      if ( NOT msg_analyser_stdout )
        set(msg_analyser_stdout "ABI/API incompatibility detected: ${analyser_exit_code}" )
      endif( )
      _message(FATAL ${msg_analyser_stdout} )
    endif( )
  endif( )
  _message(STATUS ${_msg_end})
  _message(DEBUG ${DEBUG_HIGH} "analyser_exit_code: ${analyser_exit_code}")
  _message(DEBUG ${DEBUG_HIGH}  "msg_analyser_stdout: ${msg_analyser_stdout}")
  _message(DEBUG ${DEBUG_HIGH}  "msg_analyser_stderr: ${msg_analyser_stderr}")
endfunction( )

function(VARIABLE DEFAULT_VALUE)
  if ( "${${VARIABLE}}" STREQUAL "" )
    set(${VARIABLE} ${DEFAULT_VALUE} PARENT_SCOPE)
  endif( )
endfunction( )

function(_usage)
  set(_position 0)
  foreach( _opt ${command_line_options})
    math(EXPR _position "(${_position} + 1) % ${command_line_options_stride}")
    if(${_position} EQUAL 0)
      set(_desc ${_opt})
      execute_process(
        COMMAND ${CMAKE_COMMAND_ECHO} "\t${_var}:\t${_desc}")
    elseif(${_position} EQUAL 1)
      set(_var ${_opt})
    endif( )
  endforeach( )
endfunction( )

#------------------------------------------------------------------------
# Function: main
#------------------------------------------------------------------------
function( main )
  #
  # Verify 
  #
  if ( ABI_DUMPER_PROGRAM AND ABI_COMPLIANCE_CHECKER_PROGRAM )
    #
    # Blindly pass all arguments to the verify function
    #
    _verify(${ARGN})
  else( )
    execute_process(COMMAND ${CMAKE_COMMAND} -E
      echo "Unable to check ABI/API compatibility for ${ARG_LIBRARY} (ABI_DUMPER_PROGRAM: ${ABI_DUMPER_PROGRAM} ABI_COMPLIANCE_CHECKER_PROGRAM: ${ABI_COMPLIANCE_CHECKER_PROGRAM})"
      )
  endif( )
endfunction( )

#========================================================================
#========================================================================
if(HELP)
  _usage()
  return( )
endif( )

set(ABI_API_ANALYSER_EXECUTABLE ${Python3_EXECUTABLE} ${PY_ABI_API_ANALYSER_SCRIPT} CACHE INTERNAL "")
_set_default_with_env(LOG_FORMAT LOG_FORMAT "cmake")
string(TOLOWER "${LOG_FORMAT}" LOG_FORMAT)
_msg_debug_variable(LOG_FORMAT)
set(LOG_FORMAT ${LOG_FORMAT} CACHE INTERNAL "" FORCE)
_msg_debug_variable(LOG_FORMAT)
_set_default_with_env(DEBUG DEBUG ${DEBUG_OFF})
set(DEBUG ${DEBUG} CACHE INTERNAL "" FORCE)

set(_position 0)
foreach(_opt ${command_line_options})
  if( ${_position} EQUAL 0)
    _command_line_variable(${_opt})
  endif( )
  math(EXPR _position "(${_position} + 1) % ${command_line_options_stride}")
endforeach( )

main(
  ${HEADER_DIR}
  ${HEADERS}
  ${INCLUDE_NEW_DIR}
  ${LIBRARY}
  ${LIBRARY_NEW_DIR}
  ${LIBRARY_OLD_DIR}
  ${MESSAGE}
  )
