#========================================================================
#========================================================================
%define name 	@CPACK_RPM_PACKAGE_NAME@
%define version @CPACK_RPM_PACKAGE_VERSION@
%define release @CPACK_RPM_PACKAGE_RELEASE@
%define summary @PROJECT_DESCRIPTION@
%if 0%{?rhel} >= 8 || 0%{?fedora} >= 26
%define BuildRequiresCMake BuildRequires:  cmake >= 3.6
%define cmake cmake
%define ctest ctest
%else
%define BuildRequiresCMake BuildRequires:  cmake3 >= 3.6, cmake
%define cmake cmake3
%define ctest ctest3
%endif

#========================================================================
# Main spec file
#========================================================================
Name: 		%{name}
Summary: 	%{summary}
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	GPLv2+
Group: 		LSC Software/Data Analysis
Source: 	%{name}-%{version}@CPACK_SOURCE_PACKAGE_FILE_EXTENSION@
Packager: Edward Maros (ed.maroso@ligo.org)
URL: 		  @PACKAGE_URL@
BuildArch:      noarch
BuildRoot:      %{buildroot}
%BuildRequiresCMake
BuildRequires:  rpm-build
BuildRequires:  python3-rpm-macros
BuildRequires:  make
BuildRequires:  gcc, gcc-c++, glibc
BuildRequires:	gawk, pkgconfig
BuildRequires:  doxygen
Obsoletes:      igwn-cmake < 1.3.0
Provides: 	    igwn-cmake
Prefix:		      %_prefix

%description
@PROJECT_DESCRIPTION_LONG@

#------------------------------------------------------------------------
# Get onto the fun of building the NDS software
#------------------------------------------------------------------------

%prep
%setup -q

%build
%cmake \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    -DCMAKE_INSTALL_PREFIX=%{_prefix} \
    -DCMAKE_INSTALL_LIBDIR=%{_libdir} \
    -Wno-dev \
    .

%install
make DESTDIR=%{buildroot} install

%check
%ctest --verbose %{?_smp_mflags}


#----------------------------------------------
# Do the noarch files
#----------------------------------------------

%files
%_datadir/

%changelog
* Fri Dec 06 2024 14:39:45 Edward Maros <ed.maros@ligo.org> - 1.6.0-1
- Built for new release

* Fri Dec 08 2023 10:26:16 Edward Maros <ed.maros@ligo.org> - 1.5.0-1
- Built for new release

* Tue Jul 19 2022 13:20:43 Edward Maros <ed.maros@ligo.org> - 1.4.0-1
- Built for new release

* Tue Sep 28 2021 08:47:18 Edward Maros <ed.maros@ligo.org> - 1.3.2-1
- Built for new release

* Thu Aug 05 2021 11:11:45 Edward Maros <ed.maros@ligo.org> - 1.3.1-1
- Built for new release

* Mon Aug 02 2021 13:38:53 Edward Maros <ed.maros@ligo.org> - 1.3.0-1
- Built for new release

* Fri Jul 30 2021 Edward Maros <ed.maros@ligo.org> - 1.2.3-1
- Built for new release

* Wed Mar 03 2021 Edward Maros <ed.maros@ligo.org> - 1.2.2-1
- Built for new release

* Thu May 28 2020 Edward Maros <ed.maros@ligo.org> - 1.2.1-1
- Removed .git* files from source distribution

* Wed May 27 2020 Edward Maros <ed.maros@ligo.org> - 1.2.0-1
- Built for new release
